<?php

require_once('Admin.class.php');
$admin = new Admin();


require_once('Category.class.php');
$category = new Category();

$blog_categories = $category->createCategoriesForAdmin();
$smarty->assign("blog_categories", $blog_categories);

// print_r($_POST);
// print_r($_FILES);


// --------------------------------------------------------------
// logout admin
// -------------------------------------------------------------- 

if ($_REQUEST['action'] == "LogoutAdmin") {
    
    // usuwamy dane usera z sesji
    unset($_SESSION['admin_data']);
    // i kierujemy się na stronę główną
    header('Location: /_panel/');
}
 

if (sizeof($_SESSION['admin_data'])) {
	
	$smarty->assign("admin_data", $_SESSION['admin_data']);
}



// --------------------------------------------------------------
// dynamiczne sortowanie
// --------------------------------------------------------------

if($url_config['2'] == "sortable"){
	
	if($_GET['listItem']){
	
	
		$order_list_temp = array();	
			//print_r($_GET['listItem']);
		foreach ($_GET['listItem'] as $position => $item){
			
			$key = $position + 1;
			$order_list_temp[$key] = $item;

			
			$admin->updateOrders($item, $key);
			
		}

		$smarty->assign("good_message", "Kolejność została ustalona");
		
		$smarty->display("good_message.tpl");

	}		
	
	exit;

}

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['admin_form']) {
	$smarty->assign("ret_post", $_POST['admin_form']);
	
	//print_r($_POST['admin_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['admin_form']) {
	$smarty->assign("ret_post_search", $_POST['admin_form']);
}

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania slajdów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchAdmin") {
	$_SESSION['AdminFilters'] = $_POST['admin_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($url_config['2'] == "clear") {
	unset($_SESSION['AdminFilters']);
	header("location: /_panel/admin/index");
}


// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	$admin->setStatus($url_config['3'], $url_config['4']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/admin/index/1");
	
}

// --------------------------------------------------------------
// usunięcie slajdu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	//Najpierw wyciagamy dane o artykule
	$admin_details = $admin->getAdmin($url_config['3']);	
	
	// usuwamy slajd
	$admin->removeAdmin($url_config['3']);
	
	$_SESSION['message']['good_message'] = "Admin został usunięty";
	header("location: /_panel/admin/index");	
	
}

// --------------------------------------------------------------
// zapisanie slajdu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveAdmin") {
	
	// echo "save admin";
	
	if (1) {
		if (isset($_POST['admin_form'])) {
			
				
					
	
			//Walidacja url_name
			//require_once('validate_save_admin.inc.php');
					
			if (!sizeof($error)) {			
			
			
				// zapisujemy slajd
				$AdminId = $admin->saveAdmin($_POST['admin_form']);
				
				if(!$_POST['admin_form']['id']){
					
					$_SESSION['message']['good_message'] = "Administrator został pomyślnie dodany";
				}
				else{
					
					$_SESSION['message']['good_message'] = "Administrator został pomyślnie zapisany";
				}
				
				
				
				
				
				header("location: /_panel/admin/index/1");
			
			
				
			}
			//Błędy
			else{
				
				$smarty->assign("error", $error['admin']);
				$url_config['2'] = "edit";
				//print_r($error);
				
				
			}
			

		}
	}
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['AdminSetOrder'] = $set_sort['0'];
	$_SESSION['AdminSetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['AdminSetOrder'] = "surname";
	$_SESSION['AdminSetDirection'] = "asc";
}
$smarty->assign("sort_order", $_SESSION['AdminSetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['AdminSetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 



// --------------------------------------------------------------
// lista slajdów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['3'])) {
		$url_config['3'] = 1;
	}
	
	
	//Jesli powrót do listy po jakiejś akcji
	if ($_REQUEST['action'] == "reindex"){
		
		$url_config['3'] = 1;
		
		
		
	}

	
	if (isset($_SESSION['AdminFilters'])) {
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$admins_list = $admin->getAdminsSearch($_SESSION['AdminSetOrder'], $_SESSION['AdminSetDirection'], $_SESSION['AdminFilters'], $url_config['3'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		
		$smarty->assign("ret_filters", $_SESSION['AdminFilters']);
	}
	else {
		
		$admins_list = $admin->getAdmins($_SESSION['AdminSetOrder'], $_SESSION['AdminSetDirection'], $url_config['3'], $_SESSION['lang']);
	
	}
	
	
	 //print_r($admins_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "admin_list.tpl";
	$smarty->assign("admins_list", $admins_list);
	$smarty->assign("akcja", "admin_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $_SESSION['AdminCategoryId']);
	$smarty->assign("paging", $admin->paging);
}


// --------------------------------------------------------------
// nowy slajd
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('admin_form[content]' /*name*/, '' /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '400px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	
	$template = "admin_edit.tpl";
	
	$smarty->assign("akcja", "admin_edit");
	
	$smarty->assign("category_id", $_SESSION['AdminCategoryId']);
	$smarty->assign("language_id", 1);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
}

// --------------------------------------------------------------
// edycja slajdu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$admin_details = $admin->getAdmin($url_config['3']);
	
	$template = "admin_edit.tpl";
	$smarty->assign("admin", $admin_details);
	$smarty->assign("akcja", "admin_edit");
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane slajdu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$admin_details = $admin->getAdminDetails($_REQUEST['AdminId'], $_REQUEST['LanguageId']);

	// print_r($admin_details);
	
	$template = "admin_details.tpl";
	$smarty->assign("admin", $admin_details);
}

//Przekazujemy info o domyslnej kategorii
if($_SESSION['AdminCategoryId']){
	
	$smarty->assign("CategoryId", $_SESSION['AdminCategoryId']);
	
}
else{
	
	$smarty->assign("CategoryId", 1);
}



?>