<?php

require_once('Company.class.php');
$company = new Company();

// print_r($_POST);
// print_r($_FILES);


// --------------------------------------------------------------
// dynamiczne sortowanie
// --------------------------------------------------------------

if($url_config['2'] == "sortable"){
	
	if($_GET['listItem']){
	
	
		$order_list_temp = array();	
			
		foreach ($_GET['listItem'] as $position => $item){
			
			$key = $position + 1;
			$order_list_temp[$key] = $item;

			
			$company->updateOrder($item, $key);
			
		}

	}	
	
	
	
	exit;

}

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['company_form']) {
	$smarty->assign("ret_post", $_POST['company_form']);
	
	//print_r($_POST['company_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['company_form']) {
	$smarty->assign("ret_post_search", $_POST['company_form']);
}

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchCompany") {
	$_SESSION['CompanyFilters'] = $_POST['company_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($url_config['2'] == "clear") {
	unset($_SESSION['CompanyFilters']);
	header("location: /_panel/company/index");
}
// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$company_details = $company->getCompany($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($company_details);
	
	$company->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/company/index/".$company_details['category_id']);
	
}

// --------------------------------------------------------------
// ustawia na strone glowna
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "home") {
	
	//Najpierw wyciagamy dane o artykule
	$company_details = $company->getCompany($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($company_details);
	
	$company->setHome($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status home został zmieniony";
	header("location: /_panel/company/index/".$company_details['category_id']);
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	//Najpierw wyciagamy dane o artykule
	$company_details = $company->getCompany($url_config['3'], $_SESSION['admin_data']['language']);	
	
	// usuwamy artykuł
	$company->removeCompany($url_config['3']);
	
	$_SESSION['message']['good_message'] = "Artykuł został usunięty";
	header("location: /_panel/company/index/".$company_details['category_id']);	
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveCompany") {
	
	// echo "save company";
	
	if (1) {
		if (isset($_POST['company_form'])) {
			
				
					
	
			//Walidacja url_name
			require_once('validate_save_company.inc.php');
					
			if (!sizeof($error)) {			
			
			
				// zapisujemy artykuł
				$CompanyId = $company->saveCompany($_POST['company_form']);
				
				if(!$_POST['company_form']['company_id']){
					
					$_SESSION['message']['good_message'] = "Artykuł został pomyślnie dodany";
				}
				else{
					
					$_SESSION['message']['good_message'] = "Artykuł został pomyślnie zapisany";
				}
				
				
				
				
				
				header("location: /_panel/company/index/".$_POST['company_form']['category_id']);
			
			
				
			}
			//Błędy
			else{
				
				$smarty->assign("error", $error['company']);
				$url_config['2'] = "edit";
				//print_r($error);
				
				
			}
			

		}
	}
}



// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('company','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	$url_config['3'] = $url_config['4'];
	$_REQUEST['action'] = "reindex";
}

if($url_config['2'] == "down" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('company','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	$url_config['3'] = $url_config['4'];
	$_REQUEST['action'] = "reindex";
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['CompanySetOrder'] = $set_sort['0'];
	$_SESSION['CompanySetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['CompanySetOrder'] = "order";
	$_SESSION['CompanySetDirection'] = "asc";
}
$smarty->assign("sort_order", $_SESSION['CompanySetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['CompanySetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 



// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['4'])) {
		$url_config['4'] = 1;
	}
	
	// ustawienie id kategorii (jezeli nie została podana) - admin główny	
	$_SESSION['CompanyCategoryId'] = 1;
	
	//Jesli powrót do listy po jakiejś akcji
	if ($_REQUEST['action'] == "reindex"){
		
		$url_config['4'] = 1;
		
		
		
	}

	
	if (isset($_SESSION['CompanyFilters'])) {
		
		$_SESSION['CompanyFilters']['category_id'] = $_SESSION['CompanyCategoryId'];
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$companys_list = $company->getCompanysSearch($_SESSION['CompanySetOrder'], $_SESSION['CompanySetDirection'], $_SESSION['CompanyFilters'], $url_config['4'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		
		$smarty->assign("ret_filters", $_SESSION['CompanyFilters']);
	}
	else {
		$companys_list = $company->getCompanys($_SESSION['CompanySetOrder'], $_SESSION['CompanySetDirection'], $url_config['4'], $_SESSION['CompanyCategoryId'], $_SESSION['lang']);
	
	}
	
	
	 //print_r($companys_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "company_list.tpl";
	$smarty->assign("companys_list", $companys_list);
	$smarty->assign("akcja", "company_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $_SESSION['CompanyCategoryId']);
	$smarty->assign("paging", $company->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('company_form[content]' /*name*/, '' /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '400px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	
	$template = "company_edit.tpl";
	
	$smarty->assign("akcja", "company_edit");
	
	$smarty->assign("category_id", $_SESSION['CompanyCategoryId']);
	$smarty->assign("language_id", 1);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	$smarty->assign('sSpaw2', $sSpaw2);
}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$company_details = $company->getCompany($url_config['3'], $url_config['4']);

	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('company_form[content]' /*name*/, $company_details['content'] /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '200px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	

	
	// print_r($sw);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "company_edit.tpl";
	$smarty->assign("company", $company_details);
	$smarty->assign("language_id", $company_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	$smarty->assign('sSpaw2', $sSpaw2);
	$smarty->assign("category_id", $_SESSION['CompanyCategoryId']);
	$smarty->assign("akcja", "company_edit");
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$company_details = $company->getCompanyDetails($_REQUEST['CompanyId'], $_REQUEST['LanguageId']);

	// print_r($company_details);
	
	$template = "company_details.tpl";
	$smarty->assign("company", $company_details);
}

//Przekazujemy info o domyslnej kategorii
if($_SESSION['CompanyCategoryId']){
	
	$smarty->assign("CategoryId", $_SESSION['CompanyCategoryId']);
	
}
else{
	
	$smarty->assign("CategoryId", 1);
}



?>