<?php

require_once('Gallery.class.php');
$gallery = new Gallery();


require_once('Staff.class.php');
$staff = new Staff();

$gallery_categories = $staff->getStaffsToMenu(1, 1);
//print_r($gallery_categories);
$smarty->assign("gallery_categories", $gallery_categories);


// print_r($_POST);
// print_r($_FILES);

// --------------------------------------------------------------
// dynamiczne sortowanie
// --------------------------------------------------------------

if($url_config['2'] == "sortable"){
	
	if($_GET['listItem']){
	
	
		$order_list_temp = array();	
			//print_r($_GET['listItem']);
		foreach ($_GET['listItem'] as $position => $item){
			
			$key = $position + 1;
			$order_list_temp[$key] = $item;

			
			$gallery->updateOrders($item, $key);
			
		}

		$smarty->assign("good_message", "Kolejność została ustalona");
		
		$smarty->display("good_message.tpl");

	}		
	
	exit;

}

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['gallery_form']) {
	$smarty->assign("ret_post", $_POST['gallery_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['search_form']) {
	$smarty->assign("ret_post_search", $_POST['search_form']);
}


// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$gallery_details = $gallery->getGallery($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($gallery_details);
	
	$gallery->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);
	
	$_SESSION['message']['good_message'] = "Status został pomyślnie ustawiony";
			
	header("location: /_panel/gallery/index/1");
	
}


// --------------------------------------------------------------
// ustawia jako na stronę główną / lub nie
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "home") {
	
	//Najpierw wyciagamy dane o artykule
	$gallery_details = $gallery->getGallery($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($gallery_details);
	
	$gallery->setHome($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);
	
	if($url_config['4'] == "2"){
		
		$_SESSION['message']['good_message'] = "Gratulacje. Artykuł został pomyślnie dodany na stronę główną.";
	}
	else{
		
		$_SESSION['message']['good_message'] = "Gratulacje. Artykuł został pomyślnie usunięty ze strony głównej.";
	}
			
	header("location: /_panel/gallery/index/1");
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveGallery") {
	
	//Walidacja url_name
	require_once('validate_save_gallery.inc.php');
			
	if (!sizeof($error)) {
			
			// zapisujemy artykuł
			$GalleryId = $gallery->saveGallery($_POST['gallery_form']);
			
			if(!$_POST['staff_form']['gallery_id']){
				
				$_SESSION['message']['good_message'] = "Artykuł został pomyślnie dodany";
			}
			else{
				
				$_SESSION['message']['good_message'] = "Artykuł został pomyślnie zapisany";
			}
			
			
			
			
			
			header("location: /_panel/gallery/index/".$_POST['gallery_form']['category_id']);
						
		
	}
	//Błędy
	else{
		
		$smarty->assign("error", $error['gallery']);
		$url_config['2'] = "edit";
		//print_r($error);
		
		
	}	
	
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	// usuwamy artykuł
	$gallery->removeGallery($url_config['3']);
	$_SESSION['message']['good_message'] = "Galeria została usunięta!";
	header("location: /_panel/gallery/index");
}

// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('gallery','id', 'order', 'temp', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	header("location: /_panel/gallery/index/1");
}

if($url_config['2'] == "down" && $url_config['3']) {
	
	require_once('Order.class.php');
	$kolejnosc = new Order('gallery','id', 'order', 'temp', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	header("location: /_panel/gallery/index/1");
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['GallerySetOrder'] = $set_sort['0'];
	$_SESSION['GallerySetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['GallerySetOrder'] = "order";
	$_SESSION['GallerySetDirection'] = "asc";
}
$smarty->assign("sort_order", $_SESSION['GallerySetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['GallerySetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchGallery") {
	$_SESSION['GalleryFilters'] = $_POST['search_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($_REQUEST['action'] == "ClearSearch") {
	unset($_SESSION['GalleryFilters']);
}

// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['4'])) {
		$url_config['4'] = 1;
	}
	
	// ustawienie id kategorii (jezeli nie została podana)
	if (!isset($url_config['3'])) {
		
		//jest juz poprzednia
		if(!$_SESSION['GalleryCategoryId']){
		
			$_SESSION['GalleryCategoryId'] = 1;
		}
	}
	//zostala podana
	else{
		
		$_SESSION['GalleryCategoryId'] = $url_config['3'];
			
	}

	
	if (isset($_SESSION['GalleryFilters'])) {
		
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$gallerys_list = $gallery->getGallerysSearch($_SESSION['GallerySetOrder'], $_SESSION['GallerySetDirection'], $_SESSION['GalleryFilters'], $url_config['3'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		//echo"tak";
		
	}
	else {
		
		$gallerys_list = $gallery->getGallerys($_SESSION['GallerySetOrder'], $_SESSION['GallerySetDirection'], $_SESSION['GalleryCategoryId'], $url_config['4'], $_SESSION['lang']);

	}
	
	
	 //print_r($gallerys_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "gallery_list.tpl";
	$smarty->assign("gallerys_list", $gallerys_list);
	$smarty->assign("akcja", "gallery_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $url_config['3']);
	$smarty->assign("paging", $gallery->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	if($_SESSION['admin_data']['level'] == 1){
		$sw = new SPAW_Wysiwyg('gallery_form[content]' /*name*/, '' /*value*/,
	                   'pl' /*language*/, 'mini' /*toolbar mode*/, '' /*theme*/,
	                   '400px' /*width*/, '400px' /*height*/);
	}
	elseif($_SESSION['admin_data']['level'] == 2){
		$sw = new SPAW_Wysiwyg('gallery_form[content]' /*name*/, '' /*value*/,
	                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
	                   '400px' /*width*/, '400px' /*height*/);		
		
	}
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	$smarty->assign("akcja", "gallery_edit");	
	$template = "gallery_edit.tpl";
	$smarty->assign("category_id", $url_config['4']);
	$smarty->assign("language_id", $url_config['3']);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$gallery_details = $gallery->getGallery($url_config['3'], $url_config['4']);
	
	// print_r($sw);
	
	// wersje językowe do interfejsu
	$smarty->assign("akcja", "gallery_edit");	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "gallery_edit.tpl";
	$smarty->assign("gallery", $gallery_details);
	$smarty->assign("language_id", $gallery_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$gallery_details = $gallery->getGalleryDetails($_REQUEST['GalleryId'], $_REQUEST['LanguageId']);

	// print_r($gallery_details);
	
	$template = "gallery_details.tpl";
	$smarty->assign("gallery", $gallery_details);
}


//Przekazujemy info o domyslnej kategorii
if($_SESSION['GalleryCategoryId']){
	
	$smarty->assign("CategoryId", $_SESSION['GalleryCategoryId']);
	
}
else{
	
	$smarty->assign("CategoryId", 1);
}

?>