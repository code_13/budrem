<?php

require_once('Hate.class.php');
$hate = new Hate();


require_once('Category.class.php');
$category = new Category();

$hate_categories = $category->createCategoriesForAdmin();
$smarty->assign("hate_categories", $hate_categories);



require_once('Admin.class.php');
$admin = new Admin();

$admin_list = $admin->getAdmins("surname", "asc", 1);
$smarty->assign("admin_list", $admin_list);


//print_r($admin_list);
// print_r($_FILES);

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['hate_form']) {
	$smarty->assign("ret_post", $_POST['hate_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['search_form']) {
	$smarty->assign("ret_post_search", $_POST['search_form']);
}


// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$hate_details = $hate->getHate($url_config['3'], $_SESSION['lang']);

	if ($_SESSION['admin_data']['id'] == $hate_details['author_id'] || $_SESSION['admin_data']['level'] == 2){
	
		$hate->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
		//print_r($_REQUEST['action']);
		
		$_SESSION['message']['good_message'] = "Status został pomyślnie ustawiony";
		
	}
	else{
		
		$_SESSION['message']['bad_message'] = "Nie masz uprawnień do tego alertu";
		
	}			
	
	
	header("location: /_panel/hate/index/1");
	
}


// --------------------------------------------------------------
// wysyła powiadomienie o alercie wydawcy
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "send") {
	
	
	//Najpierw wyciagamy dane o artykule
	$hate_details = $hate->getHate($url_config['3'], $_SESSION['lang']);

	if ($_SESSION['admin_data']['id'] == $hate_details['author_id'] || $_SESSION['admin_data']['level'] == 2){
	
		$hate->setSend($url_config['3'], $url_config['4'], $_SESSION['lang']);
		//print_r($_REQUEST['action']);
		
		//Najpierw wyciagamy dane autora
		$admin_details = $admin->getAdmin($hate_details['author_id']);
	
		// jeżeli status ustawiliśmy na 1 (aktywny) to wysyłamy maila z informacją do obsługi serwisu gdzie wystepuje przemoc internetowa
		if ($url_config['4'] == 1) {
			$hate->sendActivationMessage($hate_details, $admin_details);
		}	
		
		
		$_SESSION['message']['good_message'] = "Wiadomość została wysłana";
	
	}
	else{
		
		$_SESSION['message']['bad_message'] = "Nie masz uprawnień do tego alertu";
		
	}
			
	header("location: /_panel/hate/index/1");
	
}


// --------------------------------------------------------------
// ustawia jako na stronę główną / lub nie
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "home") {
	
	//Najpierw wyciagamy dane o artykule
	$hate_details = $hate->getHate($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($hate_details);
	
	$hate->setHome($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);
	
	if($url_config['4'] == "2"){
		
		$_SESSION['message']['good_message'] = "Gratulacje. Artykuł został pomyślnie dodany na stronę główną.";
	}
	else{
		
		$_SESSION['message']['good_message'] = "Gratulacje. Artykuł został pomyślnie usunięty ze strony głównej.";
	}
			
	header("location: /_panel/hate/index/1");
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveHate") {
	
	//Walidacja url_name
	require_once('validate_save_hate.inc.php');
			
	if (!sizeof($error)) {
			
			// zapisujemy artykuł
			$HateId = $hate->saveHate($_POST['hate_form']);
			
			if(!$_POST['staff_form']['hate_id']){
				
				$_SESSION['message']['good_message'] = "Artykuł został pomyślnie dodany";
			}
			else{
				
				$_SESSION['message']['good_message'] = "Artykuł został pomyślnie zapisany";
			}
			
			
			
			
			
			header("location: /_panel/hate/index/1");
						
		
	}
	//Błędy
	else{
		
		$smarty->assign("error", $error['hate']);
		$url_config['2'] = "edit";
		//print_r($error);
		
		
	}	
	
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	// usuwamy artykuł
	$hate->removeHate($url_config['3']);
	
	$_REQUEST['action'] = "reindex";
}

// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('hate','id', 'order', 'temp', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	header("location: /_panel/hate/index/1");
}

if($url_config['2'] == "down" && $url_config['3']) {
	
	require_once('Order.class.php');
	$kolejnosc = new Order('hate','id', 'order', 'temp', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	header("location: /_panel/hate/index/1");
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['HateSetOrder'] = $set_sort['0'];
	$_SESSION['HateSetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['HateSetOrder'] = "order";
	$_SESSION['HateSetDirection'] = "desc";
}
$smarty->assign("sort_order", $_SESSION['HateSetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['HateSetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchHate") {
	$_SESSION['HateFilters'] = $_POST['search_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($_REQUEST['action'] == "ClearSearch") {
	unset($_SESSION['HateFilters']);
}

// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['3'])) {
		$url_config['3'] = 1;
	}
	
	
	//Jesli powrót do listy po jakiejś akcji
	if ($_REQUEST['action'] == "reindex"){
		
		$url_config['4'] = 1;
		$url_config['3'] = 1;
		
		
	}

	
	if (isset($_SESSION['HateFilters'])) {
		
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$hates_list = $hate->getHatesSearch($_SESSION['HateSetOrder'], $_SESSION['HateSetDirection'], $_SESSION['HateFilters'], $url_config['3'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		//echo"tak";
		
	}
	else {
		
		if($_SESSION['admin_data']['level'] == 2){
			$hates_list = $hate->getHates($_SESSION['HateSetOrder'], $_SESSION['HateSetDirection'], $url_config['3'], $_SESSION['lang']);
		}
		else{
			$hates_list = $hate->getHatesByViewAndCategoryToAdmin($_SESSION['HateSetOrder'], $_SESSION['HateSetDirection'], 10, $url_config['3'], $_SESSION['lang'], $_SESSION['admin_data']['category_id']);
		}
	}
	
	
	 //print_r($hates_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "hate_list.tpl";
	$smarty->assign("hates_list", $hates_list);
	$smarty->assign("akcja", "hate_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $url_config['3']);
	$smarty->assign("paging", $hate->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	if($_SESSION['admin_data']['level'] == 1){
		$sw = new SPAW_Wysiwyg('hate_form[content]' /*name*/, '' /*value*/,
	                   'pl' /*language*/, 'mini' /*toolbar mode*/, '' /*theme*/,
	                   '400px' /*width*/, '400px' /*height*/);
	}
	elseif($_SESSION['admin_data']['level'] == 2){
		$sw = new SPAW_Wysiwyg('hate_form[content]' /*name*/, '' /*value*/,
	                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
	                   '400px' /*width*/, '400px' /*height*/);		
		
	}
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	$smarty->assign("akcja", "hate_edit");	
	$template = "hate_edit.tpl";
	$smarty->assign("category_id", $url_config['4']);
	$smarty->assign("language_id", $url_config['3']);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$hate_details = $hate->getHate($url_config['3'], $url_config['4']);
	
	// print_r($sw);
	
	// wersje językowe do interfejsu
	$smarty->assign("akcja", "hate_edit");	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "hate_edit.tpl";
	$smarty->assign("hate", $hate_details);
	$smarty->assign("language_id", $hate_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$hate_details = $hate->getHateDetails($_REQUEST['HateId'], $_REQUEST['LanguageId']);

	// print_r($hate_details);
	
	$template = "hate_details.tpl";
	$smarty->assign("hate", $hate_details);
}

?>