<?php

require_once('House.class.php');
$house = new House();


require_once('Category.class.php');
$category = new Category();

$house_categories = $category->createCategoriesForAdmin();
$smarty->assign("house_categories", $house_categories);


// print_r($_POST);
// print_r($_FILES);

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['house_form']) {
	$smarty->assign("ret_post", $_POST['house_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['search_form']) {
	$smarty->assign("ret_post_search", $_POST['search_form']);
}


// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$house_details = $house->getHouse($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($house_details);
	
	$house->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);
	
	$_SESSION['message']['good_message'] = "Status został pomyślnie ustawiony";
			
	header("location: /_panel/house/index/1");
	
}


// --------------------------------------------------------------
// ustawia jako na stronę główną / lub nie
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "home") {
	
	//Najpierw wyciagamy dane o artykule
	$house_details = $house->getHouse($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($house_details);
	
	$house->setHome($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);
	
	if($url_config['4'] == "2"){
		
		$_SESSION['message']['good_message'] = "Gratulacje. Artykuł został pomyślnie dodany na stronę główną.";
	}
	else{
		
		$_SESSION['message']['good_message'] = "Gratulacje. Artykuł został pomyślnie usunięty ze strony głównej.";
	}
			
	header("location: /_panel/house/index/1");
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveHouse") {
	
	//Walidacja url_name
	require_once('validate_save_house.inc.php');
			
	if (!sizeof($error)) {
			
			// zapisujemy artykuł
			$HouseId = $house->saveHouse($_POST['house_form']);
			
			if(!$_POST['staff_form']['house_id']){
				
				$_SESSION['message']['good_message'] = "Artykuł został pomyślnie dodany";
			}
			else{
				
				$_SESSION['message']['good_message'] = "Artykuł został pomyślnie zapisany";
			}
			
			
			
			
			
			header("location: /_panel/house/index/1");
						
		
	}
	//Błędy
	else{
		
		$smarty->assign("error", $error['house']);
		$url_config['2'] = "edit";
		//print_r($error);
		
		
	}	
	
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	// usuwamy artykuł
	$house->removeHouse($url_config['3']);
	
	$_REQUEST['action'] = "reindex";
}

// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('house','id', 'order', 'temp', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	header("location: /_panel/house/index/1");
}

if($url_config['2'] == "down" && $url_config['3']) {
	
	require_once('Order.class.php');
	$kolejnosc = new Order('house','id', 'order', 'temp', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	header("location: /_panel/house/index/1");
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['HouseSetOrder'] = $set_sort['0'];
	$_SESSION['HouseSetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['HouseSetOrder'] = "order";
	$_SESSION['HouseSetDirection'] = "desc";
}
$smarty->assign("sort_order", $_SESSION['HouseSetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['HouseSetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchHouse") {
	$_SESSION['HouseFilters'] = $_POST['search_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($_REQUEST['action'] == "ClearSearch") {
	unset($_SESSION['HouseFilters']);
}

// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['3'])) {
		$url_config['3'] = 1;
	}
	
	
	//Jesli powrót do listy po jakiejś akcji
	if ($_REQUEST['action'] == "reindex"){
		
		$url_config['4'] = 1;
		$url_config['3'] = 1;
		
		
	}

	
	if (isset($_SESSION['HouseFilters'])) {
		
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$houses_list = $house->getHousesSearch($_SESSION['HouseSetOrder'], $_SESSION['HouseSetDirection'], $_SESSION['HouseFilters'], $url_config['3'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		//echo"tak";
		
	}
	else {
		
		if($_SESSION['admin_data']['level'] == 2){
			$houses_list = $house->getHouses($_SESSION['HouseSetOrder'], $_SESSION['HouseSetDirection'], $url_config['3'], $_SESSION['lang']);
		}
		else{
			$houses_list = $house->getHousesByViewAndCategoryToAdmin($_SESSION['HouseSetOrder'], $_SESSION['HouseSetDirection'], 10, $url_config['3'], $_SESSION['lang'], $_SESSION['admin_data']['category_id']);
		}
	}
	
	
	 //print_r($houses_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "house_list.tpl";
	$smarty->assign("houses_list", $houses_list);
	$smarty->assign("akcja", "house_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $url_config['3']);
	$smarty->assign("paging", $house->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	if($_SESSION['admin_data']['level'] == 1){
		$sw = new SPAW_Wysiwyg('house_form[content]' /*name*/, '' /*value*/,
	                   'pl' /*language*/, 'mini' /*toolbar mode*/, '' /*theme*/,
	                   '400px' /*width*/, '400px' /*height*/);
	}
	elseif($_SESSION['admin_data']['level'] == 2){
		$sw = new SPAW_Wysiwyg('house_form[content]' /*name*/, '' /*value*/,
	                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
	                   '400px' /*width*/, '400px' /*height*/);		
		
	}
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	$smarty->assign("akcja", "house_edit");	
	$template = "house_edit.tpl";
	$smarty->assign("category_id", $url_config['4']);
	$smarty->assign("language_id", $url_config['3']);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$house_details = $house->getHouse($url_config['3'], $url_config['4']);
	
	// print_r($sw);
	
	// wersje językowe do interfejsu
	$smarty->assign("akcja", "house_edit");	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "house_edit.tpl";
	$smarty->assign("house", $house_details);
	$smarty->assign("language_id", $house_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$house_details = $house->getHouseDetails($_REQUEST['HouseId'], $_REQUEST['LanguageId']);

	// print_r($house_details);
	
	$template = "house_details.tpl";
	$smarty->assign("house", $house_details);
}

?>