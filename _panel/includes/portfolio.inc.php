<?php

require_once('Portfolio.class.php');
$portfolio = new Portfolio();

// print_r($_POST);
// print_r($_FILES);


// --------------------------------------------------------------
// dynamiczne sortowanie
// --------------------------------------------------------------

if($url_config['2'] == "sortable"){
	if($_GET['listItem']){
		$order_list_temp = array();	
		foreach ($_GET['listItem'] as $position => $item){
			$key = $position + 1;
			$order_list_temp[$key] = $item;
			$portfolio->updateOrder($item, $key);
		}
	}	
	exit;
}

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['portfolio_form']) {
	$smarty->assign("ret_post", $_POST['portfolio_form']);
	
	//print_r($_POST['portfolio_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['portfolio_form']) {
	$smarty->assign("ret_post_search", $_POST['portfolio_form']);
}

// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------

if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$portfolio_details = $portfolio->getPortfolio($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($portfolio_details);
	
	$portfolio->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/portfolio/index/".$portfolio_details['category_id']);
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	//Najpierw wyciagamy dane o artykule
	$portfolio_details = $portfolio->getPortfolio($url_config['3'], $_SESSION['admin_data']['language']);	
	
	// usuwamy artykuł
	$portfolio->removePortfolio($url_config['3']);
	
	$_SESSION['message']['good_message'] = "Artykuł został usunięty";
	header("location: /_panel/portfolio/index/".$portfolio_details['category_id']);	
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SavePortfolio") {
	
	// echo "save portfolio";
	
	if (1) {
		if (isset($_POST['portfolio_form'])) {


					
			if (!sizeof($error)) {			
			
			
				// zapisujemy artykuł
				$PortfolioId = $portfolio->savePortfolio($_POST['portfolio_form']);
				
				if(!$_POST['portfolio_form']['portfolio_id']){
					
					$_SESSION['message']['good_message'] = "Zdjęcie zostało pomyślnie dodane";
				}
				else{
					
					$_SESSION['message']['good_message'] = "Zdjęcie zostało pomyślnie zapisane";
				}
				
				$smarty->assign("close", "1");
				$smarty->assign("location", "/_panel/portfolio/index/".$_POST['portfolio_form']['category_id']);
				$_REQUEST['action'] = "CreateView";

			}
		}
	}
}



// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('portfolio','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	$_SESSION['message']['good_message'] = "Kolejność została pomyślnie ustawiona";
	header("location: /_panel/portfolio/index/".$url_config['4']);
}

if($url_config['2'] == "down" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('portfolio','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	$_SESSION['message']['good_message'] = "Kolejność została pomyślnie ustawiona";
	header("location: /_panel/portfolio/index/".$url_config['4']);
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

// domyślne ustawienia
$_SESSION['PortfolioSetOrder'] = "order";
$_SESSION['PortfolioSetDirection'] = "asc";

$smarty->assign("sort_order", $_SESSION['PortfolioSetOrder']);
$smarty->assign("sort_direction", $sort_direction); 



// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!($url_config['4'])) {
		$url_config['4'] = 1;
	}

	// ustawienie id wg id housea
	$_SESSION['PortfolioCategoryId'] = $url_config['3'];
	$portfolio_list = $portfolio->getPortfolios($_SESSION['PortfolioSetOrder'], $_SESSION['PortfolioSetDirection'], $url_config['4'], $_SESSION['PortfolioCategoryId'], $_SESSION['lang']);
	
	//Szczegoly wpisu houseowego
	require_once('House.class.php');
	$house = new House();	
	
	$gallery_details = $house->getHouse($url_config['3'], $_SESSION['lang']);
	$smarty->assign("gallery_details", $gallery_details);
	//print_r($gallery_details);	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "portfolio_list.tpl";
	$smarty->assign("portfolio_list", $portfolio_list);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $_SESSION['PortfolioCategoryId']);
	$smarty->assign("paging", $portfolio->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	$template = "portfolio_edit.tpl";

	$smarty->assign("category_id", $_SESSION['PortfolioCategoryId']);
	$smarty->assign("language_id", 1);

}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	
	// dane do edycji
	$portfolio_details = $portfolio->getPortfolio($url_config['3'], $_SESSION['lang']);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "portfolio_edit.tpl";
	$smarty->assign("portfolio", $portfolio_details);
	$smarty->assign("language_id", $portfolio_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $_SESSION['PortfolioCategoryId']);
	//print_r($languages_list);
}



?>