<?php

require_once('Product.class.php');
$product = new Product();

// print_r($_POST);
// print_r($_FILES);

//wyciagamy ostatnie rekordy w zaleznosci od kategorii - przepisy, miejsca, specjalisci, sklepy itp


// --------------------------------------------------------------
// dynamiczne sortowanie
// --------------------------------------------------------------

if($url_config['2'] == "sortable"){
	
	if($_GET['listItem']){
	
	
		$order_list_temp = array();	
			//print_r($_GET['listItem']);
		foreach ($_GET['listItem'] as $position => $item){
			
			$key = $position + 1;
			$order_list_temp[$key] = $item;

			
			$product->updateOrders($item, $key);
			
		}

		$smarty->assign("good_message", "Kolejność została ustalona");
		
		$smarty->display("good_message.tpl");

	}		
	
	exit;

}

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['product_form']) {
	$smarty->assign("ret_post", $_POST['product_form']);
	
	//print_r($_POST['product_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['product_form']) {
	$smarty->assign("ret_post_search", $_POST['product_form']);
}

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchProduct") {
	$_SESSION['ProductFilters'] = $_POST['product_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($url_config['2'] == "clear") {
	unset($_SESSION['ProductFilters']);
	header("location: /_panel/product/index");
}



// --------------------------------------
// wysyłka artykułu mailem 
// --------------------------------------

if ($url_config['2'] == "send") {
	
	$product_details = $product->getProductDetails($url_config['3'], $_SESSION['admin_data']['language']);
	
	require_once('Newsletter.class.php');
	$newsletter = new Newsletter();
	
	$subscriber_list = $newsletter->getSubscribers($_SESSION['admin_data']['language'], 1);
	
	
	if($subscriber_list){
		
		foreach($subscriber_list as $subs){
			
			$product_details['sub_details'] = $subs;
			
			
			$product_sent = $product->sendProductByEmail($product_details, $subs['email']);
			
		}
		
		
		
	}
	
	
	//print_r($subscriber_list);

	if ($product_sent) {
		//Oznaczamy artykuł jako wyslany
		$product->setSended($product_details['product_id']);
		
	$_SESSION['message']['good_message'] = "Artykuł został wysłany";
	header("location: /_panel/product/index/".$product_details['category_id']);
	}
}


// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$product_details = $product->getProduct($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($product_details);
	
	$product->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/product/index/".$product_details['category_id']);
	
}

// --------------------------------------------------------------
// ustawia promocje
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "promotion") {
	
	//Najpierw wyciagamy dane o artykule
	$product_details = $product->getProduct($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($product_details);
	
	$product->setPromotion($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/product/index/".$product_details['category_id']);
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	//Najpierw wyciagamy dane o artykule
	$product_details = $product->getProduct($url_config['3'], $_SESSION['admin_data']['language']);	
	
	// usuwamy artykuł
	$product->removeProduct($url_config['3']);
	
	$_SESSION['message']['good_message'] = "Artykuł został usunięty";
	header("location: /_panel/product/index/".$product_details['category_id']);	
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveProduct") {
	
	// echo "save product";
	
	if (1) {
		if (isset($_POST['product_form'])) {
			
				
					
	
			//Walidacja url_name
			require_once('validate_save_product.inc.php');
					
			if (!sizeof($error)) {			
			
			
				// zapisujemy artykuł
				$ProductId = $product->saveProduct($_POST['product_form']);
				
				if(!$_POST['product_form']['product_id']){
					
					$_SESSION['message']['good_message'] = "Artykuł został pomyślnie dodany";
				}
				else{
					
					$_SESSION['message']['good_message'] = "Artykuł został pomyślnie zapisany";
				}
				
				
				
				
				
				header("location: /_panel/product/index/".$_POST['product_form']['category_id']);
			
			
				
			}
			//Błędy
			else{
				
				$smarty->assign("error", $error['product']);
				$url_config['2'] = "edit";
				//print_r($error);
				
				
			}
			

		}
	}
}



// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('product','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	$url_config['3'] = $url_config['4'];
	$_REQUEST['action'] = "reindex";
}

if($url_config['2'] == "down" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('product','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	$url_config['3'] = $url_config['4'];
	$_REQUEST['action'] = "reindex";
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['ProductSetOrder'] = $set_sort['0'];
	$_SESSION['ProductSetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['ProductSetOrder'] = "order";
	$_SESSION['ProductSetDirection'] = "asc";
}
$smarty->assign("sort_order", $_SESSION['ProductSetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['ProductSetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 



// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['4'])) {
		$url_config['4'] = 1;
	}
	
	// ustawienie id kategorii (jezeli nie została podana)
	if (!isset($url_config['3'])) {
		
		//jest juz poprzednia
		if(!$_SESSION['ProductCategoryId']){
		
			$_SESSION['ProductCategoryId'] = 1;
		}
	}
	//zostala podana
	else{
		
		$_SESSION['ProductCategoryId'] = $url_config['3'];
			
	}
	
	
	//Jesli powrót do listy po jakiejś akcji
	if ($_REQUEST['action'] == "reindex"){
		
		$url_config['4'] = 1;
		
		
		
	}

	
	if (isset($_SESSION['ProductFilters'])) {
		
		$_SESSION['ProductFilters']['category_id'] = $_SESSION['ProductCategoryId'];
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$products_list = $product->getProductsSearch($_SESSION['ProductSetOrder'], $_SESSION['ProductSetDirection'], $_SESSION['ProductFilters'], $url_config['4'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		
		$smarty->assign("ret_filters", $_SESSION['ProductFilters']);
	}
	else {
		$products_list = $product->getProducts($_SESSION['ProductSetOrder'], $_SESSION['ProductSetDirection'], $url_config['4'], $_SESSION['ProductCategoryId'], $_SESSION['lang']);
	
	}
	
	
	 //print_r($products_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "product_list.tpl";
	$smarty->assign("products_list", $products_list);
	$smarty->assign("akcja", "product_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $_SESSION['ProductCategoryId']);
	$smarty->assign("paging", $product->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('product_form[content]' /*name*/, '' /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '400px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	
	$template = "product_edit.tpl";
	
	$smarty->assign("akcja", "product_edit");
	
	$smarty->assign("category_id", $_SESSION['ProductCategoryId']);
	$smarty->assign("language_id", 1);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$product_details = $product->getProduct($url_config['3'], $url_config['4']);

	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('product_form[content]' /*name*/, $product_details['content'] /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '200px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	
	 //print_r($product_details);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "product_edit.tpl";
	$smarty->assign("product", $product_details);
	$smarty->assign("language_id", $product_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	$smarty->assign("category_id", $_SESSION['ProductCategoryId']);
	$smarty->assign("akcja", "product_edit");
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$product_details = $product->getProductDetails($_REQUEST['ProductId'], $_REQUEST['LanguageId']);

	// print_r($product_details);
	
	$template = "product_details.tpl";
	$smarty->assign("product", $product_details);
}

//Przekazujemy info o domyslnej kategorii
if($_SESSION['ProductCategoryId']){
	
	$smarty->assign("CategoryId", $_SESSION['ProductCategoryId']);
	
}
else{
	
	$smarty->assign("CategoryId", 1);
}



?>