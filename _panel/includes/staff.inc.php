<?php

require_once('Staff.class.php');
$staff = new Staff();

// print_r($_POST);
// print_r($_FILES);






// --------------------------------------------------------------
// dynamiczne sortowanie
// --------------------------------------------------------------

if($url_config['2'] == "sortable"){
	
	if($_GET['listItem']){
	
	
		$order_list_temp = array();	
			
		foreach ($_GET['listItem'] as $position => $item){
			
			$key = $position + 1;
			$order_list_temp[$key] = $item;

			
			$staff->updateOrder($item, $key);
			
		}

	}	
	
	
	
	exit;

}

// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['staff_form']) {
	$smarty->assign("ret_post", $_POST['staff_form']);
	
	//print_r($_POST['staff_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['staff_form']) {
	$smarty->assign("ret_post_search", $_POST['staff_form']);
}

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchStaff") {
	$_SESSION['StaffFilters'] = $_POST['staff_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($url_config['2'] == "clear") {
	unset($_SESSION['StaffFilters']);
	header("location: /_panel/staff/index");
}
// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$staff_details = $staff->getStaff($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($staff_details);
	
	$staff->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/staff/index/".$staff_details['category_id']);
	
}

// --------------------------------------------------------------
// ustawia na strone glowna
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "home") {
	
	//Najpierw wyciagamy dane o artykule
	$staff_details = $staff->getStaff($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($staff_details);
	
	$staff->setHome($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status home został zmieniony";
	header("location: /_panel/staff/index/".$staff_details['category_id']);
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	//Najpierw wyciagamy dane o artykule
	$staff_details = $staff->getStaff($url_config['3'], $_SESSION['admin_data']['language']);	
	
	// usuwamy artykuł
	$staff->removeStaff($url_config['3']);
	
	$_SESSION['message']['good_message'] = "Artykuł został usunięty";
	header("location: /_panel/staff/index/".$staff_details['category_id']);	
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveStaff") {
	
	// echo "save staff";
	
	if (1) {
		if (isset($_POST['staff_form'])) {
			
				
					
	
			//Walidacja url_name
			require_once('validate_save_staff.inc.php');
					
			if (!sizeof($error)) {			
			
			
				// zapisujemy artykuł
				$StaffId = $staff->saveStaff($_POST['staff_form']);
				
				if(!$_POST['staff_form']['staff_id']){
					
					$_SESSION['message']['good_message'] = "Artykuł został pomyślnie dodany";
				}
				else{
					
					$_SESSION['message']['good_message'] = "Artykuł został pomyślnie zapisany";
				}
				
				
				
				
				
				header("location: /_panel/staff/index/".$_POST['staff_form']['category_id']);
			
			
				
			}
			//Błędy
			else{
				
				$smarty->assign("error", $error['staff']);
				$url_config['2'] = "edit";
				//print_r($error);
				
				
			}
			

		}
	}
}



// -------------------------------------------------------
// ustawianie kolejności w ramach kategorii
// -------------------------------------------------------

if($url_config['2'] == "up" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('staff','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->down($url_config['3']);
	$url_config['3'] = $url_config['4'];
	$_REQUEST['action'] = "reindex";
}

if($url_config['2'] == "down" && $url_config['3'] && $url_config['4']) {
	require_once('Order.class.php');
	$kolejnosc = new Order('staff','id', 'order', 'category_id', $url_config['4']);
	$kolejnosc->up($url_config['3']);
	$url_config['3'] = $url_config['4'];
	$_REQUEST['action'] = "reindex";
}

// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['StaffSetOrder'] = $set_sort['0'];
	$_SESSION['StaffSetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['StaffSetOrder'] = "order";
	$_SESSION['StaffSetDirection'] = "asc";
}
$smarty->assign("sort_order", $_SESSION['StaffSetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['StaffSetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 



// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['4'])) {
		$url_config['4'] = 1;
	}
	
	// ustawienie id kategorii (jezeli nie została podana) - admin główny	
	$_SESSION['StaffCategoryId'] = 1;
	
	//Jesli powrót do listy po jakiejś akcji
	if ($_REQUEST['action'] == "reindex"){
		
		$url_config['4'] = 1;
		
		
		
	}

	
	if (isset($_SESSION['StaffFilters'])) {
		
		$_SESSION['StaffFilters']['category_id'] = $_SESSION['StaffCategoryId'];
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$staffs_list = $staff->getStaffsSearch($_SESSION['StaffSetOrder'], $_SESSION['StaffSetDirection'], $_SESSION['StaffFilters'], $url_config['4'], $_SESSION['lang']);
		$smarty->assign("set_filter", "1");
		
		$smarty->assign("ret_filters", $_SESSION['StaffFilters']);
	}
	else {
		$staffs_list = $staff->getStaffs($_SESSION['StaffSetOrder'], $_SESSION['StaffSetDirection'], $url_config['4'], $_SESSION['StaffCategoryId'], $_SESSION['lang']);
	
	}
	
	
	 //print_r($staffs_list);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "staff_list.tpl";
	$smarty->assign("staffs_list", $staffs_list);
	$smarty->assign("akcja", "staff_list");
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign("category_id", $_SESSION['StaffCategoryId']);
	$smarty->assign("paging", $staff->paging);
}


// --------------------------------------------------------------
// nowy artykuł
// --------------------------------------------------------------

if ($url_config['2'] == "new") {
	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('staff_form[content]' /*name*/, '' /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '400px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	
	// wersje językowe do interfejsu
	/*
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->_all_languages;
	*/
	
	$template = "staff_edit.tpl";
	
	$smarty->assign("akcja", "staff_edit");
	
	$smarty->assign("category_id", $_SESSION['StaffCategoryId']);
	$smarty->assign("language_id", 1);
	// $smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	$smarty->assign('sSpaw2', $sSpaw2);
}

// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$staff_details = $staff->getStaff($url_config['3'], $url_config['4']);

	
	// edytor WYSIWYG
	require_once('spaw/spaw_control.class.php');
	$sw = new SPAW_Wysiwyg('staff_form[content]' /*name*/, $staff_details['content'] /*value*/,
                   'pl' /*language*/, 'default' /*toolbar mode*/, '' /*theme*/,
                   '200px' /*width*/, '400px' /*height*/);
	$sSpaw = $sw->getHtml();
	

	
	// print_r($sw);
	
	// wersje językowe do interfejsu
	
	require_once('Language.class.php');
	$language = new Language();
	$languages_list = $language->getLanguages();
	//print_r($languages_list);
	
	$template = "staff_edit.tpl";
	$smarty->assign("staff", $staff_details);
	$smarty->assign("language_id", $staff_details['language_id']);
	$smarty->assign("languages_list", $languages_list);
	$smarty->assign('sSpaw', $sSpaw);
	$smarty->assign('sSpaw2', $sSpaw2);
	$smarty->assign("category_id", $_SESSION['StaffCategoryId']);
	$smarty->assign("akcja", "staff_edit");
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$staff_details = $staff->getStaffDetails($_REQUEST['StaffId'], $_REQUEST['LanguageId']);

	// print_r($staff_details);
	
	$template = "staff_details.tpl";
	$smarty->assign("staff", $staff_details);
}

//Przekazujemy info o domyslnej kategorii
if($_SESSION['StaffCategoryId']){
	
	$smarty->assign("CategoryId", $_SESSION['StaffCategoryId']);
	
}
else{
	
	$smarty->assign("CategoryId", 1);
}



?>