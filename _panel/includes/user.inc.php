<?php

require_once('User.class.php');
$user = new User();

// print_r($_POST);
// print_r($_FILES);



// --------------------------------------------------------------
// zwracamy zawartość formularza edycji do widoku
// --------------------------------------------------------------

if($_POST['user_form']) {
	$smarty->assign("ret_post", $_POST['user_form']);
	
	//print_r($_POST['user_form']);
}

// --------------------------------------------------------------
// zwracamy zawartość formularza wyszukiwania do widoku
// --------------------------------------------------------------

if($_POST['user_form']) {
	$smarty->assign("ret_post_search", $_POST['user_form']);
}

// --------------------------------------------------------------
// ustawienie filtrów w sesji - dla wyszukiwania artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SearchUser") {
	$_SESSION['UserFilters'] = $_POST['user_form'];
}

// --------------------------------------------------------------
// usunięcie filtrów z sesji
// --------------------------------------------------------------

if ($url_config['2'] == "clear") {
	unset($_SESSION['UserFilters']);
	header("location: /_panel/user/index");
}
// --------------------------------------------------------------
// ustawia jako aktywny/nieaktywny
// --------------------------------------------------------------
//print_r($url_config);



if ($url_config['2'] == "status") {
	
	//Najpierw wyciagamy dane o artykule
	$user_details = $user->getUser($url_config['3'], $_SESSION['admin_data']['language']);
	//print_r($user_details);
	
	$user->setStatus($url_config['3'], $url_config['4'], $_SESSION['admin_data']['language']);
	//print_r($_REQUEST['action']);

	
	$_SESSION['message']['good_message'] = "Status został zmieniony";
	header("location: /_panel/user/index");
	
}

// --------------------------------------------------------------
// usunięcie artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "remove" ) {
	
	
	
	// usuwamy artykuł
	$user->removeUser($url_config['3']);
	
	$_SESSION['message']['good_message'] = "Artykuł został usunięty";
	header("location: /_panel/user/index");	
	
}

// --------------------------------------------------------------
// zapisanie artykułu
// --------------------------------------------------------------

if ($_REQUEST['action'] == "SaveUser") {
	
	// echo "save user";
	
	if (1) {
		if (isset($_POST['user_form'])) {
			
				
					
	
			//Walidacja url_name
			require_once('validate_save_user.inc.php');
					
			if (!sizeof($error)) {			
			
			
				// zapisujemy artykuł
				$UserId = $user->updateUserForAdmin($_POST['user_form']);
				
				if(!$_POST['user_form']['id']){
					
					$_SESSION['message']['good_message'] = "Użytkownik został pomyślnie dodany";
				}
				else{
					
					$_SESSION['message']['good_message'] = "Użytkownik został pomyślnie zapisany";
				}
				
				
				
				
				
				header("location: /_panel/user/edit/".$_POST['user_form']['id']);
			
			
				
			}
			//Błędy
			else{
				
				$smarty->assign("error", $error['user']);
				$url_config['2'] = "edit";
				//print_r($error);
				
				
			}
			

		}
	}
}



// --------------------------------------------------------------
// ustawienie sortowania w sesji
// --------------------------------------------------------------

if ($_REQUEST['SetSort']) {
	$set_sort = explode(",",$_REQUEST['SetSort']);
	
	/*
	print_r($set_sort);
	echo $set_sort[0];
	*/
	
	$_SESSION['UserSetOrder'] = $set_sort['0'];
	$_SESSION['UserSetDirection'] = $set_sort['1'];
}
else {
	// domyślne ustawienia
	$_SESSION['UserSetOrder'] = "name";
	$_SESSION['UserSetDirection'] = "asc";
}
$smarty->assign("sort_order", $_SESSION['UserSetOrder']);

// uwaga! do szablonu ustawiamy przeciwny sposób sortowania (dla wybranego pola wg. którego sortujemy)
$sort_direction = ($_SESSION['UserSetDirection'] == "asc")?"desc":"asc";
$smarty->assign("sort_direction", $sort_direction); 



// --------------------------------------------------------------
// lista artykułów
// --------------------------------------------------------------

if ($_REQUEST['action'] == "reindex" || !$url_config['2'] || $url_config['2'] == "index" || $url_config['2'] == "Search" || $url_config['2'] == "ClearSearch") {
	
	// ustawienie numeru strony do stronicowania (jezeli nie została podana)
	if (!isset($url_config['3'])) {
		$url_config['3'] = 1;
	}


	
	if (isset($_SESSION['UserFilters'])) {
		
		// jeżeli są w sesji ustawione filtry, to zawsze pokazujemy listę przefiltrowaną
		$users_list = $user->getUsersSearch($_SESSION['UserSetOrder'], $_SESSION['UserSetDirection'], $_SESSION['UserFilters'], $url_config['3']);
		$smarty->assign("set_filter", "1");
		
		$smarty->assign("ret_filters", $_SESSION['UserFilters']);
	}
	else {
		
		$users_list = $user->getUsers($_SESSION['UserSetOrder'], $_SESSION['UserSetDirection'], $url_config['3']);
	
	}
	
	//print_r($users_list);
	
	$template = "user_list.tpl";
	$smarty->assign("users_list", $users_list);
	$smarty->assign("akcja", "user_list");
	$smarty->assign("paging", $user->paging);
}



// --------------------------------------------------------------
// edycja artykułu
// --------------------------------------------------------------

if ($url_config['2'] == "edit") {
	//print_r($url_config);
	// dane do edycji
	$user_details = $user->getUser($url_config['3'], $url_config['4']);
	
	$template = "user_edit.tpl";
	$smarty->assign("user", $user_details);
	$smarty->assign("language_id", $user_details['language_id']);
	$smarty->assign("category_id", $_SESSION['CategoryId']);
	$smarty->assign("akcja", "user_edit");
	//print_r($languages_list);
}

// --------------------------------------------------------------
// dane artykułu - to może być wykorzystywane tylko w podglądzie?
// --------------------------------------------------------------

if ($_REQUEST['action'] == "DetailView") {
	
	// dane do przeglądania
	$user_details = $user->getUserDetails($_REQUEST['UserId'], $_REQUEST['LanguageId']);

	// print_r($user_details);
	
	$template = "user_details.tpl";
	$smarty->assign("user", $user_details);
}

//Przekazujemy info o domyslnej kategorii
if($_SESSION['CategoryId']){
	
	$smarty->assign("CategoryId", $_SESSION['CategoryId']);
	
}
else{
	
	$smarty->assign("CategoryId", 1);
}



?>