<?
	include('includes/default.php');
	

	// -------------------------------------------------------
	// Powiadomienia
	// -------------------------------------------------------

	//Powodzenie
	if($_SESSION['message']['good_message']){
		
		$smarty->assign("good_message", $_SESSION['message']['good_message']);
		unset($_SESSION['message']['good_message']);
		
	}
	
	//Błąd
	if($_SESSION['message']['bad_message']){
		
		$smarty->assign("bad_message", $_SESSION['message']['bad_message']);
		unset($_SESSION['message']['bad_message']);
		
	}	
	
	
	//Tutaj musimy zrobic rozdzielenie poszczegolnych działów w zalezności od $url_config
	
	
	//Zabezpieczenie przed niezalogowaniem
	if ($_SESSION['admin_data']['id']) {
		
		
		
		
		//Jeśli nie wybrano działu - kierujemy do artykułów	
		if(!$url_config['1']){
			$url_config['1'] = "hate";
		
		}		
		
		// -----------------------------------------------------------
		// Artykuły
		// -----------------------------------------------------------
		if($url_config['1'] == "article"){
			include('includes/article.inc.php');
			if (!$template) {
				$template = "article_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', $dict_templates['Articles']);
			$smarty->assign("script_name", "article");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Firmy współpracujące
		// -----------------------------------------------------------
		elseif($url_config['1'] == "company"){
			include('includes/company.inc.php');
			if (!$template) {
				$template = "company_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', $dict_templates['Articles']);
			$smarty->assign("script_name", "company");
			$smarty->display($template);
			
		}
        // -----------------------------------------------------------
        // Produkty - cennik etc
        // -----------------------------------------------------------
        elseif($url_config['1'] == "product"){
            include('includes/product.inc.php');
            if (!$template) {
                $template = "product_list.tpl";
            }
            $smarty->assign('nazwa_dzialu', $dict_templates['Articles']);
            $smarty->assign("script_name", "product");
            $smarty->display($template);
            
        }	
		// -----------------------------------------------------------
		// Administratorzy
		// -----------------------------------------------------------
		elseif($url_config['1'] == "admin"){
			include('includes/admin.inc.php');
			if (!$template) {
				$template = "admin_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Administratorzy");
			$smarty->assign("script_name", "admin");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Kategorie miejsc
		// -----------------------------------------------------------
		elseif($url_config['1'] == "category"){
			include('includes/category.inc.php');
			if (!$template) {
				$template = "category_list.tpl";
			}
			$smarty->assign('nazwa_dzialu',"Cechy");
			$smarty->assign("script_name", "category");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Miejscowości
		// -----------------------------------------------------------
		elseif($url_config['1'] == "city"){
			include('includes/city.inc.php');
			if (!$template) {
				$template = "city_list.tpl";
			}
			$smarty->assign('nazwa_dzialu',"Cechy");
			$smarty->assign("script_name", "city");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Zdjęcia blogowe
		// -----------------------------------------------------------
		elseif($url_config['1'] == "picture"){
			include('includes/picture.inc.php');
			if (!$template) {
				$template = "picture_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Zdjęcia");
			$smarty->assign("script_name", "picture");
			$smarty->display($template);
			
		}
        // -----------------------------------------------------------
        // Zdjęcia apartamentow
        // -----------------------------------------------------------
        elseif($url_config['1'] == "photo"){
            include('includes/photo.inc.php');
            if (!$template) {
                $template = "photo_list.tpl";
            }
            $smarty->assign('nazwa_dzialu', "Zdjęcia");
            $smarty->assign("script_name", "photo");
            $smarty->display($template);

        }
		// -----------------------------------------------------------
		// Zdjęcia domów
		// -----------------------------------------------------------
		elseif($url_config['1'] == "portfolio"){
			include('includes/portfolio.inc.php');
			if (!$template) {
				$template = "portfolio_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Zdjęcia");
			$smarty->assign("script_name", "portfolio");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Obsługa
		// -----------------------------------------------------------
		elseif($url_config['1'] == "staff"){
			include('includes/staff.inc.php');
			if (!$template) {
				$template = "staff_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Obsługa");
			$smarty->assign("script_name", "staff");
			$smarty->display($template);
			
		}
		//-----------------------------------------------------------
		// Slideshow
		//-----------------------------------------------------------
		elseif($url_config['1'] == "slideshow"){
			include('includes/slideshow.inc.php');
			if (!$template) {
				$template = "slideshow_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Slideshow");
			$smarty->assign("script_name", "slideshow");
			$smarty->display($template);
			
		}	
		// -----------------------------------------------------------
		// Blog
		// -----------------------------------------------------------
		elseif($url_config['1'] == "blog"){
			include('includes/blog.inc.php');
			if (!$template) {
				$template = "blog_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Blog");
			$smarty->assign("script_name", "blog");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Domy na sprzedaż
		// -----------------------------------------------------------
		elseif($url_config['1'] == "house"){
			include('includes/house.inc.php');
			if (!$template) {
				$template = "house_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "house");
			$smarty->assign("script_name", "house");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Galeria
		// -----------------------------------------------------------
		elseif($url_config['1'] == "gallery"){
			include('includes/gallery.inc.php');
			if (!$template) {
				$template = "gallery_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Galeria");
			$smarty->assign("script_name", "gallery");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Zgłoszenia
		// -----------------------------------------------------------
		elseif($url_config['1'] == "hate"){
			include('includes/hate.inc.php');
			if (!$template) {
				$template = "hate_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Zgłoszenia");
			$smarty->assign("script_name", "hate");
			$smarty->display($template);
			
		}
		// -----------------------------------------------------------
		// Filmy wideo
		// -----------------------------------------------------------
		elseif($url_config['1'] == "video"){
			include('includes/video.inc.php');
			if (!$template) {
				$template = "video_list.tpl";
			}
			$smarty->assign('nazwa_dzialu', "Wideo");
			$smarty->assign("script_name", "video");
			$smarty->display($template);
			
		}				
		// -----------------------------------------------------------
		// Komentarze bloga
		// -----------------------------------------------------------
		elseif($url_config['1'] == "blog_comments"){
			if ($_SESSION['admin_data']['id']) {
				
				include('includes/blog_comments.inc.php');
				
				$smarty->assign('script_name', "blog_comments.php");
				$smarty->assign('nazwa_dzialu', "Komentarze Blog");
				
				// zabezpieczenie przed brakiem ustawienia szablonu
				if (!$template) {
					$template = "blog_comments.tpl";
				}
				
				$smarty->display($template);
			}
			else {
				
				header("Location: login.php");
			}
			
		}
		
		// -----------------------------------------------------------
		// Wylogowanie
		// -----------------------------------------------------------
		elseif($url_config['1'] == "logout"){
			
			$_REQUEST['action'] = "LogoutAdmin";
			include('includes/admin.inc.php');
			
		}

	
		
		// -----------------------------------------------------------
		// Parametry
		// -----------------------------------------------------------
		elseif($url_config['1'] == "parameter"){
			include('includes/parameter.inc.php');
			if (!$template) {
				$template = "parameter.tpl";
			}
			$smarty->assign('nazwa_dzialu', $dict_templates['Parameters']);
			$smarty->assign("script_name", "parameter");
			$smarty->display($template);
			
		}

		// -----------------------------------------------------------
		// Użytkownicy
		// -----------------------------------------------------------
		elseif($url_config['1'] == "user"){
			include('includes/user.inc.php');
			if (!$template) {
				$template = "user_list.tpl";
			}

			$smarty->assign("script_name", "user");
			$smarty->display($template);
			
		}		
			
		// -----------------------------------------------------------
		// Kontakt
		// -----------------------------------------------------------
		elseif($url_config['1'] == "kontakt"){
			include('includes/contact.inc.php');
			if (!$template) {
				$template = "contact_list.tpl";
			}
			$smarty->assign('nazwa_dzialu',"Kontakt");
			$smarty->assign("script_name", "contact");
			$smarty->display($template);
			
		}	
		
		
		elseif($url_config['1'] == "good_message"){
			
			
			$smarty->display("good_message.tpl");	
			
			
		}
		
		
		else{
		
			include('includes/_error.inc.php');
			
		}			
	
	
	
	}
	else{
		
		$smarty->display("login.tpl");
		
	}
	

	
	

?>