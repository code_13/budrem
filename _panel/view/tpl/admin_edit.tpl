<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="admin_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$admin.id}Dodaj admina{else}Edycja admina{/if}</h2>

							    
							    <form id="adminForm" method="post" enctype="multipart/form-data">
									<input type="hidden" name="admin_form[id]" value="{$admin.id}">
									<input type="hidden" name="action" value="SaveAdmin">
									
									    
							        <p>
							            <label for="admin_form[surname]">Nazwisko:</label>
							            <input class="input-medium" type="text" value="{$admin.surname|default:$ret_post.surname}" name="admin_form[surname]" id="admin_form[surname]"/>
							        </p>									    
							        <p>
							            <label for="admin_form[name]">Imię:</label>
							            <input class="input-medium" type="text" value="{$admin.name|default:$ret_post.name}" name="admin_form[name]" id="admin_form[name]"/>
							        </p>
							        <p>
							            <label for="admin_form[email]">Adres e-mail (login):</label>
							            <input class="input-medium" type="text" value="{$admin.email|default:$ret_post.email}" name="admin_form[email]" id="admin_form[email]"/>
							        </p>
							        <p>
							            <label for="password_string">Hasło:</label>
							            <input class="input-medium" type="text" value="{$admin.password|default:$ret_post.password}" name="admin_form[password]" id="password_string"/>
							        	<a href="#" onClick="getPassword('8','password_string');">generuj</a>
							        </p>
									<p>
	                                    <label for="selectbox">Wybierz miasto:</label>
							            <select style="width: 200px;" name="admin_form[category_id]" id="selectbox">
							            	<option value="">- wybierz -</option>
											{foreach from=$blog_categories item=category key=key}
											
											<option value="{$category.id}" {if $admin.category_id eq $category.id}selected{/if}>{$category.name}</option>
											{/foreach}
							            </select>							        
							        
							        </p> 
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="admin_form[status]" id="admin_form[status]" style="width: 240px;" >
											<option value="">- wybierz -</option>
											<option value="1" {if $admin.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $admin.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>	
							        <p>
							        	<input {if $admin.send_email eq 1}checked="checked"{/if} type="checkbox" value="1" name="admin_form[send_email]" id="admin_form[send_email]"/> - 
							            <label style="display: inline;" for="admin_form[send_email]">Wysyłaj powiadomienia e-mail na to konto</label><br/>								        
							        <p>							        
							        {if $admin.id eq 7}
							        <input type="hidden" name="admin_form[level]" value="2">
							        <p>Poziom uprawnień: Superadmin</p>
							        {else}
							        <p>
							            <label for="selectbox">Poziom uprawnień:</label>
							            <select name="admin_form[level]" id="admin_form[level]" style="width: 240px;" onchange="show_option(this.value);" >
											<option value="">- wybierz -</option>
											<option value="1" {if $admin.level eq 1}selected{/if}>Admin</option>
											<option value="2" {if $admin.level eq 2}selected{/if}>Superadmin</option>
											{*<option value="3" {if $admin.level eq 3}selected{/if}>Menedżer</option>*}
										
							            </select>
							        </p>							        
							        {/if} 
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


