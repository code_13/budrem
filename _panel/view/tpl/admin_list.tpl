<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
                            <div class="jquery_tab">
                            
                                <div class="content_block">
                                    <h2 class="jquery_tab_title">Lista administratorów</h2>
                                    
                                    {if $good_message}
                           		  	<div class="message success closeable">
    									<p><strong>Gratulacje!</strong> {$good_message}</p>
									</div>           
                                    {/if}
						            
						            <br/>

									<div style="display: none;" id="info"></div>
									
									<ul id="listtop">
										<li class="top">
											  
											  <div class="nametop">NAZWISKO I IMIĘ</div>
											  <div class="datetop">DATA</div>
											  
											  <div class="optionstop">OPCJE</div>
									    </li><div class="clrl"></div>
									</ul>
									<ul id="order-list">
									
									{if $admins_list}
									
									{foreach from=$admins_list item=admin name=admin}
									{cycle name=color assign=row_color values="#EFEFEF ,#EFEFEF"}
									
									{if $admin.id ne 2}
									  <li id="listItem_{$admin.id}" style="background-color: {$row_color}">

											  <div class="name" style="padding-left: 33px;">{$admin.surname} {$admin.name}</div>
											  <div class="date">{$admin.date_created|date_format:"%Y-%m-%d"}</div>
											  
											  <div class="options">
											  

													<a href="{$path}/admin/edit/{$admin.id}"><img src="{$path}/images/page_white_edit.png" border="0" title="{$dict_templates.Edit} ({$language.short})"/></a>&nbsp;
					
							                                       
													{if $admin.status eq 2}
													<a href="{$path}/admin/status/{$admin.id}/1" title="ustaw status niewidoczny"><img src="{$path}/images/delete.png" border="0" title="{$dict_templates.SetStatus}"></a>
													{else}
													<a href="{$path}/admin/status/{$admin.id}/2" title="ustaw status widoczny"><img src="{$path}/images/tick.png" border="0" title="{$dict_templates.SetStatus}"></a>
													{/if}                                 
													<a href="{$path}/admin/remove/{$admin.id}"><img src="{$path}/images/false.png" border="0" title="{$dict_templates.Remove}"></a>
	
																
									</div>
										<div class="clearboth"></div>
									  </li>
									  {/if}
									{/foreach}
									
									{else}
									
									<li>Brak wyników</li>
									
									{/if}
									
									</ul>
						            
								{*	
                                <div id="summary">PODSUMOWANIE</div>
                                *}
                                
                                <div class="clearboth"></div>  

                            	</div><!--end content_block-->

                            	
                            	
                            	
                            	
                                
                            </div><!--end jquery tab-->								
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     <div class="clearboth"></div>
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>