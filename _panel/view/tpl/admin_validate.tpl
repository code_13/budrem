{literal}

<script type="text/javascript">


$().ready(function() {

	
	// validate signup form on keyup and submit
	$("#adminForm").validate({
		rules: {
			"admin_form[surname]": {
				required: true
			},
			"admin_form[name]": {
				required: true
			},
			"admin_form[email]": {
				required: true, 
				email: true
			},
			"admin_form[password]": {
				required: true
			},
			"admin_form[status]": {
				required: true
			},	
			"admin_form[level]": {
				required: true
			}
		},
		messages: {
			"admin_form[surname]": {
				required: "Wymagane"
			},
			"admin_form[name]": {
				required: "Wymagane"
			},
			"admin_form[email]": {
				required: "Wymagane",
				email: "Podaj prawidłowy adres e-mail"
			},
			"admin_form[password]": {
				required: "Wymagane"
			},			
			"admin_form[status]": {
				required: "Wymagane"
			},			
			"admin_form[level]": {
				required: "Wymagane"
			}		
		}
		
		

	});
});

</script>

{/literal}

