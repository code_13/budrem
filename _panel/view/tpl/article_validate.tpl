{literal}

<script type="text/javascript">


$().ready(function() {

	
	// validate signup form on keyup and submit
	$("#articleForm").validate({
		rules: {
			"article_form[title]": {
				required: true
				
			},
			"article_form[url_name]": {
				required: true
				
			},
			"article_form[abstract]": {
				required: true
			}			
		},
		messages: {
			"article_form[title]": {
				required: "Proszę podać nazwę"
			},
			"article_form[url_name]": {
				required: "Proszę podać nazwę url"
			},
			"article_form[abstract]": {
				required: "Proszę podać opis"
			}
			
		}
		
		

	});
});

</script>

{/literal}

