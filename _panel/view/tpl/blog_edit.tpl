<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="blog_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$blog.id}Dodaj bloga{else}Edycja bloga{/if}</h2>
							    
							   
							    
							    
									<form id="blogForm" name="BlogEdit" method="post" enctype="multipart/form-data">
									
									<input type="hidden" name="blog_form[language_id]" value="1">
									<input type="hidden" name="blog_form[blog_id]" value="{$blog.blog_id}">
									<input type="hidden" name="action" value="SaveBlog">
									
									    
							        <p>
							            <label for="blog_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$blog.title|default:$ret_post.title}" name="blog_form[title]" id="blog_form[title]"/>
							        </p>
							      
							        
							        <p>
							            <label for="blog_form[url_name]">Nazwa url, generowana na podstawie tytułu.<br/> [musi być brak znaków polskich i specjalnych - widoczne po pierwszym zapisie]:</label>
							            <input class="input-big" type="text" value="{$blog.url_name|default:$ret_post.url_name}" name="blog_form[url_name]" id="blog_form[url_name]"/>
							            {if $error.url_name}<br/><span style="color: red;">Podany url_name już istnieje - zmień tytuł.</span>{/if}
							        </p>							        
							        
							        <p>
							            <label for="blog_form[youtube]">Link YouTube:</label>
							            <input class="input-big" type="text" value="{$blog.youtube|default:$ret_post.youtube}" name="blog_form[youtube]" id="blog_form[youtube]"/>
							        </p>	
							        <p>
										<label for="date">Data utworzenia:</label>
										<input class="input-small flexy_datepicker_input" type="text" value="{$blog.date_created|date_format:"%Y-%m-%d"|default:$ret_post.date_created|date_format:"%Y-%m-%d"}" name="blog_form[date_created]" id="date"/>	
							        </p>							        
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="blog_form[status]" id="blog_form[status]" >
											<option value="1" {if $blog.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $blog.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
									

							        
							        <input type="hidden" name="blog_form[category_id]" value="{$admin_data.category_id}">
							        
							       
							        <p>
							        	<label for="editor1">Zajawka:</label>
							        	<textarea id="editor1" name="blog_form[abstract]">{$blog.abstract}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor1', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
							        <p>
							        	<label for="editor2">Treść:</label>
							        	<textarea id="editor2" name="blog_form[content]">{$blog.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor2', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
									<p>
										<br/>
										<label for="pic_01">Grafika [800px/600px] - jpg, jpeg:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $blog.pic_01}
									<p>
										<img src="{$__CFG.base_url}images/blog/{$blog.blog_id}_01_01.jpg">
										<input type="checkbox" name="blog_form[remove_picture_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        

							        
							           
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


