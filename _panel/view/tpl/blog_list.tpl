<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
                            <div class="jquery_tab">
                            
                                <div class="content_block">
                                    <h2 class="jquery_tab_title">Lista bloga</h2>
                                    
                                    {if $good_message}
                           		  	<div class="message success closeable">
    									<p><strong>Gratulacje!</strong> {$good_message}</p>
									</div>           
                                    {/if}

						            {*
						            {include file="blog_filters.tpl"}
									*}
									



									
									{include file="blog_paging.tpl"}
									
									<div style="display: none;" id="info"></div>
									
									<ul id="listtop">
										<li class="top">
											  
											  <div class="nametop">TYTUŁ</div>
											  <div class="datetop">DATA</div>
											  
											  <div class="optionstop">OPCJE</div>
									    </li><div class="clrl"></div>
									</ul>
									<ul id="order-list">
									
									{if $blogs_list}
									
									{foreach from=$blogs_list item=blog name=blog}
									{cycle name=color assign=row_color values="#EFEFEF ,#EFEFEF"}
									
									
									  <li id="listItem_{$blog.blog_id}" style="background-color: {$row_color}">
											  {if $admin_data.level eq 2}	
											  <div style="float: left; width: 30px;">{if !$smarty.foreach.blog.first}<a href="{$path}/blog/up/{$blog.blog_id}/0"><img src="{$path}/images/up.gif" border="0" title="{$dict_templates.MoveUp}"></a>{/if}&nbsp;</div>
											  <div style="float: left; width: 30px;">{if !$smarty.foreach.blog.last}<a href="{$path}/blog/down/{$blog.blog_id}/0"><img src="{$path}/images/down.gif" border="0" title="{$dict_templates.MoveDown}"></a>{/if}&nbsp;</div>											  
											  {else}
											  <div style="float: left; width: 30px;">{$smarty.foreach.blog.iteration}.&nbsp;</div>
											  {/if}
											  <div class="name">{$blog.title}</div>
											  <div class="date">{$blog.date_created|date_format:"%Y-%m-%d"}</div>
											  
											  <div class="options">

													<a href="{$path}/blog/edit/{$blog.blog_id}/1"><img src="{$path}/images/page_white_edit.png" border="0" title="{$dict_templates.Edit} ({$language.short})"></a>&nbsp;
 
													{if $admin_data.level eq 2}							
																				                                       

													 <a href="javascript: openwin('{$path}/picture/index/{$blog.blog_id}/1');"><img src="{$path}/images/gallery.gif" border="0" title="galeria zdjęć"/></a>
													{if $blog.home eq 2}     
														<a href="{$path}/blog/home/{$blog.blog_id}/1/{$paging.current}" title="usuń ze strony głónej"><img src="{$path}/images/promo_on.png" border="0" title="usuń ze strony głónej"></a>
													{else}
														<a href="{$path}/blog/home/{$blog.blog_id}/2/{$paging.current}" title="ustaw na stronę główną"><img src="{$path}/images/promo_off.png" border="0" title="ustaw na stronę główną"></a>
													{/if} 													
													
													{/if}
													
													
													{if $blog.status eq 2}     
														<a href="{$path}/blog/status/{$blog.blog_id}/1/{$paging.current}" title="ustaw status niewidoczny"><img src="{$path}/images/delete.png" border="0" title="{$dict_templates.SetStatus}"></a>
													{else}
														<a href="{$path}/blog/status/{$blog.blog_id}/2/{$paging.current}" title="ustaw status widoczny"><img src="{$path}/images/tick.png" border="0" title="{$dict_templates.SetStatus}"></a>
													{/if}   													                                    
										           
													
													
													{if $admin_data.level eq 2}
													<a href="{$path}/blog/remove/{$blog.blog_id}"><img src="{$path}/images/false.png" border="0" title="{$dict_templates.Remove}"></a>
													{/if}    		
									</div>
										<div class="clearboth"></div>
									  </li>
									{/foreach}
									
									{else}
									
									<li>Brak wyników</li>
									
									{/if}
									
									</ul>
						            
								{*	
                                <div id="summary">PODSUMOWANIE</div>
                                *}
                                
                                <div class="clearboth"></div>  
                                
                                      
                                  <br/>    
								{include file="blog_paging.tpl"}
                            	</div><!--end content_block-->

                            	
                            	
                            	
                            	
                                
                            </div><!--end jquery tab-->								
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     <div class="clearboth"></div>
                <div id="footer">
                cos tu napisać idzie
                </div><!--end footer-->
                
        </div><!-- end top -->
    		{literal}		
		<script type="text/javascript">
		
		function openwin(url)
		{
			var w = 650;
			var h = 600;
			dodanie = window.open(url,'dodanie','resizable,scrollbars,width='+w+',height='+h+',left=' + ((screen.width-w)/2) + ', top=' + ((screen.height-h)/ 2));
		}
		
		function openwin_big(url)
		{
			var w = 745;
			var h = 500;
			dodanie = window.open(url,'dodanie','resizable,scrollbars,width='+w+',height='+h+',left=' + ((screen.width-w)/2) + ', top=' + ((screen.height-h)/ 2));
		}
		
		</script>
		{/literal}      
    </body>
    
</html>