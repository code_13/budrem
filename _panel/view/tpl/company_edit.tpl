<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="company_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$company.id}Dodaj firmę{else}Edycja firmy{/if}</h2>
							    
							    
							    {*
								{if $company.id}
									{$dict_templates.GetLanguageVersion} :&nbsp;
									{foreach from=$languages_list item=language}
										&nbsp;<a href="{$path}/company/edit/{$company.company_id}/{$language.id}"><img src="{$path}/images/{$language.short}.gif" border="0" title="{$language.short}"></a>&nbsp;
									{/foreach}
								{/if}    
							    *}
							    
							    
							    <form id="companyForm" method="post" enctype="multipart/form-data">

									<input type="hidden" name="company_form[category_id]" value="{$CategoryId}">
									<input type="hidden" name="company_form[language_id]" value="{$company.language_id|default:$language_id}">
									<input type="hidden" name="company_form[company_id]" value="{$company.company_id}">
									<input type="hidden" name="action" value="SaveCompany">
									
									    
							        <p>
							            <label for="company_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$company.title|default:$ret_post.title}" name="company_form[title]" id="company_form[title]"/>
							        </p>
							        
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="company_form[status]" id="company_form[status]" >
											<option value="1" {if $company.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $company.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
							        <p>
							        	<label for="editor1">Opis:</label>
							        	<textarea id="editor1" name="company_form[content]">{$company.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor1', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
							        <p>
							            <label for="company_form[abstract]">Stanowisko:</label>
							            <input class="input-big" type="text" value="{$company.abstract|default:$ret_post.abstract}" name="company_form[abstract]" id="company_form[abstract]"/>
							        </p>	
				        
									<p>
										<br/>
										<label for="pic_01">Grafika 1:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $company.pic_01}
									<p>
										<img src="{$__CFG.base_url}images/company/{$company.company_id}_01_01.jpg">
										<input type="checkbox" name="company_form[remove_picture_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


