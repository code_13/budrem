{literal}

<script type="text/javascript">


$().ready(function() {

	
	// validate signup form on keyup and submit
	$("#companyForm").validate({
		rules: {
			"company_form[title]": {
				required: true,
				minlength: 5
				
			},
			"company_form[url_name]": {
				required: true,
				minlength: 5
				
			}			
		},
		messages: {
			"company_form[title]": {
				required: "Proszę podać nazwę",
				minlength: "Nazwa musi mieć min 5 znków"
			},
			"company_form[url_name]": {
				required: "Proszę podać nazwę url",
				minlength: "Nazwa url musi mieć min 5 znków"
			}
			
		}
		
		

	});
});

</script>

{/literal}

