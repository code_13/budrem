<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="gallery_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$gallery.id}Dodaj gallerya{else}Edycja gallerya{/if}</h2>
							    
							   
							    
							    
									<form id="galleryForm" name="GalleryEdit" method="post" enctype="multipart/form-data">
									
									<input type="hidden" name="gallery_form[language_id]" value="1">
									<input type="hidden" name="gallery_form[gallery_id]" value="{$gallery.gallery_id}">
									<input type="hidden" name="action" value="SaveGallery">
									
									    
							        <p>
							            <label for="gallery_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$gallery.title|default:$ret_post.title}" name="gallery_form[title]" id="gallery_form[title]"/>
							        </p>								        
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="gallery_form[status]" id="gallery_form[status]" >
											<option value="1" {if $gallery.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $gallery.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
									<p>
	                                    <label for="selectbox">Wybierz autora:</label>
							            <select style="width: 200px;" name="gallery_form[category_id]" id="selectbox">
							            	<option value="">- wybierz -</option>
											{foreach from=$gallery_categories item=category key=key}
											
											<option value="{$category.id}" {if $gallery.category_id eq $category.staff_id || $category.staff_id eq $CategoryId}selected{/if}>{$category.title}</option>
											{/foreach}
							            </select>							        
							        
							        </p> 
									<p>
										<br/>
										<label for="pic_01">Grafika [800px/600px] - jpg, jpeg:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $gallery.pic_01}
									<p>
										<img src="{$__CFG.base_url}img/gallery/{$gallery.gallery_id}_01_01.jpg">
										<input type="checkbox" name="gallery_form[remove_picture_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        

							        
							           
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


