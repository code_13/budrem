<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="hate_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$hate.id}Dodaj zgłoszenie{else}Edycja zgłoszeń{/if}</h2>
							    
							   
							    
							    
									<form id="hateForm" name="HateEdit" method="post" enctype="multipart/form-data">
									
									<input type="hidden" name="hate_form[language_id]" value="1">
									<input type="hidden" name="hate_form[hate_id]" value="{$hate.hate_id}">
									
									<input type="hidden" name="action" value="SaveHate">
									
									    
							        <p>
							            <label for="hate_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$hate.title|default:$ret_post.title}" name="hate_form[title]" id="hate_form[title]"/>
							        </p>
							      
							        
							        <p>
							            <label for="hate_form[url_name]">Nazwa url, generowana na podstawie tytułu.<br/> [musi być brak znaków polskich i specjalnych - widoczne po pierwszym zapisie]:</label>
							        	<input disabled="disabled" class="input-big" type="text" value="{$hate.url_name|default:$ret_post.url_name}" name="hate_form[url_name]" id="hate_form[url_name]"/>
							        	{if $error.url_name}<br/><span style="color: red;">Podany url_name już istnieje - zmień tytuł.</span>{/if}
							        </p>
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="hate_form[status]" id="hate_form[status]" >
											<option value="1" {if $hate.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $hate.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
									
									{if $admin_data.level eq 2}
									
									<p>
	                                    <label for="selectbox">Wybierz kategorię (miasto):</label>
							            <select style="width: 200px;" name="hate_form[category_id]" id="selectbox">
							            	<option value="">- wybierz -</option>
											{foreach from=$hate_categories item=category key=key}
											
											<option value="{$category.id}" {if $hate.category_id eq $category.id}selected{/if}>{$category.name}</option>
											{/foreach}
							            </select>							        
							        
							        </p> 
									<p>
	                                    <label for="selectbox">Wybierz redaktora (admina):</label>
							            <select style="width: 200px;" name="hate_form[author_id]" id="selectbox">
							            	<option value="">- wybierz -</option>
											{foreach from=$admin_list item=admin}
											
											<option value="{$admin.id}" {if $hate.author_id eq $admin.id}selected{/if}><span {if $admin.level eq 2}style="color: red;"{/if}>{$admin.surname} {$admin.name}{if $admin.level eq 2} - SA{/if}</span></option>
											{/foreach}
							            </select>							        
							        
							        </p> 							        
							        {else}
							        
							        <input type="hidden" name="hate_form[category_id]" value="{$admin_data.category_id}">
							        <input type="hidden" name="hate_form[author_id]" value="{$admin_data.id}">
							        {/if}
							        
							        
							        <p>
							            <label for="hate_form[hate]">Link gówny serwisu zawierającego hate, np: [http://www.onet.pl]:</label>
							            <input class="input-big" type="text" value="{$hate.mainlink|default:$ret_post.mainlink}" name="hate_form[mainlink]" id="hate_form[mainlink]"/>
							        </p>							        
							        <p>
							            <label for="hate_form[hate]">Link serwisu zawierającego hate, np: [http://wiadomosci.onet.pl/swiat/steinmeier-jest-to-decydujacy-tydzien-dla-ukrainy/k3qzx]:</label>
							            <input class="input-big" type="text" value="{$hate.link|default:$ret_post.link}" name="hate_form[link]" id="hate_form[link]"/>
							        </p>
							        <p>
							            <label for="hate_form[email]">Adres e-mail serwisu zawierającego hate, np: [pomoc@grupaonet.pl]:</label>
							            <input class="input-big" type="text" value="{$hate.email|default:$ret_post.email}" name="hate_form[email]" id="hate_form[email]"/>
							        </p>							        
							        
							        
							        
							        
							        {if $admin_data.level eq 2}
							        <p>
							        	<label for="editor3">Fragment, (cytat):</label>
							        	<textarea id="editor3" name="hate_form[fragment]">{$hate.fragment}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor3', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>							        
							        <p>
							        	<label for="editor1">Zajawka:</label>
							        	<textarea id="editor1" name="hate_form[abstract]">{$hate.abstract}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor1', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
							        <p>
							        	<label for="editor2">Treść:</label>
							        	<textarea id="editor2" name="hate_form[content]">{$hate.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor2', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
							        {else}
							        <p>
							        	<label for="editor1">Fragment, (cytat):</label>
							        	<textarea name="hate_form[fragment]">{$hate.fragment}</textarea>							        
							        </p>
							        <p>
							        	<label for="editor1">Zajawka:</label>
							        	<textarea id="editor1" name="hate_form[abstract]">{$hate.abstract}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor1', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php',
							        	    toolbar : 'MyToolbar'
							        	});
										</script>
										{/literal}							        
							        </p>
							        <p>
							        	<label for="editor2">Treść:</label>
							        	<textarea id="editor2" name="hate_form[content]">{$hate.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor2', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php',
							        	    toolbar : 'MyToolbarContent'
							        	});
										</script>
										{/literal}							        
							        </p>							        

							        {/if}
									<p>
										<br/>
										<label for="pic_01">Grafika [800px/600px] - jpg, jpeg:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $hate.pic_01}
									<p>
										<img src="{$__CFG.base_url}img/hate/{$hate.hate_id}_01_01.jpg">
										<input type="checkbox" name="hate_form[remove_picture_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        

							        
							           
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


