<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
                            <div class="jquery_tab">
                            
                                <div class="content_block">
                                    <h2 class="jquery_tab_title">Lista zgłoszeń</h2>
                                    
                                    {if $good_message}
                           		  	<div class="message success closeable">
    									<p><strong>Gratulacje!</strong> {$good_message}</p>
									</div>           
                                    {/if}
                                    {if $bad_message}
                           		  	<div class="message error closeable">
    									<p><strong>Uwaga!</strong> {$bad_message}</p>
									</div>           
                                    {/if}
						            {*
						            {include file="hate_filters.tpl"}
									*}
									



									
									{include file="hate_paging.tpl"}
									
									<div style="display: none;" id="info"></div>
									
									<ul id="listtop">
										<li class="top">
											  
											  <div class="nametop">TYTUŁ</div>
											  <div class="datetop">DATA</div>
											  
											  <div class="optionstop">OPCJE</div>
									    </li><div class="clrl"></div>
									</ul>
									<ul id="order-list">
									
									{if $hates_list}
									
									{foreach from=$hates_list item=hate name=hate}
									{cycle name=color assign=row_color values="#EFEFEF ,#EFEFEF"}
									
									
									  <li id="listItem_{$hate.hate_id}" style="background-color: {$row_color}">
											  {if $admin_data.level eq 2}	
											  <div style="float: left; width: 30px;">{if !$smarty.foreach.hate.first}<a href="{$path}/hate/up/{$hate.hate_id}/0"><img src="{$path}/images/up.gif" border="0" title="{$dict_templates.MoveUp}"></a>{/if}&nbsp;</div>
											  <div style="float: left; width: 30px;">{if !$smarty.foreach.hate.last}<a href="{$path}/hate/down/{$hate.hate_id}/0"><img src="{$path}/images/down.gif" border="0" title="{$dict_templates.MoveDown}"></a>{/if}&nbsp;</div>											  
											  {else}
											  <div style="float: left; width: 30px;">{$smarty.foreach.hate.iteration}.&nbsp;</div>
											  {/if}
											  <div class="name">{$hate.title}</div>
											  <div class="date">{$hate.date_created|date_format:"%Y-%m-%d"}</div>
											  
											  <div class="options">

													<a href="{$path}/hate/edit/{$hate.hate_id}/1"><img src="{$path}/images/page_white_edit.png" border="0" title="{$dict_templates.Edit} ({$language.short})"></a>&nbsp;
 
																				
																				                                       
													{if $hate.status eq 2}     
														<a href="{$path}/hate/status/{$hate.hate_id}/1/{$paging.current}" title="ustaw status niewidoczny"><img src="{$path}/images/delete.png" border="0" title="{$dict_templates.SetStatus}"></a>
													{else}
														<a href="{$path}/hate/status/{$hate.hate_id}/2/{$paging.current}" title="ustaw status widoczny"><img src="{$path}/images/tick.png" border="0" title="{$dict_templates.SetStatus}"></a>
													{/if}   
													{if $hate.send eq 1}     
														<img src="{$path}/images/send_off.png" border="0" title="Alert był już wysłany">
													{else}
														<a href="{$path}/hate/send/{$hate.hate_id}/1/{$paging.current}" title="Wyślij alert wydawcy"><img src="{$path}/images/send_on.png" border="0" title="Wyślij alert wydawcy"></a>
													{/if} 													
													
													{if $admin_data.level eq 2}
													<a href="{$path}/hate/remove/{$hate.hate_id}"><img src="{$path}/images/false.png" border="0" title="{$dict_templates.Remove}"></a>
													{/if}	    		
									</div>
										<div class="clearboth"></div>
									  </li>
									{/foreach}
									
									{else}
									
									<li>Brak wyników</li>
									
									{/if}
									
									</ul>
						            
								{*	
                                <div id="summary">PODSUMOWANIE</div>
                                *}
                                
                                <div class="clearboth"></div>  
                                
                                      
                                  <br/>    
								{include file="hate_paging.tpl"}
                            	</div><!--end content_block-->

                            	
                            	
                            	
                            	
                                
                            </div><!--end jquery tab-->								
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     <div class="clearboth"></div>
                <div id="footer">
                cos tu napisać idzie
                </div><!--end footer-->
                
        </div><!-- end top -->
    		{literal}		
		<script type="text/javascript">
		
		function openwin(url)
		{
			var w = 650;
			var h = 600;
			dodanie = window.open(url,'dodanie','resizable,scrollbars,width='+w+',height='+h+',left=' + ((screen.width-w)/2) + ', top=' + ((screen.height-h)/ 2));
		}
		
		function openwin_big(url)
		{
			var w = 745;
			var h = 500;
			dodanie = window.open(url,'dodanie','resizable,scrollbars,width='+w+',height='+h+',left=' + ((screen.width-w)/2) + ', top=' + ((screen.height-h)/ 2));
		}
		
		</script>
		{/literal}      
    </body>
    
</html>