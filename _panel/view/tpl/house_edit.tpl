<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="house_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$house.id}Dodaj housea{else}Edycja housea{/if}</h2>
							    
							   
							    
							    
									<form id="houseForm" name="HouseEdit" method="post" enctype="multipart/form-data">
									
									<input type="hidden" name="house_form[language_id]" value="1">
									<input type="hidden" name="house_form[house_id]" value="{$house.house_id}">
									<input type="hidden" name="action" value="SaveHouse">
									
									    
							        <p>
							            <label for="house_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$house.title|default:$ret_post.title}" name="house_form[title]" id="house_form[title]"/>
							        </p>
							      
							        
							        <p>
							            <label for="house_form[url_name]">Nazwa url, generowana na podstawie tytułu.<br/> [musi być brak znaków polskich i specjalnych - widoczne po pierwszym zapisie]:</label>
							            <input class="input-big" type="text" value="{$house.url_name|default:$ret_post.url_name}" name="house_form[url_name]" id="house_form[url_name]"/>
							            {if $error.url_name}<br/><span style="color: red;">Podany url_name już istnieje - zmień tytuł.</span>{/if}
							        </p>							        
							        
							        <p>
							            <label for="house_form[youtube]">Link YouTube:</label>
							            <input class="input-big" type="text" value="{$house.youtube|default:$ret_post.youtube}" name="house_form[youtube]" id="house_form[youtube]"/>
							        </p>	
							        <p>
										<label for="date">Data utworzenia:</label>
										<input class="input-small flexy_datepicker_input" type="text" value="{$house.date_created|date_format:"%Y-%m-%d"|default:$ret_post.date_created|date_format:"%Y-%m-%d"}" name="house_form[date_created]" id="date"/>	
							        </p>							        
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="house_form[status]" id="house_form[status]" >
											<option value="1" {if $house.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $house.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
									

							        
							        <input type="hidden" name="house_form[category_id]" value="{$admin_data.category_id}">
							        
							       
							        <p>
							        	<label for="editor1">Zajawka:</label>
							        	<textarea id="editor1" name="house_form[abstract]">{$house.abstract}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor1', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
							        <p>
							        	<label for="editor2">Treść:</label>
							        	<textarea id="editor2" name="house_form[content]">{$house.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor2', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
									<p>
										<br/>
										<label for="pic_01">Grafika [800px/600px] - jpg, jpeg:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $house.pic_01}
									<p>
										<img src="{$__CFG.base_url}images/house/{$house.house_id}_01_01.jpg">
										<input type="checkbox" name="house_form[remove_picture_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        

							        
							           
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


