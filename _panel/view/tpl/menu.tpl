              <div id="sidebar">
                            <ul class="nav">
                            
                                
                                <li><a class="headitem item1" href="#">Artykuły</a>
                                    <ul {if $script_name eq article}class="opened"{/if}><!-- ul items without this class get hiddden by jquery-->
                                    {if $CategoryId ne 15} 
                                    <li {if $akcja eq article_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/article/new">Dodaj nowy</a></li>
                                    {/if}
                                    <li {if $akcja eq article_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/article/index">Lista artykułów</a></li>
                                    </ul>
                                    
                                </li>  
                                <li><a class="headitem item3" href="#">Blog</a>
                                    <ul {if $script_name eq blog}class="opened"{/if}>
	                                    <li {if $akcja eq blog_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/blog/new">Dodaj wpis</a></li>
	                                    <li {if $akcja eq blog_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/blog/index">Lista informacji</a></li>
                                    </ul>                            
                                </li>
                                <li><a class="headitem item19" href="#">Domy na sprzedaż</a>
                                    <ul {if $script_name eq house}class="opened"{/if}>
	                                    <li {if $akcja eq house_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/house/new">Dodaj dom</a></li>
	                                    <li {if $akcja eq house_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/house/index">Lista domów</a></li>
                                    </ul>                            
                                </li>
                                <li><a class="headitem item17" href="#">Apartamenty</a>
                                    <ul {if $script_name eq product}class="opened"{/if}>
	                                    <li {if $akcja eq product_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/product/new">Dodaj wpis</a></li>
	                                    <li {if $akcja eq product_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/product/index">Lista apartamentów</a></li>
                                    </ul>                            
                                </li>
                                <li><a class="headitem item2" href="#">Kategorie oferty</a>
                                    <ul {if $script_name eq staff}class="opened"{/if}>
										<li {if $akcja eq staff_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/staff/new">Dodaj kategorię</a></li>
	                                    <li {if $akcja eq staff_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/staff/index">Lista kategorii</a></li>
                                    </ul>                              
                                </li>
                                <li><a class="headitem item7" href="#">Galeria</a>
                                    <ul {if $script_name eq gallery}class="opened"{/if}>
	                                    <li {if $akcja eq gallery_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/gallery/new">Dodaj zdjęcie</a></li>
	                                    <li {if $akcja eq gallery_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/gallery/index">Lista zdjęć</a></li>
                                    </ul>                            
                                </li>
                                <li><a class="headitem item5" href="#">Slajdy</a>
                                    <ul {if $script_name eq slideshow}class="opened"{/if}><!-- ul items without this class get hiddden by jquery-->                    
                                    <li {if $akcja eq slideshow_edit}style="background-color: #FFFFFF;"{/if}><a href="{$path}/slideshow/new">Dodaj nowy</a></li>                        
                                    <li {if $akcja eq slideshow_list}style="background-color: #FFFFFF;"{/if}><a href="{$path}/slideshow/index">Lista slajdów</a></li>
                                    </ul>
                                    
                                </li> 
                                
                            </ul><!--end subnav-->
                           
                           
                           <div style="height: 150px;">&nbsp;</div>

                        </div><!--end sidebar-->