<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head_picture.tpl"} 
 {if $close}
<script language="JavaScript">
	window.opener.location = '{$location}';
	window.close();
</script>
{/if}   
    <body>
    	
		{include file="portfolio_validate.tpl"}
        <div id="top" style="max-width: 300px; min-width: 300px;">
        

           	
            	<div>
                
                    <div id="main">
                    
                        <div id="content" style="margin-left: 0px;">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$portfolio.id}Dodawanie zdjęcia{else}Edycja zdjęcia{/if}</h2>

							    
							    <form id="portfolioForm" method="post" enctype="multipart/form-data">

									{if $portfolio.id}
									<input type="hidden" name="portfolio_form[category_id]" value="{$url_config.4}">
									{else}
									<input type="hidden" name="portfolio_form[category_id]" value="{$url_config.3}">
									{/if}									
									
								
									<input type="hidden" name="portfolio_form[language_id]" value="{$portfolio.language_id|default:$language_id}">
									<input type="hidden" name="portfolio_form[portfolio_id]" value="{$portfolio.portfolio_id}">
									<input type="hidden" name="action" value="SavePortfolio">
									
									    
							        <p>
							            <label for="portfolio_form[title]">Nazwa:</label>
							            <input class="input-medium" type="text" value="{$portfolio.title|default:$ret_post.title}" name="portfolio_form[title]" id="portfolio_form[title]"/>
							        </p>
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="portfolio_form[status]" id="portfolio_form[status]" >
											<option value="1" {if $portfolio.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $portfolio.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>	
									<p>
										<br/>
										<label for="pic_01">Grafika [548px/365px] - jpg, jpeg:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $portfolio.pic_01} 
									<p>
										<img src="{$__CFG.base_url}images/portfolio/{$portfolio.portfolio_id}_01_01.jpg">
										<input type="checkbox" name="portfolio_form[remove_portfolio_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        

							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->								
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    

                        
                     </div><!--end bg_wrapper-->
                     <div class="clearboth"></div>

                
        </div><!-- end top -->
        
        {literal}
        		
		
<script type="text/javascript">$('#example1').datetimepicker();
</script>

{/literal}
		{literal}	
		
		<script type="text/javascript">
		
		function openwin(url)
		{
			var w = 450;
			var h = 250;
			dodanie = window.open(url,'portfolio','resizable,scrollbars,width='+w+',height='+h+',left=' + ((screen.width-w)/2) + ', top=' + ((screen.height-h)/ 2));
		}
		
		function openwin_big(url)
		{
			var w = 800;
			var h = 700;
			dodanie = window.open(url,'portfolio','resizable,scrollbars,width='+w+',height='+h+',left=' + ((screen.width-w)/2) + ', top=' + ((screen.height-h)/ 2));
		}
		
		</script>
		{/literal}
    </body>
    
</html>