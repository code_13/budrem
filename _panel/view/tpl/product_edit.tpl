<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="product_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$product.id}Dodaj cennik{else}Edycja cennik{/if}</h2>
							    
							    
							    {*
								{if $product.id}
									{$dict_templates.GetLanguageVersion} :&nbsp;
									{foreach from=$languages_list item=language}
										&nbsp;<a href="{$path}/product/edit/{$product.product_id}/{$language.id}"><img src="{$path}/images/{$language.short}.gif" border="0" title="{$language.short}"></a>&nbsp;
									{/foreach}
								{/if}    
							    *}
							    
							    
							    <form id="productForm" method="post" enctype="multipart/form-data">
									{if $product.id}
									<input type="hidden" name="product_form[category_id]" value="{$product.category_id}">
									{else}
									<input type="hidden" name="product_form[category_id]" value="{$CategoryId}">
									{/if}
									
									<input type="hidden" name="product_form[language_id]" value="1">
									<input type="hidden" name="product_form[product_id]" value="{$product.product_id}">
									<input type="hidden" name="action" value="SaveProduct">
									
									    
							        <p>
							            <label for="product_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$product.title|default:$ret_post.title}" name="product_form[title]" id="product_form[title]"/>
							        </p>
									<p>
										<label for="product_form[url_name]">Nazwa url:</label>
										<input class="input-big" type="text" value="{$product.url_name|default:$ret_post.url_name}" name="product_form[url_name]" id="product_form[url_name]"/>
										{if $error.url_name}<br/><span style="color: red;">Podany url_name już istnieje.</span>{/if}
									</p>


									<p>
										<label for="product_form[param1]">Longtitude:</label>
										<input class="input-big" type="text" value="{$product.param1|default:$ret_post.param1}" name="product_form[param1]" id="product_form[param1]"/>
									</p>
									<p>
										<label for="product_form[param2]">Latitude:</label>
										<input class="input-big" type="text" value="{$product.param2|default:$ret_post.param2}" name="product_form[param2]" id="product_form[param2]"/>
									</p>

							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="product_form[status]" id="product_form[status]" >
											<option value="1" {if $product.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $product.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
									<p>
										<label for="editor1">Zajawka:</label>
										<textarea id="editor1" name="product_form[abstract]">{$product.abstract}</textarea>
										{literal}
											<script>
                                                CKEDITOR.replace( 'editor1', {
                                                    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
                                                    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
                                                });
											</script>
										{/literal}
									</p>
									<p>
										<label for="editor2">Treść:</label>
										<textarea id="editor2" name="product_form[content]">{$product.content}</textarea>
										{literal}
											<script>
                                                CKEDITOR.replace( 'editor2', {
                                                    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
                                                    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
                                                });
											</script>
										{/literal}
									</p>
									<p>
										<br/>
										<label for="pic_01">Grafika 1 [1920px/500px - jpg]:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
									</p>
									{if $product.pic_01}
									<p>
										<img src="{$__CFG.base_url}images/product/{$product.product_id}_01_01.jpg"/>
										<input type="checkbox" name="product_form[remove_picture_01]" value="1"/>Usuń zdjęcie
									</p>
									{/if}							        
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


