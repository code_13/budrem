<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="staff_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$staff.id}Dodaj osobę{else}Edycja osoby{/if}</h2>
							    
							    
							    {*
								{if $staff.id}
									{$dict_templates.GetLanguageVersion} :&nbsp;
									{foreach from=$languages_list item=language}
										&nbsp;<a href="{$path}/staff/edit/{$staff.staff_id}/{$language.id}"><img src="{$path}/images/{$language.short}.gif" border="0" title="{$language.short}"></a>&nbsp;
									{/foreach}
								{/if}    
							    *}
							    
							    
							    <form id="staffForm" method="post" enctype="multipart/form-data">

									<input type="hidden" name="staff_form[category_id]" value="1">
									<input type="hidden" name="staff_form[language_id]" value="{$staff.language_id|default:$language_id}">
									<input type="hidden" name="staff_form[staff_id]" value="{$staff.staff_id}">
									<input type="hidden" name="action" value="SaveStaff">
									
									    
							        <p>
							            <label for="staff_form[title]">Imię i nazwisko:</label>
							            <input class="input-big" type="text" value="{$staff.title|default:$ret_post.title}" name="staff_form[title]" id="staff_form[title]"/>
							        </p>
							        <p>
							            <label for="staff_form[url_name]">url-name:</label>
							            <input class="input-big" type="text" value="{$staff.url_name|default:$ret_post.url_name}" name="staff_form[url_name]" id="staff_form[url_name]"/>
							        </p>
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="staff_form[status]" id="staff_form[status]" >
											<option value="1" {if $staff.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $staff.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
							        <p>
							        	<label for="editor2">Treść:</label>
							        	<textarea id="editor2" name="staff_form[content]">{$staff.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor2', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>
							        {*
									<p>
										<br/>
										<label for="pic_01">Grafika 1:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $staff.pic_01}
									<p>
										<img src="{$__CFG.base_url}img/staff/{$staff.staff_id}_01_01.jpg"/>
										<input type="checkbox" name="staff_form[remove_picture_01]" value="1"/>Usuń zdjęcie
									</p>
									{/if}							        
							        *}
							           
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


