<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        
        
        {include file="user_validate.tpl"}
        
        *}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$user.id}Dodaj użytkownika{else}Edycja użytkownia{/if}</h2>
							    
                                    {if $good_message}
                           		  	<div class="message success closeable">
    									<p><strong>Gratulacje!</strong> {$good_message}</p>
									</div>           
                                    {/if}							    
							    
									
									<input type="hidden" name="user_form[id]" value="{$user.id}">
									<input type="hidden" name="user_form[email_old]" value="{$user.email}">
									    
							        <p>
							            <label for="user_form[name]">Imię:</label>
							            <input class="input-medium" type="text" value="{$user.name|default:$ret_post.name}" name="user_form[name]" id="_formuser[name]"/>
							        </p>							        
							        <p>
							            <label for="user_form[city]">Miejscowość:</label>
							            <input class="input-medium" type="text" value="{$user.city|default:$ret_post.city}" name="user_form[city]" id="user_form[city]"/>
							        </p>								      
							        <p>
							            <label for="user_form[email]">Email:</label>
							            <input class="input-medium" type="text" value="{$user.email|default:$ret_post.email}" name="user_form[email]" id="user_form[email]"/>
							        </p>
							        <p>
							            <label for="user_form[birth_date]">Data urodzenia:</label>
							            <input class="input-medium" type="text" value="{$user.birth_date|default:$ret_post.birth_date}" name="user_form[birth_date]" id="user_form[birth_date]"/>
							        </p>
							        <p>
							            <label for="user_form[newsletter]">Newsletter:</label>
							            <input type="checkbox" value="1" name="user_form[newsletter]" {if $user.newsletter eq 1}checked="checked"{/if} disabled="disabled" id="user_form[newsletter]"/>
							        </p>							         
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="user_form[status]" id="user_form[status]" >
											<option value="0" {if $user.status eq 0}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="1" {if $user.status eq 1}selected{/if}>{$dict_templates.ArticlePublished}</option>
							            </select>
							        </p>
									
									{if $user.pic_01}
									<p>
										<img src="{$__CFG.base_url}images/user/{$user.id}_01_01.jpg" alt="user">
									<p>
									{/if}	
									{if $user.file.file_name}
									<p>
										<a href="{$__CFG.base_url}data/upload/{$user.file.file_name}">Przesłane CV</a>
									<p>
									{/if}
							        <p>
							            <input onclick="window.location='{$path}/user/index/1'" class="button" type="submit" value="Zamknij"/>
							        </p>
							   
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


