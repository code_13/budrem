<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	{include file="head.tpl"}
    
    <body>
    	
    	{*
    	{include file="popup_message.tpl"}
        *}
        
        {include file="video_validate.tpl"}
        
        <div id="top">
        
			{include file="header.tpl"}
           	
            	<div id="bg_wrapper">
                
                    <div id="main">
                    
                        <div id="content">
                        
							<div class="jquery_tab">
							
							<div class="content_block">
							    <h2 class="jquery_tab_title">{if !$video.id}Dodaj video{else}Edycja video{/if}</h2>
							    
							   
							    
							    
									<form id="videoForm" name="VideoEdit" method="post" enctype="multipart/form-data">
									
									<input type="hidden" name="video_form[language_id]" value="1">
									<input type="hidden" name="video_form[category_id]" value="1">
									<input type="hidden" name="video_form[video_id]" value="{$video.video_id}">
									<input type="hidden" name="action" value="SaveVideo">
									
									    
							        <p>
							            <label for="video_form[title]">Tytuł:</label>
							            <input class="input-big" type="text" value="{$video.title|default:$ret_post.title}" name="video_form[title]" id="video_form[title]"/>
							        </p>
							      
							        
							        <p>
							            <label for="video_form[url_name]">Nazwa url:</label>
							            <input class="input-big" type="text" value="{$video.url_name|default:$ret_post.url_name}" name="video_form[url_name]" id="video_form[url_name]"/>
							        	{if $error.url_name}<br/><span style="color: red;">Podany url_name już istnieje.</span>{/if}
							        </p>							        
							        <p>
							            <label for="video_form[link]">Link YouTube:</label>
							            <input class="input-big" type="text" value="{$video.link|default:$ret_post.link}" name="video_form[link]" id="video_form[link]"/>
							        </p>							        
							        {if $CategoryId eq 1}
							        <p>
	                                    <label for="selectbox">Typ:</label>
							            <select style="width: 200px;" name="video_form[type]" id="selectbox">
											<option value="">- wybierz -</option>
											<option value="1" {if $video.type eq 1}selected{/if}>szkolenia</option>
											<option value="2" {if $video.type eq 2}selected{/if}>akcje i wydarzenia</option>
							            </select>							        
							        </p>
							        {/if}
							        
							        <p>
							            <label for="selectbox">Status:</label>
							            <select name="video_form[status]" id="video_form[status]" >
											<option value="1" {if $video.status eq 1}selected{/if}>{$dict_templates.ArticleUnPublished}</option>
											<option value="2" {if $video.status eq 2}selected{/if}>{$dict_templates.ArticlePublished}</option>
										
							            </select>
							        </p>
							
							
							        {*
							        <p>
							           <label for="textarea2">Embbed YouTube [560px/315px]:</label>
							           <textarea name="video_form[abstract]" id="textarea2" cols="40" rows="15">{$video.abstract|default:$ret_post.abstract}</textarea>
							        </p>
									*}
							        <p>
							        	<label for="editor1">Opis:</label>
							        	<textarea id="editor1" name="video_form[content]">{$video.content}</textarea>
							        	{literal}
							        	<script>
							        	CKEDITOR.replace( 'editor1', {
							        	    filebrowserBrowseUrl: '/_panel/kcfinder/browse.php',
							        	    filebrowserUploadUrl: '/_panel/kcfinder/upload.php'
							        	});
										</script>
										{/literal}							        
							        </p>							        
									
									
									
									
									<p>
										<br/>
										<label for="pic_01">Grafika 1:</label>
										<input class="input-small" type="file" id="pic_01" name="pic_01" />
										
									</p>
									
									{if $video.pic_01}
									<p>
										<img src="{$__CFG.base_url}img/video/{$video.video_id}_01_01.jpg">
										<input type="checkbox" name="video_form[remove_picture_01]" value="1">Usuń zdjęcie
									<p>
									{/if}							        
							        {*
									<p>
										<br/>
										<label for="pic_02">Grafika 2:</label>
										<input class="input-small" type="file" id="pic_02" name="pic_02" />
										
									</p>
									
									{if $video.pic_02}
									<p>
										<img src="{$__CFG.base_url}images/video/{$video.video_id}_01_02.jpg">
										<input type="checkbox" name="video_form[remove_picture_02]" value="1">Usuń zdjęcie
									<p>
									{/if}
									
									<p>
										<br/>
										<label for="pic_03">Grafika 3:</label>
										<input class="input-small" type="file" id="pic_03" name="pic_03" />
										
									</p>
									
									{if $video.pic_03}
									<p>
										<img src="{$__CFG.base_url}images/video/{$video.video_id}_01_03.jpg">
										<input type="checkbox" name="video_form[remove_picture_03]" value="1">Usuń zdjęcie
									<p>
									{/if}						        
							        *}
							        
							           
							        <p>
							            <input class="button" name="submit" type="submit" value="Zapisz"/>
							        </p>
							    </form>
							    
							</div><!--end content_block-->
							    
							</div><!--end jquery tab-->						
								

                        </div><!--end content-->
                        
                    </div><!--end main-->
                    
					{include file="menu.tpl"}
                        
                     </div><!--end bg_wrapper-->
                     
                <div id="footer">
                
                </div><!--end footer-->
                
        </div><!-- end top -->
        
    </body>
    
</html>


