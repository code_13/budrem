{literal}

<script type="text/javascript">


$().ready(function() {

	
	// validate signup form on keyup and submit
	$("#videoForm").validate({
		rules: {
			"video_form[title]": {
				required: true,
				minlength: 5
				
			},
			"video_form[url_name]": {
				required: true,
				minlength: 5
				
			},
			"video_form[abstract]": {
				required: true,
				minlength: 35
			},
			"video_form[link]": {
				required: true
			}			
		},
		messages: {
			"video_form[title]": {
				required: "Proszę podać nazwę",
				minlength: "Nazwa musi mieć min 5 znków"
			},
			"video_form[url_name]": {
				required: "Proszę podać nazwę url",
				minlength: "Nazwa url musi mieć min 5 znków"
			},
			"video_form[abstract]": {
				required: "Proszę podać opis",
				minlength: "Opis musi mieć min 35 znaków"
			},
			"video_form[link]": {
				required: "Proszę podać link"
			}
			
		}
		
		

	});
});

</script>

{/literal}

