<?php
/*
 * obsługuje prezentację kategorii produktów
 */

// print_r($_SESSION);

require_once('Blog.class.php');
$blog = new Blog();

//Kategorie bloga

require_once('Category.class.php');
$category = new Category();

$blog_categories = $category->createMenuCategoriesToView();
$smarty->assign("blog_categories", $blog_categories);
		
$intro_main = 7;
require_once('includes/introduction.inc.php');



//-------------------------------------------------------------
// Szczegóły bloga
//-------------------------------------------------------------
	
	if($url_config['1'] == "view"){
		
		if($url_config['2']){
			
			
			$blog_details = $blog->getBlogByUrlNameToView($url_config['2'], $_SESSION['lang']);
			
			if($blog_details){
				
				
				//Zdjęcia przypisane do informacji
				require_once('Picture.class.php');
				$picture = new Picture();				
				$picture_list = $picture->getPicturesByCategoryToView(100, 1, $_SESSION['lang'], $blog_details['blog_id']);
				//print_r($picture_list);
				$smarty->assign("picture_list", $picture_list);				
				
				//Tytuły meta
				$head = array();
				$head['title'] = $blog_details['title'];
				$head['description'] = str_replace('"', ' ', $blog_details['abstract']);
				$smarty->assign("head", $head);	
				$smarty->assign("blog_details", $blog_details);
				//print_r($blog_details);
				$smarty->display("blog_details.tpl");
				
			}
			else{
				
				$smarty->assign("error_heading", "404 Page Not Found");
				$smarty->assign("error_message", "Podany blog nie istnieje.");
	
				
				$template = "errors/error_general.tpl";
				$smarty->display($template);
				exit;					
				
				
				
			}		
			
		}
		else{
			
			$smarty->assign("error_heading", "404 Page Not Found");
			$smarty->assign("error_message", "Nie kombinuj z url ;-).");

			
			$template = "errors/error_general.tpl";
			$smarty->display($template);
			exit;				
			
			
		}

		
	}	
//-------------------------------------------------------------
// Lista bloga
//-------------------------------------------------------------
	
	elseif(!$url_config['1']){
		
		header("Location: ".$default_path."blog/index/");
		
	}
		
	elseif($url_config['1'] == "index"){
	
		// ustawienie numeru strony do stronicowania (jezeli nie została podana)
		if (!$url_config['2']) {
			$url_config['2'] = 1;
		}
		
		//Tytuły meta
		$head = array();
		$head['title'] = "Blog";
		$smarty->assign("head", $head);		
		//echo"blog";
		$limit = 5;
		$blog_list = $blog->getBlogsByView("order", "desc", $limit, $url_config['2'], $_SESSION['lang']);
		//print_r($blog_list);

		$smarty->assign("blog_list", $blog_list);
		$smarty->assign("paging", $blog->paging);
		
		$smarty->display("blog.tpl");
	
	}
	
//-------------------------------------------------------------
// index bloga wg kategorii
//-------------------------------------------------------------
		
	elseif($url_config['1'] == "miasto"){
	
		// ustawienie numeru strony do stronicowania (jezeli nie została podana)
		if (!$url_config['3']) {
			$url_config['3'] = 1;
		}
		
		//Szczegoly kategorii
		if($url_config['2']){
			
			$category_details = $category->getGroupByUrlName($url_config['2']);
			$smarty->assign("category_details", $category_details);
			
		}
		
		//Tytuły meta
		$head = array();
		$head['title'] = "Blog";
		$smarty->assign("head", $head);		
		
		//$limit = $__CFG['blog_count'];
		$limit = 5;
		$blog_list = $blog->getBlogsByViewAndCategory("order", "desc", $limit, $url_config['3'], $_SESSION['lang'], $category_details['id']);
		//print_r($blog_list);
		$smarty->assign("blog_list", $blog_list);
		$smarty->assign("paging", $blog->paging);
		
		$smarty->display("blog.tpl");
	
	}
	
//-------------------------------------------------------------
// Nieznany parametr
//-------------------------------------------------------------	
	
	
	else{
		
		
			$smarty->assign("error_heading", "404 Page Not Found");
			$smarty->assign("error_message", "The page you requested was not found.");

			
			$template = "errors/error_general.tpl";
			$smarty->display($template);
			exit;		
		
		
	}

		

	
	

	
	



?>