<?php

/**
 *  contact form management
 */
//Powodzenie
if($_SESSION['message']['good_message']){
	
	$smarty->assign("good_message", $_SESSION['message']['good_message']);
	unset($_SESSION['message']['good_message']);
	
}

//Błąd
if($_SESSION['message']['bad_message']){
	
	$smarty->assign("bad_message", $_SESSION['message']['bad_message']);
	unset($_SESSION['message']['bad_message']);
	
}	 

$intro_main = 10;
require_once('includes/introduction.inc.php');

if ($_REQUEST['action'] == "SaveContact") {
	if (sizeof($_POST['contact_form'])) {
		
	    // validate form data
	    
	    require_once('validate_contact_form.inc.php');
	    
		
		if (!sizeof($error)) {
		
			require_once ('Contact.class.php');
			$contact = new Contact();
			
			$contact->saveContact($_POST['contact_form']);

			// komunikat do widoku
			$_SESSION['message']['good_message'] = "Twoja wiadomość została wysłana. Dziękujemy!";
			header("location: /kontakt");

	

			
		}
		else {
			
		print_r($error);
			
			// wystąpiły błędy - przekazujemy info o tych błędach
			$smarty->assign("message", $error['contact_form']['message']);
			$smarty->assign("error", $error['contact_form']);
			$smarty->assign("ret_post", $_POST['contact_form']);
			$smarty->assign("main", "contact_form.tpl");
		}
		
	}
}	
	
$smarty->assign("main", "main_contact.tpl");
?>
