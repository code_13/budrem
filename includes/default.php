<?php

//error_reporting(0); 
//----------------------------------------------------
//Konfig podstawowy
//----------------------------------------------------
	
	require_once('./config/config.php');
	
	session_start();
//-------------------------------------------------------------------------
// Rozpoznanie klienta - i ewentuialne przeniesienie na wersje emobilna
//-------------------------------------------------------------------------	
	//Tworzymy obiekt klasy uagent_info
	require_once('DetectAgent.class.php');
	$obiekt = new uagent_info();
	//Spróbujmy wykryć Smartphone'a
	//Poniższa metoda (funkcja) zwróci 1 lub 0
	$Mobile = $obiekt->DetectMobileLong();
		
	if($Mobile == 1){
		
		$_SESSION['MobileAgent'] = 1;
			
	}	
	
	
	//print_r($_GET);
	// obsługa wielojęzyczności
	require_once($__CFG['base_path'].'/includes/language.php');
	
	// dołaczamy odpowiedni słownik i przekazujemy parametry do smartów
	require_once($__CFG['base_path'].'/includes/lang/dict_'.$language_details['short'].'.php');
	$smarty->assign('dict_templates', $dict_templates);
	$smarty->assign('dict_reports', $dict_reports);
	$smarty->assign('dict_errors', $dict_errors);
	$smarty->assign("dict_menu", $dict_menu);
	
	
//----------------------------------------------------	
// obsługa użytkowników
//----------------------------------------------------
	require_once($__CFG['base_path'].'/includes/user.inc.php');


//----------------------------------------------------
//Obsługa przyjaznych url
//----------------------------------------------------

	
	$arrParams = array();
	$strDefaultPath = '/index';
	$_SERVER['REQUEST_URI'] = isset( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : $strDefaultPath;
	$arrParams = explode( '/', substr( $_SERVER['REQUEST_URI'], 1) );
	
	$url_config = $arrParams;
	if($_SERVER['REQUEST_URI'] == '/'){
		
		$_SERVER['REQUEST_URI'] = 'index';
		$url_config['0'] = $_SERVER['REQUEST_URI'];
		
	}
	$smarty->assign("url_config", $url_config);


//------------------------------
// Obrazki
//------------------------------
	 
	//print_r($__CFG['base_url']);
	$default_path = $__CFG['base_url'];
	$smarty->assign("DP", $default_path);
	
	$smarty->assign("cook", $_SESSION['active_book']);
	
	
	
	//print_r($_SESSION['user_data']);
    //Lista apartamentow do menu - wszystkie aktywne
    require_once('Product.class.php');
    $product = new Product();
    $menu_list = $product->getProductsActiveAll($_SESSION['lang']);
    //print_r($index_promo);
    $smarty->assign('menu_list', $menu_list);



    //Domy w promocji
    require_once('House.class.php');
    $house = new House();
    $house_promo = $house->getHousesByViewToHome('order', 'desc', 3, 1, $_SESSION['lang']);
    //print_r($house_promo);
    $smarty->assign('house_promo', $house_promo);

    //Apartamenty w promocji
    require_once('Product.class.php');
    $product = new Product();
    $product_promo = $product->getProductsByPromotion(3, $_SESSION['lang']);
    //print_r($product_promo);
    $smarty->assign('product_promo', $product_promo);


    require_once('Article.class.php');
	$article = new Article();
	$intro_foot = $article->getArticle(31, $_SESSION['lang']);
	$smarty->assign('intro_foot', $intro_foot);
	
    require_once('Blog.class.php');
    $blog = new Blog();
    $last_blog = $blog->getBlogsByViewToHome("order", "asc", 100, 1, 1);
    $smarty->assign('last_blog', $last_blog);
    //print_r($last_blog);
	
	//Random portfolio
	require_once('Portfolio.class.php');
	$portfolio = new Portfolio();
	$random_portfolio = $portfolio->getRandomPortfolios(5, $_SESSION['lang']);
	//print_r($random_portfolio);
	$smarty->assign('random_portfolio', $random_portfolio);	
	
	//Lista kategorii (ekipa)
	require_once('Staff.class.php');
	$staff = new Staff();
	$staff_list = $staff->getStaffsByCategoryToView(100, 1, 1, 1);
	$smarty->assign("category_list", $staff_list);
?>