<?php
/*
 * obsługuje prezentację kategorii produktów
 */

// print_r($_SESSION);

require_once('Gallery.class.php');
$gallery = new Gallery();

//Kategorie gallerya

require_once('Staff.class.php');
$staff = new Staff();

$gallery_categories = $staff->getStaffsByCategoryToView(100, 1, 1, 1);
$smarty->assign("gallery_categories", $gallery_categories);
		
$intro_main = 7;
require_once('includes/introduction.inc.php');



//-------------------------------------------------------------
// Szczegóły gallerya
//-------------------------------------------------------------
	
	if($url_config['1'] == "view"){
		
		if($url_config['1']){
			
			
			$gallery_details = $gallery->getGalleryByUrlNameToView($url_config['2'], $_SESSION['lang']);
			
			if($gallery_details){
				
				
				//Zdjęcia przypisane do informacji
				require_once('Picture.class.php');
				$picture = new Picture();				
				$picture_list = $picture->getPicturesByCategoryToView(100, 1, $_SESSION['lang'], $gallery_details['gallery_id']);
				//print_r($picture_list);
				$smarty->assign("picture_list", $picture_list);				
				
				//Tytuły meta
				$head = array();
				$head['title'] = $gallery_details['title'];
				$head['description'] = str_replace('"', ' ', $gallery_details['abstract']);
				$smarty->assign("head", $head);	
				$smarty->assign("gallery_details", $gallery_details);
				//print_r($gallery_details);
				$smarty->display("gallery_details.tpl");
				
			}
			else{
				
				$smarty->assign("error_heading", "404 Page Not Found");
				$smarty->assign("error_message", "Podany gallery nie istnieje.");
	
				
				$template = "errors/error_general.tpl";
				$smarty->display($template);
				exit;					
				
				
				
			}		
			
		}
		else{
			
			$smarty->assign("error_heading", "404 Page Not Found");
			$smarty->assign("error_message", "Nie kombinuj z url ;-).");

			
			$template = "errors/error_general.tpl";
			$smarty->display($template);
			exit;				
			
			
		}

		
	}	
//-------------------------------------------------------------
// Lista gallerya
//-------------------------------------------------------------

		
	else{
	
		// ustawienie numeru strony do stronicowania (jezeli nie została podana)
		if (!$url_config['2']) {
			$url_config['2'] = 1;
		}
		
		//Szczegoly kategorii
		if($url_config['1']){
			
			$category_details = $staff->getStaffByUrlNameToView($url_config['1'], 1);
			$smarty->assign("category_details", $category_details);
			
		}
		
		//Tytuły meta
		$head = array();
		$head['title'] = $category_details['title'];
		$head['desc'] = strip_tags($category_details['content']) ;
		$smarty->assign("head", $head);		
		
		//$limit = $__CFG['gallery_count'];
		$limit = 500;
		$gallery_list = $gallery->getGallerysByViewAndCategory("order", "asc", $limit, $url_config['2'], $_SESSION['lang'], $category_details['staff_id']);
		//print_r($gallery_list);
		$smarty->assign("gallery_list", $gallery_list);
		$smarty->assign("paging", $gallery->paging);
		
		$smarty->display("gallery.tpl");
	
	}
	
//-------------------------------------------------------------
// Nieznany parametr


		

	
	

	
	



?>