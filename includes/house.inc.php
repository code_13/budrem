<?php
/*
 * obsługuje prezentację kategorii produktów
 */

// print_r($_SESSION);

require_once('House.class.php');
$house = new House();

//Kategorie housea

require_once('Category.class.php');
$category = new Category();

$house_categories = $category->createMenuCategoriesToView();
$smarty->assign("house_categories", $house_categories);
		
$intro_main = 7;
require_once('includes/introduction.inc.php');



//-------------------------------------------------------------
// Szczegóły housea
//-------------------------------------------------------------
	
	if($url_config['1'] == "view"){
		
		if($url_config['2']){
			
			
			$house_details = $house->getHouseByUrlNameToView($url_config['2'], $_SESSION['lang']);
			
			if($house_details){
				
				
				//Zdjęcia przypisane do informacji
				require_once('Portfolio.class.php');
				$portfolio = new Portfolio();				
				$portfolio_list = $portfolio->getPortfoliosByCategoryToView(100, 1, $_SESSION['lang'], $house_details['house_id']);
				//print_r($portfolio_list);
				$smarty->assign("portfolio_list", $portfolio_list);				
				
				//Tytuły meta
				$head = array();
				$head['title'] = $house_details['title'];
				$head['description'] = str_replace('"', ' ', $house_details['abstract']);
				$smarty->assign("head", $head);	
				$smarty->assign("house_details", $house_details);
				//print_r($house_details);
				$smarty->display("house_details.tpl");
				
			}
			else{
				
				$smarty->assign("error_heading", "404 Page Not Found");
				$smarty->assign("error_message", "Podany house nie istnieje.");
	
				
				$template = "errors/error_general.tpl";
				$smarty->display($template);
				exit;					
				
				
				
			}		
			
		}
		else{
			
			$smarty->assign("error_heading", "404 Page Not Found");
			$smarty->assign("error_message", "Nie kombinuj z url ;-).");

			
			$template = "errors/error_general.tpl";
			$smarty->display($template);
			exit;				
			
			
		}

		
	}	
//-------------------------------------------------------------
// Lista housea
//-------------------------------------------------------------
	
	elseif(!$url_config['1']){
		
		header("Location: ".$default_path."domy/index/");
		
	}
		
	elseif($url_config['1'] == "index"){
	
		// ustawienie numeru strony do stronicowania (jezeli nie została podana)
		if (!$url_config['2']) {
			$url_config['2'] = 1;
		}
		
		//Tytuły meta
		$head = array();
		$head['title'] = "Domy na sprzedaż";
		$smarty->assign("head", $head);		
		//echo"house";
		$limit = 50;
		$house_list = $house->getHousesByView("order", "desc", $limit, $url_config['2'], $_SESSION['lang']);
		//print_r($house_list);

		$smarty->assign("house_list", $house_list);
		$smarty->assign("paging", $house->paging);
		
		$smarty->display("house.tpl");
	
	}
	
//-------------------------------------------------------------
// index housea wg kategorii
//-------------------------------------------------------------
		
	elseif($url_config['1'] == "miasto"){
	
		// ustawienie numeru strony do stronicowania (jezeli nie została podana)
		if (!$url_config['3']) {
			$url_config['3'] = 1;
		}
		
		//Szczegoly kategorii
		if($url_config['2']){
			
			$category_details = $category->getGroupByUrlName($url_config['2']);
			$smarty->assign("category_details", $category_details);
			
		}
		
		//Tytuły meta
		$head = array();
		$head['title'] = "House";
		$smarty->assign("head", $head);		
		
		//$limit = $__CFG['house_count'];
		$limit = 5;
		$house_list = $house->getHousesByViewAndCategory("order", "desc", $limit, $url_config['3'], $_SESSION['lang'], $category_details['id']);
		//print_r($house_list);
		$smarty->assign("house_list", $house_list);
		$smarty->assign("paging", $house->paging);
		
		$smarty->display("house.tpl");
	
	}
	
//-------------------------------------------------------------
// Nieznany parametr
//-------------------------------------------------------------	
	
	
	else{
		
		
			$smarty->assign("error_heading", "404 Page Not Found");
			$smarty->assign("error_message", "The page you requested was not found.");

			
			$template = "errors/error_general.tpl";
			$smarty->display($template);
			exit;		
		
		
	}

		

	
	

	
	



?>