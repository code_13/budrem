<?php

// --------------------------------------------------------------
// log in user
// --------------------------------------------------------------

if ($_REQUEST['action'] == "LoginUser") {

	sleep(1);
 
	unset($_SESSION['user_data']);
	//Niszczymy poprzednie komunikaty
	unset($_SESSION['message']);
	
	if (!sizeof($_SESSION['user_data'])) {
		
		if ($_POST['login_form']['email']) {
			
			
			//print_r($_POST);
			// niezalogowany i podano dane do autoryzacji
			
			$user_details = $user->loginUser($_POST['login_form']);
			
			
	       	if (sizeof($user_details)) {
	           	
	           	// logged correct
	       		$_SESSION['user_data'] = $user_details;
	       		$smarty->assign('user_data', $_SESSION['user_data']);
	       		
	       		$return['validate'] = 1;
	       		$return['success_message'] = 'Udało się...';
	       		$_SESSION['message']['good_message'] = "Zostałeś zalogowany";
	       		
				//Istnieje w sesji żadanie przekierowania do url
		       if($_SESSION['target']){

	       			// przenieś się do podanego wczesenij adresu
	       			$link = base64_decode($_SESSION['target']);
	       			unset($_SESSION['target']);      			
	       			
	       			$return['redir'] = $link;
	       			
	       		}

	       		
	       		       		
	    
	       	}
	       	else {
	       		// błąd przy logowaniu
				$return['validate'] = 0;
				$return['error_message'] = 'Nieprawidłowy login lub hasło.';
				//$return['error']['email'] = 'Proszę podać adres e-mail';

	           	// na wszelki wypadek niszczymy bieżacą sesję usera
				unset($_SESSION['user_data']);
				
	    	}
	    }
	    else {
	    
	       		// błąd przy logowaniu
				$return['validate'] = 0;
				$return['error_message'] = 'Proszę podać adres e-mail';
	       		$return['error']['email'] = 'Proszę podać adres e-mail';

	           	// na wszelki wypadek niszczymy bieżacą sesję usera
				unset($_SESSION['user_data']);	    
	    
	    
	    }
	}
	
	echo json_encode($return);
}


// --------------------------------------------------------------
// logout user
// -------------------------------------------------------------- 

if ($url_config['0'] == "wyloguj") {

    // usuwamy dane usera z sesji
    unset($_SESSION['user_data']);
	//Niszczymy poprzednie komunikaty
	unset($_SESSION['message']);
	unset($_SESSION['order']);
    unset($_SESSION['bonus_add']);
    unset($_SESSION['bonus_min']);
    unset($_SESSION['active_book']);
    $_SESSION['message']['good_message'] = "Zostałeś wylogowany";
    	
	// i kierujemy się na stronę główną

    header('Location: /');
}