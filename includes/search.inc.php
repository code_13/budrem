<?php

require_once('Blog.class.php');
$blog = new Blog();

//--------------------------------------------------------------
// Wyczyszczenie wynikow wyszukiwania
//--------------------------------------------------------------

if($url_config['1'] == "none"){
	
	
	unset($_SESSION['search_form']);
	$_SESSION['message']['good_message'] = "Wyniki wyszukiwania zostały usunięte";
	header("location: /");
}


//Zabezpieczenie przed brakiem podania numeru strony do stronicowania
if($url_config['1']){
	
	$_SESSION['search_form_page'] = $url_config['1'];
	
}
else{
	
	$_SESSION['search_form_page'] = 1;
	
}

//-----------------------------------------------------
// Akcja wyszukiwania
//----------------------------------------------------


if($_REQUEST['action'] == "Search"){
	
	
	
	if (sizeof($_POST['search_form'])) {
		
			if($_POST['search_form']['title'] == "Szukaj"){
				
				$_POST['search_form']['title'] = "";
			}
			else{
				
				$fraza = addslashes($_POST['search_form']['title']);
				$_POST['search_form']['title'] = $fraza;
			}
			
			
		
			$_SESSION['search_form'] = $_POST['search_form'];
			//print_r($_POST['search_form']);

		
			header("location: /szukaj");
	}	

	
}
else{
	
	if(sizeof($_SESSION['search_form'])){

		$search_form = $_SESSION['search_form'];
		// lista wyników
		$limit = 10;
		//$search_data = $product->getProductsSearch($limit, "order", "desc", $search_form, $_SESSION['search_form_page'], $_SESSION['lang']);
		
		$search_data = $blog->getBlogsSearchForView($limit, "order", "desc", $search_form, $_SESSION['search_form_page'], $_SESSION['lang']);
		
		$smarty->assign("article_list", $search_data);
		$smarty->assign("paging", $blog->paging);
		$fraza2 = stripslashes($_SESSION['search_form']['title']);
		$_SESSION['search_form']['title'] = $fraza2;
		$smarty->assign("ret_post", $_SESSION['search_form']);
		
		
		
		
	}
}

?>
