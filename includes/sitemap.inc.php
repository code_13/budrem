<?php
require_once('Blog.class.php');
$blog = new Blog();

require_once('Recipe.class.php');
$recipe = new Recipe();

require_once('Place.class.php');
$place = new Place();

require_once('Specialist.class.php');
$specialist = new Specialist();

$blog_list = $blog->getBlogsAll($_SESSION['lang']);
$recipe_list = $recipe->getRecipesAll($_SESSION['lang']);
$place_list = $place->getPlacesAll($_SESSION['lang']);
$specialist_list = $specialist->getSpecialistsAll($_SESSION['lang']);

header("Content-Type: text/xml");
$smarty->assign("blog_list", $blog_list);
$smarty->assign("recipe_list", $recipe_list);
$smarty->assign("place_list", $place_list);
$smarty->assign("specialist_list", $specialist_list);


