<?php

/**
 * obsługa sesji usera 
 */

require_once ('User.class.php');
global $User;
$user = new User;



		
// --------------------------------------------------------------
// Update information of user
// --------------------------------------------------------------

		//Jeśli zażdano zmiany adres email przez użytkownika
		if($_REQUEST['action'] == "UpdateUser"){
				
			if (sizeof($_POST['register_form'])) {
				
				//Sprawdzamy poprawność przesłanych danych za pomoca walidatora
				require_once('validate_user_profile.inc.php');
	
				if (!sizeof($error['register_form'])) {
//					/print_r($_POST['register_form']);
					
					//Update do bazy
					$user->updateUser($_POST['register_form']);
					
					$user_details = $user->getUser($_SESSION['user_data']['id']);
					
					//Zapisanie sie do newslttera
					if ($_POST['register_form']['newsletter'] == 1){
						
						//Sprawdzamy czy user jest zapisany już do newslettera
						require_once ('Newsletter.class.php');
						$newsletter = new Newsletter();
						$newsletter->subscribeNewsletter($user_details['email'], $user_details['id']);				
						
					}		
					//wypisanie z newslettera
					else{
						
						require_once ('Newsletter.class.php');
						$newsletter = new Newsletter();
						$newsletter->unsubscribeNewsletterById($user_details['id']);						
						
						
						
					}
		
		
			       	$return['validate'] = 1;
			       	$return['success_message'] = 'Udało się...';
		
					// komunikat do widoku
					$_SESSION['message']['good_message'] = "Gratulujemy. Konto zostało utworzone. Wysłaliśmy do Ciebie e-mail z dalszymi instrukcjami odnośnie Twojej resjestracji.";
							
					

					//Wyciągam zaktualizowane dane
					$user_details = $user->getUser($_SESSION['user_data']['id']);
					//print_r($user_details);
					// I napisuję je w sesji
					$_SESSION['user_data'] = $user_details;
					//Przekazuje do widoku
					$smarty->assign("user_data", $_SESSION['user_data']);
					
					$_SESSION['message']['good_message'] = "Dane zostały zaktualizowane";
					$smarty->assign("good_message", $_SESSION['message']['good_message']);

				}
				else{
					
					// błąd
					$smarty->assign("register_message", $error['register_form']['message']);
					$smarty->assign("error", $error['register_form']);
					$smarty->assign("ret_post", $_POST['register_form']);
					
					$return['validate'] = 0;
					$return['error_message'] = 'Nieprawidłowy login lub hasło.';
					$return['error'] = $error['register_form'];
				}
			
			}
			
		}
echo json_encode($return);

    // --------------------------------------------------------------
// user profile to view
// --------------------------------------------------------------
 
if (sizeof($_SESSION['user_data'])) {
	//print_r($_SESSION['user_data']);
	$smarty->assign("user_data", $_SESSION['user_data']);
}    
?>