<?php

/**
 * obsługa sesji usera 
 */

require_once ('User.class.php');
$user = new User;

// --------------------------------------------------------------
// logout user
// -------------------------------------------------------------- 

if ($_REQUEST['action'] == "LogoutUser") {

    // usuwamy dane usera z sesji
    unset($_SESSION['user_data']);
	//Niszczymy poprzednie komunikaty
	unset($_SESSION['message']);
	unset($_SESSION['order']);
    unset($_SESSION['bonus_add']);
    unset($_SESSION['bonus_min']);
    $_SESSION['message']['good_message'] = "Zostałeś wylogowany";
    	
	// i kierujemy się na stronę główną

    header('Location: /');
}

// --------------------------------------------------------------
// user profile to view
// --------------------------------------------------------------
 
if (sizeof($_SESSION['user_data'])) {
	//print_r($_SESSION['user_data']);
	$smarty->assign("user_data", $_SESSION['user_data']);
}

//print_r($_SESSION['user_data']);   
?>