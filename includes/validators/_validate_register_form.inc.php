<?php
	
	require_once('Utils.class.php');
	
	$error = array();


	if (!$_POST['register_form']['email']) {
		$error['register_form']['email'] = "Podaj proszę adres e-mail";
	}
	else{
		
		if (!Utils::isEmail($_POST['register_form']['email'])) {
			$error['register_form']['email'] = "Podany adres e-mail nei jest prawidłowy";
		}
		if ($user->checkIfEmailExists($_POST['register_form']['email'])) {
			$error['register_form']['email'] = "Ten adres jest już zarejestrowany";		// podany adres już istnieje
		}		
		
	}

	
	if (!$_POST['register_form']['password']) {
		$error['register_form']['password'] = "Podaj hasło";
	}
	else if ($_POST['register_form']['password'] != $_POST['register_form']['confirm_password']) {
		$error['register_form']['confirm_password'] = "Podane hasła nie są takie same";
	}
	
	if (!$_POST['register_form']['confirm_password']) {
		$error['register_form']['confirm_password'] = "Podaj ponownie swoje hasło";
	}
	
	if (!$_POST['register_form']['name']) {
		$error['register_form']['name'] = "Powiesz nam jak masz na imię?";
	}
	
	if (!$_POST['register_form']['city']) {
		$error['register_form']['city'] = "Gdzie mieszkasz?";
	}
	
	if (!$_POST['register_form']['birth_date']) {
		$error['register_form']['birth_date'] = "Chemy założyć Ci życzenia, kiedy masz urodziny?";
	}	
	
	if (!$_POST['agree']) {
		$error['register_form']['agree'] = "Proszę zaakceptować warunki";	// brak akceptacji regulaminu
	}


?>