<?php
	require_once ('User.class.php');
	$user = new User;	
	
	require_once('Utils.class.php');
	
	$error = array();
	

	if (!$_POST['register_form']['email']) {
		$error['register_form']['email'] = "Podaj swój adres email";
	}
	elseif (!Utils::isEmail($_POST['register_form']['email'])) {
		$error['register_form']['email'] = "Podaj prawidłowy adres email";
	}
	
	if ($_POST['register_form']['email'] != $_POST['register_form']['email_old']) {
		$details = $user->getUserByOtherEmailAndId($_POST['register_form']['email'], $_SESSION['user_data']['id']);
		if (sizeof($details)) {
			// taki email jest wykorzystywany przez innego usera, więc nie możemy go wykorzystać
			$error['register_form']['email'] = "ten adres jest już wykorzystywany przez inne konto";
		}
	}
	
	if (!$_POST['register_form']['password']) {
		$error['register_form']['password'] = "Podaj swoje hasło.";
	}
	else if ($_POST['register_form']['password'] != $_POST['register_form']['confirm_password']) {
		$error['register_form']['confirm_password'] = "Podane hasła różnią się od siebie";
	}
	
	if (!$_POST['register_form']['confirm_password']) {
		$error['register_form']['confirm_password'] = "Potwierdź swoje hasło";
	}
	
	if (!$_POST['register_form']['name']) {
		$error['register_form']['name'] = "Podaj swoje imię";
	}
	if (!$_POST['register_form']['birth_date']) {
		$error['register_form']['birth_date'] = "Podaj swoją datę urodzenia";
	}	

	if (sizeof($error)) {
		$error['register_form']['message'] = "Sprawdź niepoprawnie wypełnione pola";
	}

	
?>