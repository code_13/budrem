<?php
	
	require_once('Utils.class.php');
	
	$error = array();
	
	if (!$_POST['contact_form']['email']) {
		$error['contact_form']['email'] = "Pole wymagane.";
	}
	elseif (!Utils::isEmail($_POST['contact_form']['email'])) {
		$error['contact_form']['email'] = "Adres email nie jest poprawny.";
	}
	if (!$_POST['contact_form']['name']) {
		$error['contact_form']['name'] = "Pole wymagane.";
	}

	if (!$_POST['contact_form']['content']) {
		$error['contact_form']['content'] = "Pole wymagane.";
	}
	

?>