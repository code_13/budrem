<?php

	
	
	include('includes/default.php');
	

	
	
// -------------------------------------------------------
// Powiadomienia
// -------------------------------------------------------

	//Powodzenie
	if($_SESSION['message']['good_message']){
		
		$smarty->assign("good_message", $_SESSION['message']['good_message']);
		unset($_SESSION['message']['good_message']);
		
	}
	//Błąd
	if($_SESSION['message']['bad_message']){
		
		$smarty->assign("bad_message", $_SESSION['message']['bad_message']);
		unset($_SESSION['message']['bad_message']);
		
	}	
	

//------------------------------------------------
//	Homepage
//------------------------------------------------	
	if($url_config['0'] == "index"){
		
		include('includes/index.inc.php');
		$smarty->assign("script", "home");
		//Tytuły meta
		$head = array();
		$head['title'] = "Start";
		$smarty->assign("head", $head);	
		$smarty->display("index.tpl");
		
		
	}
//------------------------------------------------
//	Wiadomości
//------------------------------------------------	
	
	elseif($url_config['0'] == "blog"){

		$smarty->assign("script", "blog");
		include('includes/blog.inc.php');


	}
//------------------------------------------------
//	Domy na sprzedaż
//------------------------------------------------	
	
	elseif($url_config['0'] == "domy"){

		$smarty->assign("script", "house");
		include('includes/house.inc.php');


	}
//------------------------------------------------
//	Galeria
//------------------------------------------------	
	
	elseif($url_config['0'] == "oferta"){

		$smarty->assign("script", "gallery");
		include('includes/gallery.inc.php');


	}
//------------------------------------------------
//	Apartamenty
//------------------------------------------------	
	
	elseif($url_config['0'] == "apartamenty"){

		$smarty->assign("script", "product");
		include('includes/product.inc.php');


	}
		
//------------------------------------------------		
//	O nas
//------------------------------------------------	
	elseif($url_config['0'] == "o-nas"){
	
		include('includes/about.inc.php');
		
		$smarty->assign("script", "about");
		
		//Tytuły meta
		$head = array();
		$head['title'] = "O nas";
		$smarty->assign("head", $head);	
		$smarty->display("aboutus.tpl");
		
	}	
//------------------------------------------------		
//	Faq
//------------------------------------------------	
	elseif($url_config['0'] == "faq"){
	
		include('includes/faq.inc.php');
		
		$smarty->assign("script", "faq");
		
		//Tytuły meta
		$head = array();
		$head['title'] = "Baza wiedzy - Najczęściej zadawane pytania";
		$smarty->assign("head", $head);	
		$smarty->display("faq.tpl");
		
	}	
//------------------------------------------------		
//	Kontakt
//------------------------------------------------	
	elseif($url_config['0'] == "kontakt"){
	
		include('includes/contact.inc.php');
		
		$smarty->assign("script", "contact");
		
		//Tytuły meta
		$head = array();
		$head['title'] = "Kontakt";
		$smarty->assign("head", $head);	

		$smarty->display("contact.tpl");
		
	}
	
//-------------------------------------------------------------------
//	Zapytanie o produkt, którego nie ma w tej chwili na stanie - ajax
//-------------------------------------------------------------------	
	elseif($url_config['0'] == "contactForm"){

		include('includes/contactForm.inc.php');

	}

//------------------------------------------------
//	Generowanie obrazka
//------------------------------------------------	
	
	elseif($url_config['0'] == "captcha"){
	
	
		include('includes/captcha.inc.php');
		
	}

//------------------------------------------------
//	Dunamiczne wyszukiwanie
//------------------------------------------------	
	
	elseif($url_config['0'] == "suggest"){
	
	
		include('includes/suggest.inc.php');
		$template = "suggest.tpl";
		$smarty->display($template);
		
	}
//------------------------------------------------
//	Obrazek captcha
//------------------------------------------------	
	
	elseif($url_config['0'] == "captcha_image"){
		
		$smarty->display("captcha.tpl");
		
		
	}	
	
//------------------------------------------------
//	Wyszukiwanie
//------------------------------------------------	
	
	elseif($url_config['0'] == "szukaj"){
	
		//Tytuły meta
		$head = array();
		$head['title'] = "Wyniki wyszukiwania";
		$smarty->assign("head", $head);		
		include('includes/search.inc.php');
		$smarty->display("search.tpl");
		
	}
//------------------------------------------------
//	Mapa witryny
//------------------------------------------------	
	
	elseif($url_config['0'] == "sitemap.xml"){

		require_once('includes/sitemap.inc.php');
		$smarty->display("sitemap.tpl");
		
	}
//------------------------------------------------
//	Administarcja
//------------------------------------------------
	/*
	elseif($url_config['0'] == "admin"){
	
	
		header("Location: /_panel/");
		
	}	
	*/
	
	
	else{
		
		
		
		$smarty->assign("error_heading", "404 Page Not Found");
		$smarty->assign("error_message", "The page you requested was not found.");
		
		$template = "errors/error_general.tpl";
		$smarty->display($template);
		
		
	
	
	
	}
//echo $_SESSION['registration_place_id'];	

//print_r($_SESSION);


?>