<?

require_once 'Utils.class.php';

class Category {
	
	/**
	 * @var object DBManager
	 */
	var $DBM;
	
	var $message;
	var $paging;

	function Category() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * tworzy menu z kategoriami (grupami) produktowymi - rekurencyjnie!
	 * zasada : doklejamy kolejne subgrupy dopty dopóki jakiekolwiek są ;-)
	 */
	
	function createMenuCategories ($parent = 0, $status = 0) {
		
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where 1 = 1 ";
		
		if ($status) {
			
			$sql .= " and status = '$status' ";
		}
		
		if($parent == 0){
			$sql .= " order by `order` asc ";
		}
		else{
			
			$sql .= " order by name asc ";
		}
		
		$groups = $this->DBM->getTable($sql);
		
		
		
		//print_r($groups);
		return $groups;

	}	
	
	/**
	 * tworzy menu z kategoriami (grupami) produktowymi - rekurencyjnie!
	 * zasada : doklejamy kolejne subgrupy dopty dopóki jakiekolwiek są ;-) - dla zawezania grup wyszukiwania
	 */
	
	function createMenuCategoriesByCategory ($category_id) {
		
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where 1 = 1 ";

		
		if($category_id){
			
			$sql .= "and category_id = '$category_id' ";
			$sql .= " order by `order` asc ";
		}
		else{
			
			$sql .= " order by name asc ";
		}
		
		$groups = $this->DBM->getTable($sql);
		return $groups;

	}
	
	
	/**
	 * uaktualnia automatycznie kolejnosc produktow w kategorii
	 */
	
	function updateOrder($category_id, $position) {
			
		if ($category_id && $position) {
			
			$sql = "update category set `order` = '$position' where id = '$category_id' ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;

	}
	
	/**
	 * sprawdza czy dany blog istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego wpisu/
	 */
	
	function getCategoryByUrlNameAndId($url_name, $category_id) {
			
		if ($url_name && $category_id) {	
			//print_r($product_id);
			$sql = "select category.* from category where category.url_name = '$url_name' and category.id != '$category_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany wpis bloga istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo wpisu/
	 */
	
	function getCategoryByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select category.* from category where category.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * tworzy menu z kategoriami (grupami) produktowymi - rekurencyjnie!
	 * zasada : doklejamy kolejne subgrupy dopty dopóki jakiekolwiek są ;-)
	 */
	
	function createMenuCategoriesToView ($parent = 0, $status = 0) {
		
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where 1 = 1 ";
		
		if ($status) {
			//print_r($status);
			$sql .= " and status = '$status' ";
		}
		
		$sql .= " order by `order` asc ";
		
		$groups = $this->DBM->getTable($sql);
		return $groups;

	}
	
	/**
	 * tworzy menu z kategoriami (grupami) produktowymi - rekurencyjnie!
	 * zasada : doklejamy kolejne subgrupy dopty dopóki jakiekolwiek są ;-)
	 */
	
	function createMenuCategoriesToSearch ($parent = 0, $status = 0) {
		//echo $parent;
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where subcategory_id = '$parent' ";
		
		if ($status) {
			//print_r($status);
			$sql .= " and status = '$status' ";
		}
		
		$sql .= " order by `order` asc ";
		
		$groups = $this->DBM->getTable($sql);
		return $groups;

	}
	
	/**
	 * tworzy menu z kategoriami (grupami) produktowymi - rekurencyjnie!
	 * zasada : doklejamy kolejne subgrupy dopty dopóki jakiekolwiek są ;-)
	 */
	
	function createMenuCategoriesToViewForCategory ($category_id, $parent = 0, $status = 0) {
		
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where subcategory_id = '$category_id' ";
		
		if ($status) {
			//print_r($status);
			$sql .= " and status = '$status' ";
		}
		
		$sql .= " order by `order` asc ";
		
		$groups = $this->DBM->getTable($sql);
		return $groups;

	}
	
	/**
	 * tworzy menu z kategoriami (grupami) produktowymi - rekurencyjnie!
	 * zasada : doklejamy kolejne subgrupy dopty dopóki jakiekolwiek są ;-)
	 */
	
	function createMenuGraph ($parent = 0, $status = 0) {
		
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where parent = '$parent' and type = '2' ";
		
		if ($status) {
			$sql .= " and status = '$status' ";
		}
		
		$sql .= " order by `order` asc ";
		
		$groups = $this->DBM->getTable($sql);
		
		if (sizeof($groups)) {
			
			// znalezione subkategorie - dla każdej z nich wywołujemy jeszcze raz metodę
			foreach ($groups as $key => $details) {
				
				$sub = $this->createMenuCategories($details['id']);
				
				if (sizeof($sub)) {
					$groups[$key]['sub'] = $sub;
				}
			} 
			
			// zwracamy wyciągniętą tablicę z kategoriami
			return $groups;
		}
	}
	
	/**
	 * wyciąga szczegóły podanej kategorii na podstawie url_config
	 */
	
	function getProductCategory ($category_id) {
		
		if ($category_id) {
		
			$sql = "select * from `category` where url_name = '$category_id'";
			$category_details = $this->DBM->getRow($sql);
			
			return $category_details;
		}
	}

	
	/**
	 * wyciąga szczegóły podanej kategorii na podstawie id
	 */
	
	function getProductCategoryById ($category_id) {
		
		if ($category_id) {
		
			$sql = "select * from `category` where id = '$category_id'";
			$category_details = $this->DBM->getRow($sql);
			
			return $category_details;
		}
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($category_id, $status) {
		//print_r($status);
		if ($category_id) {
			
			$sql = "update category set status = '$status' where id = '$category_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}		
	
	/**
	 * zapisuje kategorię produktu
	 */
	
	function saveProductCategory ($category_form) {
		
		if (sizeof($category_form)) {
			
			if (!$category_form['id']) {
				
				
				// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
				$sql = "select max(`order`) as last_order from `category` where 1 = 1";
				$order = $this->DBM->getRow($sql);
				$next_order = $order['last_order'] + 1;
			

					

					
				//Jeśli nowa kategoria główna - nowa metryka wraz z kolejnoscia
				$sql = "insert into `category` (name, url_name, status, `order`) values ('$category_form[name]', '$category_form[url_name]', '$category_form[status]', '$next_order')";

				
			
			
			}
			else {

				// aktualizacja kategorii
				$sql = "update `category` set name = '$category_form[name]', url_name = '$category_form[url_name]', status = '$category_form[status]' where id = '$category_form[id]'";				
			}
			
			$this->DBM->modifyTable($sql);
			
			if (!$category_form['id']) {
				$category_id = $this->DBM->lastInsertID;
			}
			else {
				$category_id = $category_form['id'];
			}
			
			return $category_id;
		}
	}
	
	/**
	 * wyciąga listę dostepnych grup produktów
	 */
	
	function getGroups () {
		
		$sql = "select * from `category` ";
		$groups = $this->DBM->getTable($sql);
		
		return $groups;
	}	
	
	/**
	 * wyciąga nazwe kategorii
	 */
	
	function getGroup ($category_id) {
		
		if($category_id){
		
			$sql = "select * from `category` where id = '$category_id' ";
			$group = $this->DBM->getRow($sql);
			//print_r($group);
			return $group;
		}
	}
	
	/**
	 * wyciąga nazwe kategorii wg url+config
	 */
	
	function getGroupByUrlName ($url_name) {
		
		if($url_name){
		
			$sql = "select * from `category` where url_name = '$url_name' ";
			$group = $this->DBM->getRow($sql);
			//print_r($group);
			return $group;
		}
	}
	
	/**
	 * wyciąga listę grup dla podanej kategorii (grupy)
	 */
	
	function getCategoriesByCategory ($category_id) {
		
		if (!$category_id) $category_id = 0;
		
		// if ($category_id) {
			
			// $sql = "select gr.* from `category` gr where gr.parent = '$category_id' order by name asc ";
			$sql = "select gr.* from `category` gr where gr.parent = '$category_id' and status = 1 order by name asc";
			$categories = $this->DBM->getTable($sql);
			
			if (sizeof($categories)) {
				
				foreach ($categories as $key => $category) {
					
					$categories[$key]['name'] = ltrim($category['name'], "1");
				}
			}
			
			return $categories;
		// }
	}
	
	/**
	 * usuwa kategorię produktu
	 */
	
	function removeProductCategory ($category_id) {
		
		if ($category_id) {
			
			
			// usuwamy samą kategorię
			$sql = "delete from `category` where id = $category_id ";
			$rv = $this->DBM->modifyTable($sql);
				
				
			
			
			

		}
		return $rv;
	}
	
	/**
	 * Tworzy listę kategorii dla administracji
	 */
	
	function createCategoriesForAdmin($status = 0) {
		
		// sprawdzamy czy mamy jakieś subkategorie dla podanej kategorii
		$sql = "select * from `category` where 1 = 1 ";
		
		if ($status) {
			$sql .= " and status = '$status' ";
		}
		
		$sql .= " order by `order` asc ";
		
		$groups = $this->DBM->getTable($sql);
		
		if (sizeof($groups)) {
			
			// znalezione subkategorie - dla każdej z nich wywołujemy jeszcze raz metodę
			foreach ($groups as $key => $details) {
				
				$sub = $this->createMenuCategories($details['id']);
				
				if (sizeof($sub)) {
					$groups[$key]['sub'] = $sub;
				}
			} 
			
			// zwracamy wyciągniętą tablicę z kategoriami
			return $groups;
		}
	}
	
	/**
	 * Pierwsza aktywna i bez podkategorii dla administracji
	 */
	
	function firstCatergory () {
		
		// sprawdzamy czy są wogóle jakieś kategorie
		$sql = "select * from `category` where parent = 0 and status = 1";
		$groups_temp = $this->DBM->getTable($sql);
		
		if (sizeof($groups_temp)) {
			
			//Jesli sa - reindexujemy ich tablicę
			$groups = array();
			foreach ($groups_temp as $group_details) {
				$groups[$group_details[id]] = $group_details;
			}

			//Sprawdzamy czy kategoria posiada jakies subkategorie
			foreach($groups as $key => $category){
				
				$sql = "select * from `category` where parent = '$category[id]' and status = 1";
				$subcategories_temp = $this->DBM->getTable($sql);
				
				
				if(sizeof($subcategories_temp)){
					//Jesli sa - dopisujemy je do tablicy glownych - a nadrzedna usuwamy
					$subcategories = array();
					foreach ($subcategories_temp as $subcategory_details) {
						$groups[$subcategory_details[id]] = $subcategory_details;
						unset($groups[$subcategory_details['parent']]);
					}						
				}	
			}
			
			$new_groups = array();
			foreach($groups as $group){
				
				$new_groups[] = $group['id'];
				
				
			}

			// zwracamy wyciągniętą tablicę z kategoriami
			//print_r($groups);
			return $new_groups;
		}
	}
	
	
	
	
}
?>