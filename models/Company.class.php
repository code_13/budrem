<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Company {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Company() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * wyciąga listę dostepnych wydawców
	 */
	
	function getCompanysForHome($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id = 1) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company.category_id = '$category_id' and company_multilang.status = '2' and company_multilang.home = '1' and company_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.category_id = '$category_id' and company_multilang.status = '2' and company_multilang.home = '1' and company_multilang.language_id = '$lang_id' ";		
		$sql .= " order by rand() asc limit 3 ";
		
		// echo $sql."<hr>";
		
		$company_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($company_list_temp)) {
			$company_list = array();
			foreach ($company_list_temp as $company_details) {
				$company_list[$company_details[company_id]] = $company_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $company_list;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getCompanys($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company.category_id = '$category_id' and company_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.category_id = '$category_id' and company_multilang.language_id = '$lang_id' ";		
		$sql .= " order by company.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$company_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($company_list_temp)) {
			$company_list = array();
			foreach ($company_list_temp as $company_details) {
				$company_list[$company_details[company_id]] = $company_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $company_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($company_id, $status) {
		
		if ($company_id) {
			
			$sql = "update company_multilang set status = '$status' where company_multilang.company_id = '$company_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}

	/**
	 * Przestawia status  na strone glowna
	 */
	
	function setHome ($company_id, $status) {
		
		if ($company_id) {
			
			$sql = "update company_multilang set home = '$status' where company_multilang.company_id = '$company_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getCompanysSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND company.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND company.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(company_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(company_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(company_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(company_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(company_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(company_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND company_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND company_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND company_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND company_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND company_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND company_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by company.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$company_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($company_list_temp)) {
			$company_list = array();
			foreach ($company_list_temp as $company_details) {
				$company_list[$company_details[company_id]] = $company_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $company_list;
	}
	
	/**
	 * pobiera przefiltrowaną listę artykułów /dla widokow/
	 */
	
	function getCompanysSearchToView($limit, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {

		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND company.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND company.category_id = ".$search_form['category_id']." ";
			}
			else{
				$sql .= " AND (company.category_id = 1 or company.category_id = 3 or company.category_id = 10 or company.category_id = 11) ";
				$sql_count .= " AND (company.category_id = 1 or company.category_id = 3 or company.category_id = 10 or company.category_id = 11) ";				
				
			}
			if ($search_form['phrase']) {
				$sql .= " AND (lower(company_multilang.title) like lower('%".$search_form['phrase']."%') or lower(company_multilang.content) like lower('%".$search_form['phrase']."%')) ";
				$sql_count .= " AND (lower(company_multilang.title) like lower('%".$search_form['phrase']."%') or lower(company_multilang.content) like lower('%".$search_form['phrase']."%')) ";
			}

		}
		
		$sql .= " order by company.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$company_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($company_list_temp)) {
			$company_list = array();
			foreach ($company_list_temp as $company_details) {
				$company_list[$company_details[company_id]] = $company_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $company_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getCompanysForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$company_list = $this->getCompanysSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_company where product_id = '$product_id'";
			$companys_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($companys_temp)) {
				foreach ($companys_temp as $details) {
					if ($company_list[$details['company_id']]) {
						$company_list[$details['company_id']]['selected'] = 1;
					}
				}
			}
			
			return $company_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getCompanysOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang, product_company where company_multilang.company_id = company.id and product_company.company_id = company.id and company_multilang.language_id = '$lang_id' and product_company.product_id = '$product_id' ";
			$company_list = $this->DBM->getTable($sql);
			
			return $company_list;
		}
	}
	
	
	/**
	 * get companys by category id
	 */
	
	function getCompanysByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$company_list = $this->getCompanysSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $company_list;
		}
	}
	
	/**
	 * get companys by category id
	 */
	
	function getCompanysToMenu ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$company_list = $this->getCompanysSearch("order", "asc", $search_form, 1, $lang_id, $page_number);
			
			return $company_list;
		}
	}
	
	/**
	 * wyciaga dane o znajomych
	 */
	
	function getCompanysByNews ($limit, $page_number, $lang_id){
		
		if($limit){

			global $__CFG;
		// TEST!!!
		$__CFG['record_count_limit'] = $limit;
		
		$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' and company.category_id = '5' order by date_created desc";		
			$sql_count  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' and company.category_id = '5' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getCompanysByCategoryToView ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.status = '2' and company_multilang.language_id = '$lang_id' and company.category_id = '$category_id' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company_multilang.status = '2' and company_multilang.language_id = '$lang_id' and company.category_id = '$category_id' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getCompanysByHomeToView ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' and company.category_id = '$category_id' and company_multilang.home = '1' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' and company.category_id = '$category_id' and company_multilang.home = '1' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii biorąc pod uwagę typ wiadomości
	 */
	
	function getCompanysByCategoryAndTypeToView ($type, $limit, $page_number, $lang_id, $category_id){
		
			if($type && $limit && $page_number && $lang_id && $category_id){
			
			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' and company.category_id = '$category_id' and company_multilang.type = '$type' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from company, company_multilang where company_multilang.company_id = company.id and company_multilang.language_id = '$lang_id' and company.category_id = '$category_id' and company_multilang.type = '$type' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}

	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego artykułu/
	 */
	
	function getCompanyByUrlNameAndId($url_name, $company_id) {
			
		if ($url_name && $company_id) {	
			//print_r($company_id);
			$sql = "select company_multilang.* from company_multilang where company_multilang.url_name = '$url_name' and company_multilang.company_id != '$company_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo artykułu/
	 */
	
	function getCompanyByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select company_multilang.* from company_multilang where company_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getCompany($company_id, $lang_id) {
			
		if ($company_id && $lang_id) {	
			$sql = "select company.category_id, company.order, company.pic_01, company.pic_02, company.pic_03, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.id = '$company_id' and company_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getCompanyByUrlNameToView($url_name, $lang_id) {
		
		global $smarty;
			
		if ($url_name && $lang_id) {	
			$sql = "select company.category_id, company.order, company.pic_01, company.pic_02, company.pic_03, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.url_name = '$url_name' and company_multilang.language_id = '$lang_id'";
			$company_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($company_details)) {
				
				//Jesli istnieje dany artykuł - podajemy go tagow
				$tags['title'] = str_replace('"', '', $company_details['title']);
				$tags['abstract'] = str_replace('"', '', $company_details['abstract']);
				
				$smarty->assign("tags", $tags);
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.`order` > '$company_details[order]' and company.category_id = '$company_details[category_id]' and company_multilang.language_id = '$lang_id' and company_multilang.status != 1 order by company.`order` asc limit 1";
				$next_company = $this->DBM->getRow($sql);
				
				if (sizeof($next_company)) {
					$company_details['next'] = $next_company['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.`order` < '$company_details[order]' and company.category_id = '$company_details[category_id]' and company_multilang.language_id = '$lang_id' and company_multilang.status != 1 order by company.`order` desc limit 1";
				$previous_company = $this->DBM->getRow($sql);
				
				if (sizeof($previous_company)) {
					$company_details['previous'] = $previous_company['url_name'];
				}
			}		
		
		
		}
		return $company_details;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getCompanyByUrlNameAndTypeToView($type, $url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select company.category_id, company.order, company.pic_01, company.pic_02, company.pic_03, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company_multilang.url_name = '$url_name' and company_multilang.language_id = '$lang_id'";
			$company_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($company_details)) {
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.`order` > '$company_details[order]' and company.category_id = '$company_details[category_id]' and company_multilang.language_id = '$lang_id' and company_multilang.type = '$type' and company_multilang.status != 1 order by company.`order` asc limit 1";
				$next_company = $this->DBM->getRow($sql);
				
				if (sizeof($next_company)) {
					$company_details['next'] = $next_company['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.`order` < '$company_details[order]' and company.category_id = '$company_details[category_id]' and company_multilang.language_id = '$lang_id' and company_multilang.type = '$type' and company_multilang.status != 1 order by company.`order` desc limit 1";
				$previous_company = $this->DBM->getRow($sql);
				
				if (sizeof($previous_company)) {
					$company_details['previous'] = $previous_company['url_name'];
				}
			}		
		
		
		}
		return $company_details;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstCompanyInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select company.id from company, company_multilang where company_multilang.company_id = company.id and company.category_id = '$category_id' and company_multilang.language_id = '$lang_id' and company_multilang.status != 0 order by company.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getCompanyDetails($company_id, $lang_id) {
		
		if ($company_id && $lang_id) {
			$sql = "select company.category_id, company.order, company.pic_01, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.id = '$company_id' and company_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function saveCompany($company_form) {
		
		global $__CFG;
		
		// data bieżąca 
		if($company_form['date_created']){
			
			$date_now = $company_form['date_created'];
			
		}
		else{
			
			$date_now = date("Y-m-d H:i:s", time());
			
		}
		
		
		
		if (!$company_form['company_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from company where category_id = '$company_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into company (category_id, `order`) values ('$company_form[category_id]', '0') ";
			$rv = $this->DBM->modifyTable($sql);
			$company_id = $this->DBM->lastInsertID;
			
					// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "company_".$company_id."_01.jpg";
				$sql = "update company set pic_01 = '$pic_01' where id = '$company_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeCompanyPicture ($_FILES['pic_01']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_01_01.jpg", 30, 30);
				$this->resizeCompanyPicture ($_FILES['pic_01']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_02_01.jpg", 60, 60);
				$this->resizeCompanyPicture ($_FILES['pic_01']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_03_01.jpg", 200, 200);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			// dodaj obrazek nr 2
			if ($_FILES['pic_02']['name']) {
				$pic_02 = "company_".$company_id."_02.jpg";
				$sql = "update company set pic_02 = '$pic_02' where id = '$company_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeCompanyPicture ($_FILES['pic_02']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_01_02.jpg", 30, 30);
				$this->resizeCompanyPicture ($_FILES['pic_02']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_02_02.jpg", 60, 60);
				$this->resizeCompanyPicture ($_FILES['pic_02']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_03_02.jpg", 200, 200);
				unlink($_FILES['pic_02']['tmp_name']);				
				

			}
			
					
			// dodaj obrazek nr 3
			if ($_FILES['pic_03']['name']) {
				$pic_03 = "company_".$company_id."_03.jpg";
				$sql = "update company set pic_03 = '$pic_03' where id = '$company_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeCompanyPicture ($_FILES['pic_03']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_01_03.jpg", 30, 30);
				$this->resizeCompanyPicture ($_FILES['pic_03']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_02_03.jpg", 60, 60);
				$this->resizeCompanyPicture ($_FILES['pic_03']['tmp_name'], $__CFG['company_pictures_path'].$company_id."_03_03.jpg", 200, 200);
				unlink($_FILES['pic_03']['tmp_name']);				
				

			}			
		
			
			if ($company_id) {
				
				// zapisujemy wersję językową
				$sql  = "insert into company_multilang (company_id, type, url_name, podpis, language_id, title, abstract, content, status, date_created, date_modified) ";
				$sql .= " values ('$company_id', '$company_form[type]', '$company_form[url_name]', '$company_form[podpis]', '$company_form[language_id]', '$company_form[title]', '$company_form[abstract]', '$company_form[content]', '$company_form[status]', '$date_now', '$date_now') ";
				$rv = $this->DBM->modifyTable($sql);
				
				// i wszystkie pozostałe wersje językowe (puste)
				$sql = "select * from language";
				$languages_list = $this->DBM->getTable($sql);
				
				if (sizeof($languages_list)) {
					foreach ($languages_list as $language) {
						// bez tego, który już wczesniej zapisaliśmy!
						if ($language['id'] != $company_form['language_id']) {
							$sql  = "insert into company_multilang (company_id, language_id, status, date_created, date_modified, title) ";
							$sql .= " values ('$company_id', '$language[id]', '0', '$date_now', '$date_now', '$company_form[title]') ";
							$rv = $this->DBM->modifyTable($sql);
						}
					}
				}
			}
		}
		else {
			
			// aktualizacja artykułu
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update company set category_id = '$company_form[category_id]' where id = '$company_form[company_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			
					// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($company_form['remove_picture_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update company set pic_01 = '$pic_01' where id = '$company_form[company_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_01_01.jpg");
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_02_01.jpg");
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_03_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "company_".$company_form['company_id']."_01.jpg";
				$sql = "update company set pic_01 = '$pic_01' where id = '$company_form[company_id]'";
				$rv = $this->DBM->modifyTable($sql);
				


				
				// pierwsza miniatura
				$this->resizeCompanyPicture ($_FILES['pic_01']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_01_01.jpg", 30, 30);
				$this->resizeCompanyPicture ($_FILES['pic_01']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_02_01.jpg", 60, 60);
				$this->resizeCompanyPicture ($_FILES['pic_01']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_03_01.jpg", 200, 200);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}
			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 2
			if ($company_form['remove_picture_02']) {
				// usuwamy obrazek
				$pic_02 = "";
				$sql = "update company set pic_02 = '$pic_02' where id = '$company_form[company_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_01_02.jpg");
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_02_02.jpg");
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_03_02.jpg");
			}
			elseif ($_FILES['pic_02']['name']) {
				// aktualizujemy obrazek
				$pic_02 = "company_".$company_form['company_id']."_02.jpg";
				$sql = "update company set pic_02 = '$pic_02' where id = '$company_form[company_id]'";
				$rv = $this->DBM->modifyTable($sql);
				

				
				// pierwsza miniatura
				$this->resizeCompanyPicture ($_FILES['pic_02']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_01_02.jpg", 30, 30);
				$this->resizeCompanyPicture ($_FILES['pic_02']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_02_02.jpg", 60, 60);
				$this->resizeCompanyPicture ($_FILES['pic_02']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_03_02.jpg", 200, 200);
				unlink($_FILES['pic_02']['tmp_name']);					
				
			}			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 3
			if ($company_form['remove_picture_03']) {
				// usuwamy obrazek
				$pic_03 = "";
				$sql = "update company set pic_03 = '$pic_03' where id = '$company_form[company_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_01_03.jpg");
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_02_03.jpg");
				unlink($__CFG['company_pictures_path'].$company_form['company_id']."_03_03.jpg");
			}
			elseif ($_FILES['pic_03']['name']) {
				// aktualizujemy obrazek
				$pic_03 = "company_".$company_form['company_id']."_03.jpg";
				$sql = "update company set pic_03 = '$pic_03' where id = '$company_form[company_id]'";
				$rv = $this->DBM->modifyTable($sql);

				
				// pierwsza miniatura
				$this->resizeCompanyPicture ($_FILES['pic_03']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_01_03.jpg", 30, 30);
				$this->resizeCompanyPicture ($_FILES['pic_03']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_02_03.jpg", 60, 60);
				$this->resizeCompanyPicture ($_FILES['pic_03']['tmp_name'], $__CFG['company_pictures_path'].$company_form[company_id]."_03_03.jpg", 200, 200);
				unlink($_FILES['pic_03']['tmp_name']);					
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// a potem wersja językowa
			$sql = "update company_multilang set type = '$company_form[type]', url_name = '$company_form[url_name]', podpis = '$company_form[podpis]',  title = '$company_form[title]', abstract = '$company_form[abstract]', content = '$company_form[content]', status = '$company_form[status]', date_modified = '$date_now' where company_id = '$company_form[company_id]' and language_id = '$company_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($company_id) { 
			return $company_id;
		}
		else {
			return $company_form['company_id'];
		}
	}
	
	
	/**
	 * zapisuje pojedynczy artykuł (notes z formularza)
	 */
	
	function saveCompanyNotes($company_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
			
		
		$company_form['category_id'] = 10;
		$company_form['language_id'] = 1;
		$company_form['status'] = 0;
		
		if (!$company_form['company_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from company where category_id = '$company_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into company (category_id, `order`) values ('$company_form[category_id]', '0') ";
			$rv = $this->DBM->modifyTable($sql);
			$company_id = $this->DBM->lastInsertID;
	
		
			if ($company_id) {
				
				// zapisujemy wersję językową
				$sql  = "insert into company_multilang (company_id, type, url_name, podpis, language_id, title, abstract, content, status, date_created, date_modified) ";
				$sql .= " values ('$company_id', '$company_form[type]', '$company_form[url_name]', '$company_form[name]', '$company_form[language_id]', '$company_form[title]', '$company_form[abstract]', '$company_form[content]', '$company_form[status]', '$date_now', '$date_now') ";
				$rv = $this->DBM->modifyTable($sql);
				
				// i wszystkie pozostałe wersje językowe (puste)
				$sql = "select * from language";
				$languages_list = $this->DBM->getTable($sql);
				
				if (sizeof($languages_list)) {
					foreach ($languages_list as $language) {
						// bez tego, który już wczesniej zapisaliśmy!
						if ($language['id'] != $company_form['language_id']) {
							$sql  = "insert into company_multilang (company_id, language_id, status, date_created, date_modified, title) ";
							$sql .= " values ('$company_id', '$language[id]', '0', '$date_now', '$date_now', '$company_form[title]') ";
							$rv = $this->DBM->modifyTable($sql);
						}
					}
				}
			}
		}

		// zwracamy ID ostatnio zapisanego artykułu

		return $company_id;

	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removeCompany($company_id) {
			
		if ($company_id) {
			// metryka
			$sql = "delete from company where id = $company_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from company_multilang where company_id = $company_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * uaktualnia automatycznie kolejnosc artykułow w kategorii
	 */
	
	function updateOrder($company_id, $position) {
			
		if ($company_id && $position) {
			
			$sql = "update company set `order` = '$position' where id = '$company_id' ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;

	}
	
	/**
	 * gets n random companys from given category
	 */
	
	function getRandomCompanys ($category_id, $limit, $lang_id) {
		
		if ($category_id && $limit && $lang_id) {
			$sql = "select company.category_id, company.order, company_multilang.* from company, company_multilang where company_multilang.company_id = company.id and company.category_id = '$category_id' and company_multilang.language_id = '$lang_id' order by rand() limit $limit";
			$company_list = $this->DBM->getTable($sql);
			
			return $company_list;
		}
		
	}
	
	/**
	 * gets all companys
	 */
	
	function getCompanysAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from company_multilang where language_id = '$lang_id' order by date_modified desc ";
			$company_list = $this->DBM->getTable($sql);
			
			return $company_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 2;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizeCompanyPicture ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['gallery_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 80);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 80);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>