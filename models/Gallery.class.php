<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Gallery {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Gallery() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getGallerys($order = "order", $direction = "desc", $category_id, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		
		$sql  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' ";		
		$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$gallery_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($gallery_list_temp)) {
			$gallery_list = array();
			foreach ($gallery_list_temp as $gallery_details) {
				$gallery_list[$gallery_details[gallery_id]] = $gallery_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $gallery_list;
	}
	
	/**
	 * wyciaga listę artykułów dla rss
	 */
	
	function getGalleryByRss ($order = "order", $direction = "asc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = '2' ";
			
			$sql .= " order by gallery_multilang.date_created desc limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * Nadaje polskie nazwy miesiecy, dni etc
	 */
	
	function dateV($format,$timestamp=null){
		$to_convert = array(
			'l'=>array('dat'=>'N','str'=>array('Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota','Niedziela')),
			'F'=>array('dat'=>'n','str'=>array('styczeń','luty','marzec','kwiecień','maj','czerwiec','lipiec','sierpień','wrzesień','październik','listopad','grudzień')),
			'f'=>array('dat'=>'n','str'=>array('sty','lut','mar','kwi','maj','cze','lip','sie','wrz','paź','lis','gru'))
		);
		if ($pieces = preg_split('#[:/.\-, ]#', $format)){	
			if ($timestamp === null) { $timestamp = time(); }
			foreach ($pieces as $datepart){
				if (array_key_exists($datepart,$to_convert)){
					$replace[] = $to_convert[$datepart]['str'][(date($to_convert[$datepart]['dat'],$timestamp)-1)];
				}else{
					$replace[] = date($datepart,$timestamp);
				}
			}
			$result = strtr($format,array_combine($pieces,$replace));
			return $result;
		}
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getGallerysByWtz($order = "order", $direction = "desc", $page_number = 1, $lang_id, $wtz) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.author_id = '$wtz' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.author_id = '$wtz'  ";		
		$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$gallery_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($gallery_list_temp)) {
			$gallery_list = array();
			foreach ($gallery_list_temp as $gallery_details) {
				$gallery_list[$gallery_details[gallery_id]] = $gallery_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $gallery_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($gallery_id, $status) {
		
		if ($gallery_id) {
			
			$sql = "update gallery_multilang set status = '$status' where gallery_multilang.gallery_id = '$gallery_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
	
	/**
	 * Przestawia status home
	 */
	
	function setHome ($gallery_id, $status) {
		
		if ($gallery_id) {
			
			$sql = "update gallery_multilang set home = '$status' where gallery_multilang.gallery_id = '$gallery_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getGallerysSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND gallery.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND gallery.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(gallery_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(gallery_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(gallery_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(gallery_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(gallery_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(gallery_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND gallery_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND gallery_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND gallery_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND gallery_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND gallery_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND gallery_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by gallery.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$gallery_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($gallery_list_temp)) {
			$gallery_list = array();
			foreach ($gallery_list_temp as $gallery_details) {
				$gallery_list[$gallery_details[gallery_id]] = $gallery_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number);
		}
		
		return $gallery_list;
	}
	
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getGallerysSearchForView($limit, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
			
		

		
		
		global $__CFG;
		
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND gallery.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND gallery.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(gallery_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(gallery_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(gallery_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(gallery_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(gallery_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(gallery_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND gallery_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND gallery_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND gallery_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND gallery_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND gallery_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND gallery_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
			if ($search_form['phrase']) {
				$sql .= " AND (lower(gallery_multilang.title) like lower('%".$search_form['phrase']."%') or lower(gallery_multilang.content) like lower('%".$search_form['phrase']."%') or lower(gallery_multilang.abstract) like lower('%".$search_form['phrase']."%')) ";
				$sql_count .= " AND (lower(gallery_multilang.title) like lower('%".$search_form['phrase']."%') or lower(gallery_multilang.content) like lower('%".$search_form['phrase']."%') or lower(gallery_multilang.abstract) like lower('%".$search_form['phrase']."%')) ";
			}
		}
		
		$sql .= " order by gallery.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$gallery_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($gallery_list_temp)) {
			$gallery_list = array();
			foreach ($gallery_list_temp as $gallery_details) {
				$gallery_list[$gallery_details[gallery_id]] = $gallery_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $gallery_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getGallerysForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$gallery_list = $this->getGallerysSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_gallery where product_id = '$product_id'";
			$gallerys_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($gallerys_temp)) {
				foreach ($gallerys_temp as $details) {
					if ($gallery_list[$details['gallery_id']]) {
						$gallery_list[$details['gallery_id']]['selected'] = 1;
					}
				}
			}
			
			return $gallery_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getGallerysOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang, product_gallery where gallery_multilang.gallery_id = gallery.id and product_gallery.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and product_gallery.product_id = '$product_id' ";
			$gallery_list = $this->DBM->getTable($sql);
			
			return $gallery_list;
		}
	}
	
	
	/**
	 * get gallerys by category id
	 */
	
	function getGallerysByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$gallery_list = $this->getGallerysSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $gallery_list;
		}
	}
	
	/**
	 * wyciaga listę gallerya dla widoku serwisu wraz z pagingiem
	 */
	
	function getGallerysByView ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = '2' ";
			
			$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
				//print_r($gallery_list_temp);
				if($news_list_temp){
				//print_r($gallery_list_temp);
				$news_list2 = $this->calculateCommentsVolumeForGallery($news_list_temp);
				
			}
			
			// reindex array
			if (sizeof($news_list2)) {
				$gallery_list = array();
				foreach ($news_list2 as $gallery_details) {
					
					//$date_created = strftime("%Y-%m-%d", strtotime($gallery_details['date_created']));
					
					$mies_pl = $this->dateV('f',strtotime($gallery_details['date_created']));
					$gallery_details[miesiac] = $mies_pl;
					
					//Zdjęcia przypisane do informacji
					require_once('Picture.class.php');
					$picture = new Picture();				
					$picture_list = $picture->getPicturesByCategoryToView(100, 1, $_SESSION['lang'], $gallery_details['gallery_id']);
					
					$gallery_details[pictures] = $picture_list;
					
					$gallery_list[$gallery_details[gallery_id]] = $gallery_details;
				}
			}			
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $gallery_list;
	}
	
	/**
	 * wyciaga listę gallerya dla widoku serwisu wraz z pagingiem
	 */
	
	function getGallerysByViewAndCategory ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id, $category_id){
		
		if($limit){
			

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '$category_id' and gallery_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '$category_id' and gallery_multilang.status = '2' ";
			
			$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę gallerya dla widoku serwisu wraz z pagingiem
	 */
	
	function getGallerysByViewAndCategoryToAdmin ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id, $category_id){
		
		if($limit){
			

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '$category_id' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '$category_id' ";
			
			$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
				//print_r($gallery_list_temp);
				if($news_list_temp){
				//print_r($gallery_list_temp);
				$news_list = $this->calculateCommentsVolumeForGallery($news_list_temp);
				
			}
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę gallerya dla widoku serwisu wraz z pagingiem
	 */
	
	function getGallerysByViewToHome ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = '2' and gallery_multilang.home = '2' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = '2' and gallery_multilang.home = '2' ";
			
			$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
			
			// reindex array
			if (sizeof($news_list_temp)) {
				$gallery_list = array();
				foreach ($news_list_temp as $gallery_details) {
					
					//$date_created = strftime("%Y-%m-%d", strtotime($gallery_details['date_created']));
					
					$mies_pl = $this->dateV('f',strtotime($gallery_details['date_created']));
					$gallery_details[miesiac] = $mies_pl;
					
					//Zdjęcia przypisane do informacji
					require_once('Picture.class.php');
					$picture = new Picture();				
					$picture_list = $picture->getPicturesByCategoryToView(100, 1, $_SESSION['lang'], $gallery_details['gallery_id']);
					
					$gallery_details[pictures] = $picture_list;
					
					$gallery_list[$gallery_details[gallery_id]] = $gallery_details;
				}
			}			
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $gallery_list;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getGalleryByUrlNameToView($url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select gallery.category_id, gallery.order, gallery.pic_01, gallery.pic_02, gallery.pic_03, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.url_name = '$url_name' and gallery_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
			
			$mies_pl = $this->dateV('f',strtotime($rv['date_created']));
			$rv[miesiac] = $mies_pl;			
			
		}
		return $rv;
	}
	
	/**
	 * Liczba komentarzy dla danego wpisu
	 */
	
	function calculateCommentsVolumeForGallery($gallery_list) {
		
		if (sizeof($gallery_list)) {
			
			// dla każdego użytkownika sprawdzamy ilośc jego znajomych
			foreach ($gallery_list as $key => $gallery_details) {
				
				//print_r($gallery_details);
				
				$sql = "select count(*) as ilosc from comment_gallery where gallery_id = '$gallery_details[gallery_id]' ";
				$details = $this->DBM->getRow($sql);

				$gallery_list[$key]['comments_volume'] = $details['ilosc'];
			}
		}
		
		return $gallery_list;	
	}
	
	/**
	 * wyciaga listę gallerya dla widoku serwisu wraz z pagingiem
	 */
	
	function getGallerysByHome ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '1' and gallery_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '1' and gallery_multilang.status = '2' ";
			
			$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę gallerya dla rss
	 */
	
	function getGallerysByRss ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '1' and gallery_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery_multilang.language_id = '$lang_id' and gallery.category_id = '1' and gallery_multilang.status = '2' ";
			
			$sql .= " order by gallery.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}

	/**
	 * uaktualnia automatycznie kolejnosc artykułow w kategorii
	 */
	
	function updateOrders($gallery_id, $position) {
			
		if ($gallery_id && $position) {
			
			$sql = "update gallery set `order` = '$position' where id = '$gallery_id' ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;

	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getGallery($gallery_id, $lang_id) {
			
		if ($gallery_id && $lang_id) {	
			$sql = "select gallery.category_id, gallery.order, gallery.pic_01, gallery.pic_02, gallery.pic_03, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.id = '$gallery_id' and gallery_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstGalleryInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select gallery.id from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status != 0 order by gallery.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getGalleryDetails($gallery_id, $lang_id) {
		
		if ($gallery_id && $lang_id) {
			$sql = "select gallery.category_id, gallery.order, gallery.pic_01, gallery.pic_02, gallery.pic_03, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.id = '$gallery_id' and gallery_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany gallery istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego wpisu/
	 */
	
	function getGalleryByUrlNameAndId($url_name, $gallery_id) {
			
		if ($url_name && $gallery_id) {	
			//print_r($product_id);
			$sql = "select gallery_multilang.* from gallery_multilang where gallery_multilang.url_name = '$url_name' and gallery_multilang.gallery_id != '$gallery_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * Link z serwisu YouTube
	 */
	
	function YouTube($youtubeurl){
		

		$rpme = array("http://","www.",".com","youtube");
		$VID = str_ireplace($rpme, "", $youtubeurl);
		$VID = substr($VID, strpos($VID,"v=") + 2, strlen($VID) - strpos($VID,"v=") + 2);
	
		If(strstr($VID,"&")){
			$VID = substr($VID, 0, strpos($VID,"&") - 0);
		}
		
		$html = str_ireplace("\n","",fopen("http://www.youtube.com/watch?v=" . $VID, "r"));
		
		return $VID;		
		
		
	}
	
	/**
	 * sprawdza czy dany wpis gallerya istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo wpisu/
	 */
	
	function getGalleryByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select gallery_multilang.* from gallery_multilang where gallery_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function saveGallery($gallery_form) {
		
		global $__CFG;
		
		// data bieżąca 
		if ($gallery_form['date_created']){
			
			$date_now = $gallery_form['date_created'];
		}
		//data uwtorzernia nie zostala dodana
		else{
			
			$date_now = date("Y-m-d H:i:s", time());
		}
		
		$yt_ident = $this->YouTube($gallery_form['youtube']);
		
		if (!$gallery_form['gallery_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from gallery where category_id = '$gallery_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into gallery (category_id, `order`) values ('$gallery_form[category_id]', '$next_order') ";
			$rv = $this->DBM->modifyTable($sql);
			$gallery_id = $this->DBM->lastInsertID;
			
			// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "gallery_".$gallery_id."_01.jpg";
				$sql = "update gallery set pic_01 = '$pic_01' where id = '$gallery_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeGalleryPicture ($_FILES['pic_01']['tmp_name'], $__CFG['gallery_pictures_path'].$gallery_id."_01_01.jpg", 50, 50);
				$this->resizeGalleryPicture ($_FILES['pic_01']['tmp_name'], $__CFG['gallery_pictures_path'].$gallery_id."_02_01.jpg", 500, 500);
				$this->resizeGalleryPicture ($_FILES['pic_01']['tmp_name'], $__CFG['gallery_pictures_path'].$gallery_id."_03_01.jpg", 900, 900);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			if ($gallery_id) {
				
				$sql  = "insert into gallery_multilang (gallery_id, url_name, language_id, title, abstract, content, status, date_created, date_modified, author_id, date_gallery, youtube, yt_ident) ";
				$sql .= " values ('$gallery_id', '$gallery_form[url_name]', '$gallery_form[language_id]', '$gallery_form[title]', '$gallery_form[abstract]', '$gallery_form[content]', '$gallery_form[status]', '$date_now', '$date_now', '$gallery_form[author_id]', '$gallery_form[date_gallery]', '$gallery_form[youtube]', '$yt_ident') ";

				
				$rv = $this->DBM->modifyTable($sql);
				
			}
		}
		else {
			
			// aktualizacja artykułu
			
			
			//Ustalamy kolejnosc
			$gallery_details = $this->getGallery($gallery_form[gallery_id], $gallery_form[language_id]);
			if ($gallery_details['category_id'] != $gallery_form[category_id]){
				
				// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
				$sql = "select max(`order`) as last_order from gallery where category_id = '$gallery_form[category_id]'";
				$order = $this->DBM->getRow($sql);
				$next_order = $order['last_order'] + 1;				
				
			}
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update gallery set category_id = '$gallery_form[category_id]', `order` = '$next_order' where id = '$gallery_form[gallery_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($gallery_form['remove_picture_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update gallery set pic_01 = '$pic_01' where id = '$gallery_form[gallery_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['gallery_pictures_path'].$gallery_form['gallery_id']."_01_01.jpg");
				unlink($__CFG['gallery_pictures_path'].$gallery_form['gallery_id']."_02_01.jpg");
				unlink($__CFG['gallery_pictures_path'].$gallery_form['gallery_id']."_03_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "gallery_".$gallery_form['gallery_id']."_01.jpg";
				$sql = "update gallery set pic_01 = '$pic_01' where id = '$gallery_form[gallery_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				
				// pierwsza miniatura
				$this->resizeGalleryPicture ($_FILES['pic_01']['tmp_name'], $__CFG['gallery_pictures_path'].$gallery_form[gallery_id]."_01_01.jpg", 50, 50);
				$this->resizeGalleryPicture ($_FILES['pic_01']['tmp_name'], $__CFG['gallery_pictures_path'].$gallery_form[gallery_id]."_02_01.jpg", 500, 500);
				$this->resizeGalleryPicture ($_FILES['pic_01']['tmp_name'], $__CFG['gallery_pictures_path'].$gallery_form[gallery_id]."_03_01.jpg", 900, 900);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}

			// a potem wersja językowa
			$sql = "update gallery_multilang set title = '$gallery_form[title]', url_name = '$gallery_form[url_name]', abstract = '$gallery_form[abstract]', content = '$gallery_form[content]', status = '$gallery_form[status]', date_created = '$date_now', date_modified = '$date_now', author_id = '$gallery_form[author_id]', date_gallery = '$gallery_form[date_gallery]', youtube = '$gallery_form[youtube]', yt_ident = '$yt_ident' where gallery_id = '$gallery_form[gallery_id]' and language_id = '$gallery_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($gallery_id) { 
			return $gallery_id;
		}
		else {
			return $gallery_form['gallery_id'];
		}
	}
	
	/**
	 * zapisuje powiązanie artykułów z produktem
	 */
	
	function saveGallerysForProduct ($product_id, $gallerys) {
		
		if ($product_id && sizeof($gallerys)) {
			
			foreach ($gallerys as $gallery_id => $value) {
				
				// najpierw sprawdzamy, czy już przypadkiem takiego powiązania nie ma
				$sql = "select * from product_gallery where product_id = '$product_id' and gallery_id = '$gallery_id'";
				$details = $this->DBM->getRow($sql);
				
				// dalej działamy tylko wtedy kiedy nie ma jeszcze takiego powiązania!
				if (!sizeof($details)) {
					$sql = "insert into product_gallery (product_id, gallery_id) values ('$product_id', '$gallery_id')";
					$this->DBM->modifyTable($sql);
				}
			}
		}
	}
	
	/**
	 * zapisuje komentarz do gallery
	 */
	
	function saveCommentGallery($comment_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
		
		if ($comment_form['content']) {
			
			$content = strip_tags($comment_form['content']);
			$name= strip_tags($comment_form['name']);
			
			// nowa metryka
			$sql = "insert into comment_gallery ( user_id, gallery_id, date_created, content, remote_ip ) values ('$name', '$comment_form[gallery_id]', '$date_now', '$content', '$_SERVER[REMOTE_ADDR]' ) ";
			$rv = $this->DBM->modifyTable($sql);
		}
		
	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removeGallery($gallery_id) {
			
		if ($gallery_id) {
			// metryka
			$sql = "delete from gallery where id = $gallery_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from gallery_multilang where gallery_id = $gallery_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * usuwa powiązania podanych artykułów z podanym produktem
	 */
	
	function removeGallerysFromProduct ($product_id, $gallerys) {
		
		if ($product_id && sizeof($gallerys)) {
			
			foreach ($gallerys as $gallery_id => $value) {
				
				$sql = "delete from product_gallery where product_id = '$product_id' and gallery_id = '$gallery_id'";
				$this->DBM->modifyTable($sql);
			}
		}
	}
	
	/**
	 * gets n random gallerys from given category
	 */
	
	function getRandomGallerys ($category_id, $limit, $lang_id) {
		
		if ($category_id && $limit && $lang_id) {
			$sql = "select gallery.category_id, gallery.order, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' order by rand() limit $limit";
			$gallery_list = $this->DBM->getTable($sql);
			
			return $gallery_list;
		}
		
	}
	
	/**
	 * wyciąga listę artykułów z zazanczonymi artykułami dla danego produktu
	 */
	
	function getGallerysForProductAdmin ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$gallerys = $this->getGallerys("order", "desc", 1, 2, $lang_id);
			
			return $gallerys;
		}
		
	}
	
	/**
	 * gets all gallerys
	 */
	
	function getGallerysAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from gallery_multilang where language_id = '$lang_id' order by date_modified desc ";
			$gallery_list = $this->DBM->getTable($sql);
			
			return $gallery_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
			global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 3;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters2($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging_gallery = array();
		$last_page = ceil($record_count / $limit);
		
		// echo "last page : ".$last_page."<br>";
		$paging_gallery['count'] = $last_page;
		// poprzednia strona
		if ($page_number == 1) {
			$paging_gallery['previous'] = "";
			$paging_gallery['first'] = "";
		}
		else {
			$paging_gallery['previous'] = $page_number - 1;
			$paging_gallery['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging_gallery['next'] = "";
			$paging_gallery['last'] = "";	
		}
		else {
			$paging_gallery['next'] = $page_number + 1;
			$paging_gallery['last'] = $last_page;
		}
		
		$paging_gallery['current'] = $page_number;
		
		
		$this->paging_gallery = $paging_gallery;
		return $paging_gallery;
		
	}
	
	/**
	 * wyciąga najnowsze newsy
	 */
	
	function getRecentNews ($category_id, $limit, $lang_id) {
		
		if ($lang_id) {
		
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status = 2 ";		
			$sql .= " order by gallery.`order` desc limit $limit ";
			
			// echo $sql;
			
			$gallery_list = $this->DBM->getTable($sql);
			
			return $gallery_list;
		}
	}
	
	/**
	 * pobiera listę komentarzy do gallery	
	 */
	
	function getCommentsByGallerys($gallery_id, $limit_count, $page_number = 1, $order = "order", $direction = "desc") {
		
		global $__CFG;

		$limit = $limit_count;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from comment_gallery, gallery where gallery.id = comment_gallery.gallery_id and gallery.id = '$gallery_id' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select comment_gallery.* from comment_gallery, gallery where gallery.id = comment_gallery.gallery_id and gallery.id = '$gallery_id' ";		
		$sql .= " order by comment_gallery.date_created desc limit $offset, $limit ";
		
		//echo $sql."<hr>";
		
		$comment_list = $this->DBM->getTable($sql);
		

		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters2($record_count, $page_number, $limit);
		}
		
		return $comment_list;
	}
	
	/**
	 * pobiera listę komentarzy do gallery	/administracja/
	 */
	
	function getCommentsByGallerysForAdmin($gallery_id, $limit_count, $page_number = 1, $order = "order", $direction = "desc") {
		
		global $__CFG;

		$limit = $limit_count;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from comment_gallery, gallery where gallery.id = comment_gallery.gallery_id and gallery.id = '$gallery_id' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select comment_gallery.* from comment_gallery, gallery where  gallery.id = comment_gallery.gallery_id and gallery.id = '$gallery_id' ";		
		$sql .= " order by comment_gallery.date_created desc limit $offset, $limit ";
		
		//echo $sql."<hr>";
		
		$comment_list = $this->DBM->getTable($sql);
		

		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $comment_list;
	}
	
	/**
	 * usuwa powiązania podanych artykułów z podanym produktem
	 */
	
	function removeCommentForGallery ($comment_id) {
		
		if ($comment_id) {
			

				
			$sql = "delete from comment_gallery where id = '$comment_id' ";
			$this->DBM->modifyTable($sql);

		}
	}
	
	/**
	 * wyciąga editoriale
	 */
	
	function getEditorials ($category_id, $lang_id) {
		
		if ($lang_id) {
		
			$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' ";		
			$sql .= " order by gallery.`order` desc";
			
			// echo $sql;
			
			$gallery_list = $this->DBM->getTable($sql);
			
			return $gallery_list;
		}
	}
	
	/**
	 * wyciąga pojedynczy editorial wraz z nawigacją
	 */
	
	function getEditorial ($gallery_id, $lang_id, $user_id = null) {
		
		if ($lang_id) {
			
			// kategoria editoriali
			if ($user_id) {
				$category_id = 4;
			}
			else {
				$category_id = 9;
			}
			
			// jeżeli został podany konkretny editorial
			if ($gallery_id) {
				
				$sql = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.id = '$gallery_id' and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status != 0";
				$gallery_details = $this->DBM->getRow($sql);
				
				if (sizeof($gallery_details)) {
					
					// wybieramy nastepny (nowszy)
					
					$sql = "select gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.`order` > '$gallery_details[order]' and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status != 0 order by gallery.`order` asc limit 1";
					$next_gallery = $this->DBM->getRow($sql);
					
					if (sizeof($next_gallery)) {
						$gallery_details['next_gallery'] = $next_gallery['gallery_id'];
					}
					
					// wybieramy poprzedni (starszy) 
					
					$sql = "select gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.`order` < '$gallery_details[order]' and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status != 0 order by gallery.`order` desc limit 1";
					$previous_gallery = $this->DBM->getRow($sql);
					
					if (sizeof($previous_gallery)) {
						$gallery_details['previous_gallery'] = $previous_gallery['gallery_id'];
					}
					
					return $gallery_details;
				}	
			}
			else {
				// nie podano konkretnego - bierzemy dwa najnowsze - od razu do nawigacji
				
				$sql  = "select gallery.category_id, gallery.order, gallery.pic_01, gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.category_id = '$category_id' and gallery_multilang.language_id = '$lang_id' and gallery_multilang.status != 0 ";
				$sql .= " order by gallery.`order` desc limit 2";
				$details = $this->DBM->getTable($sql);
				
				// echo $sql;
				
				// szczegóły bieżącego
				$gallery_details = $details[0];
				
				// nowszego (następnego) nie ma !
				// ...
				
				// a to jest starszy
				if (sizeof($details[1])) {
					// poprzedni editorial ( = starszy!)
					$gallery_details['previous_gallery'] = $details[1]['gallery_id'];
				}
				
				return $gallery_details;
			}
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł jako treść helpa
	 */
	
	function getHelp($gallery_id, $lang_id) {
			
		if ($gallery_id && $lang_id) {	
			$sql = "select gallery_multilang.* from gallery, gallery_multilang where gallery_multilang.gallery_id = gallery.id and gallery.id = '$gallery_id' and gallery_multilang.language_id = '$lang_id'";
			$gallery_details = $this->DBM->getRow($sql);
			
			return $gallery_details['content'];	
		}
	}
	
	/**
	 * wysyła artykuł mailem (HTML)
	 */
	
	function sendGalleryByEmail ($gallery_details, $email) {
		
		global $_mail_params;
		global $dict_templates;
		global $__CFG;
		global $smarty;
		
		if (sizeof($gallery_details) && $email) {
			
			$content = $smarty->fetch("send_gallery.tpl");
				
			$mail_data['html_body'] = $content;
			
			$mail_data['headers']['MIME-Version']= '1.0';
			$mail_data['headers']['Subject'] = $dict_templates['SendGallerySubject'];
			$mail_data['headers']['From'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['To'] = $email;
			$mail_data['headers']['Reply-To'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['Date'] = date("r",time());
			
			require_once 'HCMailer2.class.php';
			
			$mailerek = new HCMailer2($mail_data);
			$mailerek->sendMailMime();
			
			return true;
		}
	}
	
	/**
	 * generuje z podanego artykułu plik tekstowy
	 */
	
	function createFileFromGallery ($gallery_id, $path, $lang_id) {
		
		global $__CFG;
		
		if ($gallery_id && $path && $lang_id) {
			
			// szczegóły artykułu
			$sql = "select title, content from gallery_multilang where gallery_id = '$gallery_id' and language_id = '$lang_id'";
			$details = $this->DBM->getRow($sql);
			
			if (sizeof($details)) {
				
				$details['content'] = str_replace("</p>", "\r\n", $details['content']);
				$details['content'] = strip_tags($details['content']);
				
				$filename = strtolower(Utils::toascii_replace(str_replace(" ", "_", trim($details['title'])))).".txt";
				
				// echo $path.$filename."<br/>";
				
				$fp = fopen($path.$filename, 'w');
				fwrite($fp, $details['title']."\r\n\r\n");
				fwrite($fp, $details['content']);
				fclose($fp);
				
				// echo " done<br/>";
			} 
		}
	}
	
	function echocode ($code){
		
		if($code){
			
			print_r($code);
			
		}
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizeGalleryPicture ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['gallery_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 100);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 100);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>