<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Hate {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Hate() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getHates($order = "order", $direction = "desc", $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		
		$sql  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' ";		
		$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$hate_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($hate_list_temp)) {
			$hate_list = array();
			foreach ($hate_list_temp as $hate_details) {
				$hate_list[$hate_details[hate_id]] = $hate_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $hate_list;
	}
	
	/**
	 * wyciaga listę artykułów dla rss
	 */
	
	function getHateByRss ($order = "order", $direction = "asc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' ";
			
			$sql .= " order by hate_multilang.date_created desc limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * Nadaje polskie nazwy miesiecy, dni etc
	 */
	
	function dateV($format,$timestamp=null){
		$to_convert = array(
			'l'=>array('dat'=>'N','str'=>array('Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota','Niedziela')),
			'F'=>array('dat'=>'n','str'=>array('styczeń','luty','marzec','kwiecień','maj','czerwiec','lipiec','sierpień','wrzesień','październik','listopad','grudzień')),
			'f'=>array('dat'=>'n','str'=>array('sty','lut','mar','kwi','maj','cze','lip','sie','wrz','paź','lis','gru'))
		);
		if ($pieces = preg_split('#[:/.\-, ]#', $format)){	
			if ($timestamp === null) { $timestamp = time(); }
			foreach ($pieces as $datepart){
				if (array_key_exists($datepart,$to_convert)){
					$replace[] = $to_convert[$datepart]['str'][(date($to_convert[$datepart]['dat'],$timestamp)-1)];
				}else{
					$replace[] = date($datepart,$timestamp);
				}
			}
			$result = strtr($format,array_combine($pieces,$replace));
			return $result;
		}
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getHatesByWtz($order = "order", $direction = "desc", $page_number = 1, $lang_id, $wtz) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.author_id = '$wtz' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.author_id = '$wtz'  ";		
		$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$hate_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($hate_list_temp)) {
			$hate_list = array();
			foreach ($hate_list_temp as $hate_details) {
				$hate_list[$hate_details[hate_id]] = $hate_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $hate_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($hate_id, $status) {
		
		if ($hate_id) {
			
			$sql = "update hate_multilang set status = '$status' where hate_multilang.hate_id = '$hate_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
	
	/**
	 * Przestawia status wysłania
	 */
	
	function setSend ($hate_id, $status) {
		
		if ($hate_id) {
			
			$sql = "update hate_multilang set send = '$status' where hate_multilang.hate_id = '$hate_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
	
	/**
	 * Przestawia status home
	 */
	
	function setHome ($hate_id, $status) {
		
		if ($hate_id) {
			
			$sql = "update hate_multilang set home = '$status' where hate_multilang.hate_id = '$hate_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
	
	/**
	 * send message to admin about activation
	 */
	
	function sendActivationMessage ($hate_details, $admin_details) {
		
		global $_contact_mails;
		global $_mail_params;
		global $dict_templates;
		global $base_url;
		global $__CFG;
		global $smarty;
		
		if ($hate_details && $admin_details) {
			
			$mailing = array();
			
			// treść maila
			$mailing['content']  = "Dzień dobry,<br/>";
			$mailing['content']  .= "na państwa portalu wykryliśmy kontrowersyjne treści, które mogą naruszać czyjeś dobre imię, ogólnie przyjęte normy zachowania, kulturę wypowiedzi lub mogące być niezgodne z prawem.<br/><br/>";
			
			$mailing['title'] = "Nie odpowiednie treści na portalu <a href='".$hate_details['link']."'>".$hate_details['mainlink']."</a>";
			
			$mailing['content']  .= '"... '.$hate_details['fragment'].'..."<br/><br/>';
			$mailing['content']  .= "Link do pełnego artykułu: <a href='".$hate_details['link']."'>".$hate_details['mainlink']."</a><br/><br/>";
			
			//Podpis autora
			$mailing['content']  .= "Z poważaniem,</br>";
			$mailing['content']  .= $admin_details['surname']." ".$admin_details['name']."</br>";
			$mailing['content']  .= "Administrator serwisu <a href='".$__CFG['base_url']."'>www.niehejtuje.com</a></br></br>";
			$mailing['content']  .= "Kontakt można uzyskać pod linkiem: <a href='".$__CFG['base_url']."kontakt'>www.niehejtuje.com/kontakt</a></br></br>";
			
			
			
			
			$smarty->assign("mailing", $mailing);
			
			$content = $smarty->fetch("mailing_template.tpl");
			
			$mail_data['html_body'] = $content;
			
			$mail_data['headers']['MIME-Version']= '1.0';
			$mail_data['headers']['Subject'] = $__CFG['newsletter_mail_name']." // Nie odpowiednie treści na portalu ".$hate_details['mainlink'];
			$mail_data['headers']['From'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['To'] = $hate_details['email'];
			$mail_data['headers']['Reply-To'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['Date'] = date("r",time());
			
			require_once 'HCMailer2.class.php';
			
			$mailerek = new HCMailer2($mail_data);
			$mailerek->sendMailMime();
			
			return true;
			
		}
		else {
			return false;
		}
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getHatesSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND hate.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND hate.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(hate_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(hate_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(hate_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(hate_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(hate_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(hate_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND hate_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND hate_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND hate_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND hate_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND hate_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND hate_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by hate.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$hate_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($hate_list_temp)) {
			$hate_list = array();
			foreach ($hate_list_temp as $hate_details) {
				$hate_list[$hate_details[hate_id]] = $hate_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number);
		}
		
		return $hate_list;
	}
	
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getHatesSearchForView($limit, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$page_number = 1;
			}		
		

		
		
		global $__CFG;
		
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND hate.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND hate.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(hate_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(hate_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(hate_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(hate_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(hate_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(hate_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND hate_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND hate_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND hate_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND hate_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND hate_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND hate_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
			if ($search_form['phrase']) {
				$sql .= " AND (lower(hate_multilang.title) like lower('%".$search_form['phrase']."%') or lower(hate_multilang.content) like lower('%".$search_form['phrase']."%') or lower(hate_multilang.abstract) like lower('%".$search_form['phrase']."%')) ";
				$sql_count .= " AND (lower(hate_multilang.title) like lower('%".$search_form['phrase']."%') or lower(hate_multilang.content) like lower('%".$search_form['phrase']."%') or lower(hate_multilang.abstract) like lower('%".$search_form['phrase']."%')) ";
			}
		}
		
		$sql .= " order by hate.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$hate_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($hate_list_temp)) {
			$hate_list = array();
			foreach ($hate_list_temp as $hate_details) {
				$hate_list[$hate_details[hate_id]] = $hate_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $hate_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getHatesForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$hate_list = $this->getHatesSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_hate where product_id = '$product_id'";
			$hates_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($hates_temp)) {
				foreach ($hates_temp as $details) {
					if ($hate_list[$details['hate_id']]) {
						$hate_list[$details['hate_id']]['selected'] = 1;
					}
				}
			}
			
			return $hate_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getHatesOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang, product_hate where hate_multilang.hate_id = hate.id and product_hate.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and product_hate.product_id = '$product_id' ";
			$hate_list = $this->DBM->getTable($sql);
			
			return $hate_list;
		}
	}
	
	
	/**
	 * get hates by category id
	 */
	
	function getHatesByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$hate_list = $this->getHatesSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $hate_list;
		}
	}
	
	/**
	 * wyciaga listę hatea dla widoku serwisu wraz z pagingiem
	 */
	
	function getHatesByView ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' ";
			
			$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
				//print_r($hate_list_temp);
				if($news_list_temp){
				//print_r($hate_list_temp);
				$news_list2 = $this->calculateCommentsVolumeForHate($news_list_temp);
				
			}
			
			// reindex array
			if (sizeof($news_list2)) {
				$hate_list = array();
				foreach ($news_list2 as $hate_details) {
					
					//$date_created = strftime("%Y-%m-%d", strtotime($hate_details['date_created']));
					
					$mies_pl = $this->dateV('f',strtotime($hate_details['date_created']));
					$hate_details[miesiac] = $mies_pl;
					
					//Zdjęcia przypisane do informacji
					require_once('Picture.class.php');
					$picture = new Picture();				
					$picture_list = $picture->getPicturesByCategoryToView(100, 1, $_SESSION['lang'], $hate_details['hate_id']);
					
					$hate_details[pictures] = $picture_list;
					
					$hate_list[$hate_details[hate_id]] = $hate_details;
				}
			}			
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $hate_list;
	}
	
	/**
	 * wyciaga listę hatea dla widoku serwisu wraz z pagingiem
	 */
	
	function getHatesByViewAndCategory ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id, $category_id){
		
		if($limit){
			

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate.category_id = '$category_id' and hate_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate.category_id = '$category_id' and hate_multilang.status = '2' ";
			
			$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
				//print_r($hate_list_temp);
				if($news_list_temp){
				//print_r($hate_list_temp);
				$news_list = $this->calculateCommentsVolumeForHate($news_list_temp);
				
			}
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę hatea dla widoku serwisu wraz z pagingiem
	 */
	
	function getHatesByViewAndCategoryToAdmin ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id, $category_id){
		
		if($limit){
			

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate.category_id = '$category_id' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate.category_id = '$category_id' ";
			
			$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
				//print_r($hate_list_temp);
				if($news_list_temp){
				//print_r($hate_list_temp);
				$news_list = $this->calculateCommentsVolumeForHate($news_list_temp);
				
			}
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę hatea dla widoku serwisu wraz z pagingiem
	 */
	
	function getHatesByViewToHome ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' and hate_multilang.home = '2' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' and hate_multilang.home = '2' ";
			
			$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list_temp = $this->DBM->getTable($sql);
			
				//print_r($hate_list_temp);
				if($news_list_temp){
				//print_r($hate_list_temp);
				$news_list2 = $this->calculateCommentsVolumeForHate($news_list_temp);
				
			}
			
			// reindex array
			if (sizeof($news_list2)) {
				$hate_list = array();
				foreach ($news_list2 as $hate_details) {
					
					//$date_created = strftime("%Y-%m-%d", strtotime($hate_details['date_created']));
					
					$mies_pl = $this->dateV('f',strtotime($hate_details['date_created']));
					$hate_details[miesiac] = $mies_pl;
					
					//Zdjęcia przypisane do informacji
					require_once('Picture.class.php');
					$picture = new Picture();				
					$picture_list = $picture->getPicturesByCategoryToView(100, 1, $_SESSION['lang'], $hate_details['hate_id']);
					
					$hate_details[pictures] = $picture_list;
					
					$hate_list[$hate_details[hate_id]] = $hate_details;
				}
			}			
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $hate_list;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getHateByUrlNameToView($url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select hate.category_id, hate.order, hate.pic_01, hate.pic_02, hate.pic_03, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.url_name = '$url_name' and hate_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
			
			$mies_pl = $this->dateV('f',strtotime($rv['date_created']));
			$rv[miesiac] = $mies_pl;			
			
		}
		return $rv;
	}
	
	/**
	 * Liczba komentarzy dla danego wpisu
	 */
	
	function calculateCommentsVolumeForHate($hate_list) {
		
		if (sizeof($hate_list)) {
			
			// dla każdego użytkownika sprawdzamy ilośc jego znajomych
			foreach ($hate_list as $key => $hate_details) {
				
				//print_r($hate_details);
				
				$sql = "select count(*) as ilosc from comment_hate where hate_id = '$hate_details[hate_id]' ";
				$details = $this->DBM->getRow($sql);

				$hate_list[$key]['comments_volume'] = $details['ilosc'];
			}
		}
		
		return $hate_list;	
	}
	
	/**
	 * wyciaga listę hatea dla widoku serwisu wraz z pagingiem
	 */
	
	function getHatesByHome ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate_multilang.status = '2' ";
			
			$sql .= " order by rand() limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę hatea dla rss
	 */
	
	function getHatesByRss ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate.category_id = '1' and hate_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate_multilang.language_id = '$lang_id' and hate.category_id = '1' and hate_multilang.status = '2' ";
			
			$sql .= " order by hate.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getHate($hate_id, $lang_id) {
			
		if ($hate_id && $lang_id) {	

			$sql = "select hate.category_id, hate.order, hate.pic_01, hate.pic_02, hate.pic_03, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.id = '$hate_id' and hate_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstHateInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select hate.id from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' and hate_multilang.status != 0 order by hate.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getHateDetails($hate_id, $lang_id) {
		
		if ($hate_id && $lang_id) {
			$sql = "select hate.category_id, hate.order, hate.pic_01, hate.pic_02, hate.pic_03, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.id = '$hate_id' and hate_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany hate istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego wpisu/
	 */
	
	function getHateByUrlNameAndId($url_name, $hate_id) {
			
		if ($url_name && $hate_id) {	
			//print_r($product_id);
			$sql = "select hate_multilang.* from hate_multilang where hate_multilang.url_name = '$url_name' and hate_multilang.hate_id != '$hate_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany wpis hatea istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo wpisu/
	 */
	
	function getHateByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select hate_multilang.* from hate_multilang where hate_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function saveHate($hate_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
		
		//Czyścimy fraze
		$phrase2 = $hate_form['title'];
		$hate_form['title'] = htmlspecialchars($phrase2, ENT_QUOTES);	
		
		if (!$hate_form['hate_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from hate where temp = 0";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into hate (category_id, `order`) values ('$hate_form[category_id]', '$next_order') ";
			$rv = $this->DBM->modifyTable($sql);
			$hate_id = $this->DBM->lastInsertID;
			
			// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "hate_".$hate_id."_01.jpg";
				$sql = "update hate set pic_01 = '$pic_01' where id = '$hate_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeHatePicture ($_FILES['pic_01']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_01_01.jpg", 50, 50);
				$this->resizeHatePicture ($_FILES['pic_01']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_02_01.jpg", 640, 480);
				$this->resizeHatePicture ($_FILES['pic_01']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_03_01.jpg", 800, 800);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			// dodaj obrazek nr 2
			if ($_FILES['pic_02']['name']) {
				$pic_02 = "hate_".$hate_id."_02.jpg";
				$sql = "update hate set pic_02 = '$pic_02' where id = '$hate_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeHatePicture ($_FILES['pic_02']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_01_02.jpg", 50, 50);
				$this->resizeHatePicture ($_FILES['pic_02']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_03_02.jpg", 640, 480);
				$this->resizeHatePicture ($_FILES['pic_02']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_04_02.jpg", 800, 800);
				unlink($_FILES['pic_02']['tmp_name']);				
				

			}
			
					
			// dodaj obrazek nr 3
			if ($_FILES['pic_03']['name']) {
				$pic_03 = "hate_".$hate_id."_03.jpg";
				$sql = "update hate set pic_03 = '$pic_03' where id = '$hate_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeHatePicture ($_FILES['pic_03']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_01_03.jpg", 50, 50);
				$this->resizeHatePicture ($_FILES['pic_03']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_03_03.jpg", 640, 480);
				$this->resizeHatePicture ($_FILES['pic_03']['tmp_name'], $__CFG['hate_pictures_path'].$hate_id."_04_03.jpg", 800, 800);
				unlink($_FILES['pic_03']['tmp_name']);				
				

			}
			
			if ($hate_id) {
				
				$sql  = "insert into hate_multilang (hate_id, url_name, language_id, title, abstract, content, status, date_created, date_modified, author_id, date_hate, email, link, fragment, mainlink) ";
				$sql .= " values ('$hate_id', '$hate_form[url_name]', '$hate_form[language_id]', '$hate_form[title]', '$hate_form[abstract]', '$hate_form[content]', '$hate_form[status]', '$date_now', '$date_now', '$hate_form[author_id]', '$hate_form[date_hate]', '$hate_form[email]', '$hate_form[link]', '$hate_form[fragment]', '$hate_form[mainlink]' ) ";
				$rv = $this->DBM->modifyTable($sql);
				
			}
		}
		else {
			
			// aktualizacja artykułu
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update hate set category_id = '$hate_form[category_id]' where id = '$hate_form[hate_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($hate_form['remove_picture_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update hate set pic_01 = '$pic_01' where id = '$hate_form[hate_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_01_01.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_02_01.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_03_01.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_04_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "hate_".$hate_form['hate_id']."_01.jpg";
				$sql = "update hate set pic_01 = '$pic_01' where id = '$hate_form[hate_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				
				// pierwsza miniatura
				$this->resizeHatePicture ($_FILES['pic_01']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_01_01.jpg", 50, 50);
				$this->resizeHatePicture ($_FILES['pic_01']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_02_01.jpg", 640, 480);
				$this->resizeHatePicture ($_FILES['pic_01']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_03_01.jpg", 800, 800);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}
			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 2
			if ($hate_form['remove_picture_02']) {
				// usuwamy obrazek
				$pic_02 = "";
				$sql = "update hate set pic_02 = '$pic_02' where id = '$hate_form[hate_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_01_02.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_03_02.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_04_02.jpg");
			}
			elseif ($_FILES['pic_02']['name']) {
				// aktualizujemy obrazek
				$pic_02 = "hate_".$hate_form['hate_id']."_02.jpg";
				$sql = "update hate set pic_02 = '$pic_02' where id = '$hate_form[hate_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeHatePicture ($_FILES['pic_02']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_01_02.jpg", 50, 50);
				$this->resizeHatePicture ($_FILES['pic_02']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_03_02.jpg", 640, 480);
				$this->resizeHatePicture ($_FILES['pic_02']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_04_02.jpg", 800, 800);
				unlink($_FILES['pic_02']['tmp_name']);					
				
			}			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 3
			if ($hate_form['remove_picture_03']) {
				// usuwamy obrazek
				$pic_03 = "";
				$sql = "update hate set pic_03 = '$pic_03' where id = '$hate_form[hate_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_01_03.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_03_03.jpg");
				unlink($__CFG['hate_pictures_path'].$hate_form['hate_id']."_04_03.jpg");
			}
			elseif ($_FILES['pic_03']['name']) {
				// aktualizujemy obrazek
				$pic_03 = "hate_".$hate_form['hate_id']."_03.jpg";
				$sql = "update hate set pic_03 = '$pic_03' where id = '$hate_form[hate_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeHatePicture ($_FILES['pic_03']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_01_03.jpg", 50, 50);
				$this->resizeHatePicture ($_FILES['pic_03']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_03_03.jpg", 640, 480);
				$this->resizeHatePicture ($_FILES['pic_03']['tmp_name'], $__CFG['hate_pictures_path'].$hate_form[hate_id]."_04_03.jpg", 800, 800);
				unlink($_FILES['pic_03']['tmp_name']);					
				
			}				
			
			// a potem wersja językowa
			$sql = "update hate_multilang set title = '$hate_form[title]', url_name = '$hate_form[url_name]', abstract = '$hate_form[abstract]', content = '$hate_form[content]', status = '$hate_form[status]', date_modified = '$date_now', author_id = '$hate_form[author_id]', date_hate = '$hate_form[date_hate]', email = '$hate_form[email]', link = '$hate_form[link]', fragment = '$hate_form[fragment]', mainlink = '$hate_form[mainlink]' where hate_id = '$hate_form[hate_id]' and language_id = '$hate_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($hate_id) { 
			return $hate_id;
		}
		else {
			return $hate_form['hate_id'];
		}
	}
	
	/**
	 * zapisuje powiązanie artykułów z produktem
	 */
	
	function saveHatesForProduct ($product_id, $hates) {
		
		if ($product_id && sizeof($hates)) {
			
			foreach ($hates as $hate_id => $value) {
				
				// najpierw sprawdzamy, czy już przypadkiem takiego powiązania nie ma
				$sql = "select * from product_hate where product_id = '$product_id' and hate_id = '$hate_id'";
				$details = $this->DBM->getRow($sql);
				
				// dalej działamy tylko wtedy kiedy nie ma jeszcze takiego powiązania!
				if (!sizeof($details)) {
					$sql = "insert into product_hate (product_id, hate_id) values ('$product_id', '$hate_id')";
					$this->DBM->modifyTable($sql);
				}
			}
		}
	}
	
	/**
	 * zapisuje komentarz do hate
	 */
	
	function saveCommentHate($comment_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
		
		if ($comment_form['content']) {
			
			$content = strip_tags($comment_form['content']);
			$name= strip_tags($comment_form['name']);
			
			// nowa metryka
			$sql = "insert into comment_hate ( user_id, hate_id, date_created, content, remote_ip ) values ('$name', '$comment_form[hate_id]', '$date_now', '$content', '$_SERVER[REMOTE_ADDR]' ) ";
			$rv = $this->DBM->modifyTable($sql);
		}
		
	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removeHate($hate_id) {
			
		if ($hate_id) {
			// metryka
			$sql = "delete from hate where id = $hate_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from hate_multilang where hate_id = $hate_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * usuwa powiązania podanych artykułów z podanym produktem
	 */
	
	function removeHatesFromProduct ($product_id, $hates) {
		
		if ($product_id && sizeof($hates)) {
			
			foreach ($hates as $hate_id => $value) {
				
				$sql = "delete from product_hate where product_id = '$product_id' and hate_id = '$hate_id'";
				$this->DBM->modifyTable($sql);
			}
		}
	}
	
	/**
	 * gets n random hates from given category
	 */
	
	function getRandomHates ($category_id, $limit, $lang_id) {
		
		if ($category_id && $limit && $lang_id) {
			$sql = "select hate.category_id, hate.order, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' order by rand() limit $limit";
			$hate_list = $this->DBM->getTable($sql);
			
			return $hate_list;
		}
		
	}
	
	/**
	 * wyciąga listę artykułów z zazanczonymi artykułami dla danego produktu
	 */
	
	function getHatesForProductAdmin ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$hates = $this->getHates("order", "desc", 1, 2, $lang_id);
			
			return $hates;
		}
		
	}
	
	/**
	 * gets all hates
	 */
	
	function getHatesAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from hate_multilang where language_id = '$lang_id' order by date_modified desc ";
			$hate_list = $this->DBM->getTable($sql);
			
			return $hate_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
			global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 3;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters2($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging_hate = array();
		$last_page = ceil($record_count / $limit);
		
		// echo "last page : ".$last_page."<br>";
		$paging_hate['count'] = $last_page;
		// poprzednia strona
		if ($page_number == 1) {
			$paging_hate['previous'] = "";
			$paging_hate['first'] = "";
		}
		else {
			$paging_hate['previous'] = $page_number - 1;
			$paging_hate['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging_hate['next'] = "";
			$paging_hate['last'] = "";	
		}
		else {
			$paging_hate['next'] = $page_number + 1;
			$paging_hate['last'] = $last_page;
		}
		
		$paging_hate['current'] = $page_number;
		
		
		$this->paging_hate = $paging_hate;
		return $paging_hate;
		
	}
	
	/**
	 * wyciąga najnowsze newsy
	 */
	
	function getRecentNews ($category_id, $limit, $lang_id) {
		
		if ($lang_id) {
		
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' and hate_multilang.status = 2 ";		
			$sql .= " order by hate.`order` desc limit $limit ";
			
			// echo $sql;
			
			$hate_list = $this->DBM->getTable($sql);
			
			return $hate_list;
		}
	}
	
	/**
	 * pobiera listę komentarzy do hate	
	 */
	
	function getCommentsByHates($hate_id, $limit_count, $page_number = 1, $order = "order", $direction = "desc") {
		
		global $__CFG;

		$limit = $limit_count;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from comment_hate, hate where hate.id = comment_hate.hate_id and hate.id = '$hate_id' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select comment_hate.* from comment_hate, hate where hate.id = comment_hate.hate_id and hate.id = '$hate_id' ";		
		$sql .= " order by comment_hate.date_created desc limit $offset, $limit ";
		
		//echo $sql."<hr>";
		
		$comment_list = $this->DBM->getTable($sql);
		

		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters2($record_count, $page_number, $limit);
		}
		
		return $comment_list;
	}
	
	/**
	 * pobiera listę komentarzy do hate	/administracja/
	 */
	
	function getCommentsByHatesForAdmin($hate_id, $limit_count, $page_number = 1, $order = "order", $direction = "desc") {
		
		global $__CFG;

		$limit = $limit_count;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from comment_hate, hate where hate.id = comment_hate.hate_id and hate.id = '$hate_id' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select comment_hate.* from comment_hate, hate where  hate.id = comment_hate.hate_id and hate.id = '$hate_id' ";		
		$sql .= " order by comment_hate.date_created desc limit $offset, $limit ";
		
		//echo $sql."<hr>";
		
		$comment_list = $this->DBM->getTable($sql);
		

		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $comment_list;
	}
	
	/**
	 * usuwa powiązania podanych artykułów z podanym produktem
	 */
	
	function removeCommentForHate ($comment_id) {
		
		if ($comment_id) {
			

				
			$sql = "delete from comment_hate where id = '$comment_id' ";
			$this->DBM->modifyTable($sql);

		}
	}
	
	/**
	 * wyciąga editoriale
	 */
	
	function getEditorials ($category_id, $lang_id) {
		
		if ($lang_id) {
		
			$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' ";		
			$sql .= " order by hate.`order` desc";
			
			// echo $sql;
			
			$hate_list = $this->DBM->getTable($sql);
			
			return $hate_list;
		}
	}
	
	/**
	 * wyciąga pojedynczy editorial wraz z nawigacją
	 */
	
	function getEditorial ($hate_id, $lang_id, $user_id = null) {
		
		if ($lang_id) {
			
			// kategoria editoriali
			if ($user_id) {
				$category_id = 4;
			}
			else {
				$category_id = 9;
			}
			
			// jeżeli został podany konkretny editorial
			if ($hate_id) {
				
				$sql = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.id = '$hate_id' and hate_multilang.language_id = '$lang_id' and hate_multilang.status != 0";
				$hate_details = $this->DBM->getRow($sql);
				
				if (sizeof($hate_details)) {
					
					// wybieramy nastepny (nowszy)
					
					$sql = "select hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.`order` > '$hate_details[order]' and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' and hate_multilang.status != 0 order by hate.`order` asc limit 1";
					$next_hate = $this->DBM->getRow($sql);
					
					if (sizeof($next_hate)) {
						$hate_details['next_hate'] = $next_hate['hate_id'];
					}
					
					// wybieramy poprzedni (starszy) 
					
					$sql = "select hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.`order` < '$hate_details[order]' and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' and hate_multilang.status != 0 order by hate.`order` desc limit 1";
					$previous_hate = $this->DBM->getRow($sql);
					
					if (sizeof($previous_hate)) {
						$hate_details['previous_hate'] = $previous_hate['hate_id'];
					}
					
					return $hate_details;
				}	
			}
			else {
				// nie podano konkretnego - bierzemy dwa najnowsze - od razu do nawigacji
				
				$sql  = "select hate.category_id, hate.order, hate.pic_01, hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.category_id = '$category_id' and hate_multilang.language_id = '$lang_id' and hate_multilang.status != 0 ";
				$sql .= " order by hate.`order` desc limit 2";
				$details = $this->DBM->getTable($sql);
				
				// echo $sql;
				
				// szczegóły bieżącego
				$hate_details = $details[0];
				
				// nowszego (następnego) nie ma !
				// ...
				
				// a to jest starszy
				if (sizeof($details[1])) {
					// poprzedni editorial ( = starszy!)
					$hate_details['previous_hate'] = $details[1]['hate_id'];
				}
				
				return $hate_details;
			}
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł jako treść helpa
	 */
	
	function getHelp($hate_id, $lang_id) {
			
		if ($hate_id && $lang_id) {	
			$sql = "select hate_multilang.* from hate, hate_multilang where hate_multilang.hate_id = hate.id and hate.id = '$hate_id' and hate_multilang.language_id = '$lang_id'";
			$hate_details = $this->DBM->getRow($sql);
			
			return $hate_details['content'];	
		}
	}
	
	/**
	 * wysyła artykuł mailem (HTML)
	 */
	
	function sendHateByEmail ($hate_details, $email) {
		
		global $_mail_params;
		global $dict_templates;
		global $__CFG;
		global $smarty;
		
		if (sizeof($hate_details) && $email) {
			
			$content = $smarty->fetch("send_hate.tpl");
				
			$mail_data['html_body'] = $content;
			
			$mail_data['headers']['MIME-Version']= '1.0';
			$mail_data['headers']['Subject'] = $dict_templates['SendHateSubject'];
			$mail_data['headers']['From'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['To'] = $email;
			$mail_data['headers']['Reply-To'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['Date'] = date("r",time());
			
			require_once 'HCMailer2.class.php';
			
			$mailerek = new HCMailer2($mail_data);
			$mailerek->sendMailMime();
			
			return true;
		}
	}
	
	/**
	 * generuje z podanego artykułu plik tekstowy
	 */
	
	function createFileFromHate ($hate_id, $path, $lang_id) {
		
		global $__CFG;
		
		if ($hate_id && $path && $lang_id) {
			
			// szczegóły artykułu
			$sql = "select title, content from hate_multilang where hate_id = '$hate_id' and language_id = '$lang_id'";
			$details = $this->DBM->getRow($sql);
			
			if (sizeof($details)) {
				
				$details['content'] = str_replace("</p>", "\r\n", $details['content']);
				$details['content'] = strip_tags($details['content']);
				
				$filename = strtolower(Utils::toascii_replace(str_replace(" ", "_", trim($details['title'])))).".txt";
				
				// echo $path.$filename."<br/>";
				
				$fp = fopen($path.$filename, 'w');
				fwrite($fp, $details['title']."\r\n\r\n");
				fwrite($fp, $details['content']);
				fclose($fp);
				
				// echo " done<br/>";
			} 
		}
	}
	
	function echocode ($code){
		
		if($code){
			
			print_r($code);
			
		}
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizeHatePicture ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['gallery_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 100);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 100);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>