<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Photo {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Photo() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getPhotos($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = 100;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.category_id = '$category_id' and photo_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.category_id = '$category_id' and photo_multilang.language_id = '$lang_id' ";		
		$sql .= " order by photo.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$photo_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($photo_list_temp)) {
			$photo_list = array();
			foreach ($photo_list_temp as $photo_details) {
				$photo_list[$photo_details[photo_id]] = $photo_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $photo_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($photo_id, $status) {
		
		if ($photo_id) {
			
			$sql = "update photo_multilang set status = '$status' where photo_multilang.photo_id = '$photo_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getPhotosSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND photo.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND photo.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(photo_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(photo_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(photo_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(photo_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(photo_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(photo_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND photo_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND photo_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND photo_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND photo_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND photo_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND photo_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by photo.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$photo_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($photo_list_temp)) {
			$photo_list = array();
			foreach ($photo_list_temp as $photo_details) {
				$photo_list[$photo_details[photo_id]] = $photo_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $photo_list;
	}
	
	/**
	 * pobiera przefiltrowaną listę artykułów /dla widokow/
	 */
	
	function getPhotosSearchToView($limit, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {

		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND photo.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND photo.category_id = ".$search_form['category_id']." ";
			}
			else{
				$sql .= " AND (photo.category_id = 1 or photo.category_id = 3 or photo.category_id = 10 or photo.category_id = 11) ";
				$sql_count .= " AND (photo.category_id = 1 or photo.category_id = 3 or photo.category_id = 10 or photo.category_id = 11) ";				
				
			}
			if ($search_form['phrase']) {
				$sql .= " AND (lower(photo_multilang.title) like lower('%".$search_form['phrase']."%') or lower(photo_multilang.content) like lower('%".$search_form['phrase']."%')) ";
				$sql_count .= " AND (lower(photo_multilang.title) like lower('%".$search_form['phrase']."%') or lower(photo_multilang.content) like lower('%".$search_form['phrase']."%')) ";
			}

		}
		
		$sql .= " order by photo.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$photo_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($photo_list_temp)) {
			$photo_list = array();
			foreach ($photo_list_temp as $photo_details) {
				$photo_list[$photo_details[photo_id]] = $photo_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $photo_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getPhotosForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$photo_list = $this->getPhotosSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_photo where product_id = '$product_id'";
			$photos_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($photos_temp)) {
				foreach ($photos_temp as $details) {
					if ($photo_list[$details['photo_id']]) {
						$photo_list[$details['photo_id']]['selected'] = 1;
					}
				}
			}
			
			return $photo_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getPhotosOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang, product_photo where photo_multilang.photo_id = photo.id and product_photo.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and product_photo.product_id = '$product_id' ";
			$photo_list = $this->DBM->getTable($sql);
			
			return $photo_list;
		}
	}
	
	
	/**
	 * get photos by category id
	 */
	
	function getPhotosByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$photo_list = $this->getPhotosSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $photo_list;
		}
	}
	
	/**
	 * get photos by category id
	 */
	
	function getPhotosToMenu ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$photo_list = $this->getPhotosSearch("order", "asc", $search_form, 1, $lang_id, $page_number);
			
			return $photo_list;
		}
	}
	
	/**
	 * wyciaga dane o znajomych
	 */
	
	function getPhotosByNews ($limit, $page_number, $lang_id){
		
		if($limit){

			global $__CFG;
		// TEST!!!
		$__CFG['record_count_limit'] = $limit;
		
		$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '5' order by date_created desc";		
			$sql_count  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '5' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getPhotosByCategoryToView ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '$category_id' and photo_multilang.status = '2' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '$category_id' and photo_multilang.status = '2' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getPhotosByCategoryToViewRandom ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '$category_id' and photo_multilang.status = '2' order by rand()";		
			$sql_count  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '$category_id' and photo_multilang.status = '2' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii biorąc pod uwagę typ wiadomości
	 */
	
	function getPhotosByCategoryAndTypeToView ($type, $limit, $page_number, $lang_id, $category_id){
		
			if($type && $limit && $page_number && $lang_id && $category_id){
			
			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '$category_id' and photo_multilang.type = '$type' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.language_id = '$lang_id' and photo.category_id = '$category_id' and photo_multilang.type = '$type' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}

	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego artykułu/
	 */
	
	function getPhotoByUrlNameAndId($url_name, $photo_id) {
			
		if ($url_name && $photo_id) {	
			//print_r($photo_id);
			$sql = "select photo_multilang.* from photo_multilang where photo_multilang.url_name = '$url_name' and photo_multilang.photo_id != '$photo_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo artykułu/
	 */
	
	function getPhotoByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select photo_multilang.* from photo_multilang where photo_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getPhoto($photo_id, $lang_id) {
			
		if ($photo_id && $lang_id) {	
			$sql = "select photo.category_id, photo.order, photo.pic_01, photo.pic_02, photo.pic_03, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.id = '$photo_id' and photo_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getPhotoByUrlNameToView($url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select photo.category_id, photo.order, photo.pic_01, photo.pic_02, photo.pic_03, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.url_name = '$url_name' and photo_multilang.language_id = '$lang_id'";
			$photo_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($photo_details)) {
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.`order` > '$photo_details[order]' and photo.category_id = '$photo_details[category_id]' and photo_multilang.language_id = '$lang_id' and photo_multilang.status != 1 order by photo.`order` asc limit 1";
				$next_photo = $this->DBM->getRow($sql);
				
				if (sizeof($next_photo)) {
					$photo_details['next'] = $next_photo['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.`order` < '$photo_details[order]' and photo.category_id = '$photo_details[category_id]' and photo_multilang.language_id = '$lang_id' and photo_multilang.status != 1 order by photo.`order` desc limit 1";
				$previous_photo = $this->DBM->getRow($sql);
				
				if (sizeof($previous_photo)) {
					$photo_details['previous'] = $previous_photo['url_name'];
				}
			}		
		
		
		}
		return $photo_details;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getPhotoByUrlNameAndTypeToView($type, $url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select photo.category_id, photo.order, photo.pic_01, photo.pic_02, photo.pic_03, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo_multilang.url_name = '$url_name' and photo_multilang.language_id = '$lang_id'";
			$photo_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($photo_details)) {
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.`order` > '$photo_details[order]' and photo.category_id = '$photo_details[category_id]' and photo_multilang.language_id = '$lang_id' and photo_multilang.type = '$type' and photo_multilang.status != 1 order by photo.`order` asc limit 1";
				$next_photo = $this->DBM->getRow($sql);
				
				if (sizeof($next_photo)) {
					$photo_details['next'] = $next_photo['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.`order` < '$photo_details[order]' and photo.category_id = '$photo_details[category_id]' and photo_multilang.language_id = '$lang_id' and photo_multilang.type = '$type' and photo_multilang.status != 1 order by photo.`order` desc limit 1";
				$previous_photo = $this->DBM->getRow($sql);
				
				if (sizeof($previous_photo)) {
					$photo_details['previous'] = $previous_photo['url_name'];
				}
			}		
		
		
		}
		return $photo_details;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstPhotoInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select photo.id from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.category_id = '$category_id' and photo_multilang.language_id = '$lang_id' and photo_multilang.status != 0 order by photo.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getPhotoDetails($photo_id, $lang_id) {
		
		if ($photo_id && $lang_id) {
			$sql = "select photo.category_id, photo.order, photo.pic_01, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.id = '$photo_id' and photo_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function savePhoto($photo_form) {
		
		global $__CFG;
		
		// data bieżąca 
		if($photo_form['date_created']){
			
			$date_now = $photo_form['date_created'];
			
		}
		else{
			
			$date_now = date("Y-m-d H:i:s", time());
			
		}
		
		
		
		if (!$photo_form['photo_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from photo where category_id = '$photo_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into photo (category_id, `order`) values ('$photo_form[category_id]', '$next_order') ";
			$rv = $this->DBM->modifyTable($sql);
			$photo_id = $this->DBM->lastInsertID;
			
					// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "photo_".$photo_id."_01.jpg";
				$sql = "update photo set pic_01 = '$pic_01' where id = '$photo_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizePhotoPhoto ($_FILES['pic_01']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_01_01.jpg", 70, 70);
				$this->resizePhotoPhoto ($_FILES['pic_01']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_02_01.jpg", 400, 400);
				$this->resizePhotoPhoto ($_FILES['pic_01']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_03_01.jpg", 800, 800);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			// dodaj obrazek nr 2
			if ($_FILES['pic_02']['name']) {
				$pic_02 = "photo_".$photo_id."_02.jpg";
				$sql = "update photo set pic_02 = '$pic_02' where id = '$photo_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizePhotoPhoto ($_FILES['pic_02']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_01_02.jpg", 70, 70);
				$this->resizePhotoPhoto ($_FILES['pic_02']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_02_02.jpg", 400, 400);
				$this->resizePhotoPhoto ($_FILES['pic_02']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_03_02.jpg", 800, 800);
				unlink($_FILES['pic_02']['tmp_name']);				
				

			}
			
					
			// dodaj obrazek nr 3
			if ($_FILES['pic_03']['name']) {
				$pic_03 = "photo_".$photo_id."_03.jpg";
				$sql = "update photo set pic_03 = '$pic_03' where id = '$photo_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizePhotoPhoto ($_FILES['pic_03']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_01_03.jpg", 70, 70);
				$this->resizePhotoPhoto ($_FILES['pic_03']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_02_03.jpg", 400, 400);
				$this->resizePhotoPhoto ($_FILES['pic_03']['tmp_name'], $__CFG['photo_pictures_path'].$photo_id."_03_03.jpg", 800, 800);
				unlink($_FILES['pic_03']['tmp_name']);				
				

			}			
		
			
			if ($photo_id) {
				
				// zapisujemy wersję językową
				$sql  = "insert into photo_multilang (photo_id, type, url_name, podpis, language_id, title, abstract, content, status, date_created, date_modified) ";
				$sql .= " values ('$photo_id', '$photo_form[type]', '$photo_form[url_name]', '$photo_form[podpis]', '$photo_form[language_id]', '$photo_form[title]', '$photo_form[abstract]', '$photo_form[content]', '$photo_form[status]', '$date_now', '$date_now') ";
				$rv = $this->DBM->modifyTable($sql);
				
				// i wszystkie pozostałe wersje językowe (puste)
				$sql = "select * from language";
				$languages_list = $this->DBM->getTable($sql);
				
				if (sizeof($languages_list)) {
					foreach ($languages_list as $language) {
						// bez tego, który już wczesniej zapisaliśmy!
						if ($language['id'] != $photo_form['language_id']) {
							$sql  = "insert into photo_multilang (photo_id, language_id, status, date_created, date_modified, title) ";
							$sql .= " values ('$photo_id', '$language[id]', '0', '$date_now', '$date_now', '$photo_form[title]') ";
							$rv = $this->DBM->modifyTable($sql);
						}
					}
				}
			}
		}
		else {
			
			// aktualizacja artykułu
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update photo set category_id = '$photo_form[category_id]' where id = '$photo_form[photo_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			
					// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($photo_form['remove_photo_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update photo set pic_01 = '$pic_01' where id = '$photo_form[photo_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_01_01.jpg");
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_02_01.jpg");
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_03_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "photo_".$photo_form['photo_id']."_01.jpg";
				$sql = "update photo set pic_01 = '$pic_01' where id = '$photo_form[photo_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				


				
				// pierwsza miniatura
				$this->resizePhotoPhoto ($_FILES['pic_01']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_01_01.jpg", 70, 70);
				$this->resizePhotoPhoto ($_FILES['pic_01']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_02_01.jpg", 400, 400);
				$this->resizePhotoPhoto ($_FILES['pic_01']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_03_01.jpg", 800, 800);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}
			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 2
			if ($photo_form['remove_photo_02']) {
				// usuwamy obrazek
				$pic_02 = "";
				$sql = "update photo set pic_02 = '$pic_02' where id = '$photo_form[photo_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_01_02.jpg");
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_02_02.jpg");
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_03_02.jpg");
			}
			elseif ($_FILES['pic_02']['name']) {
				// aktualizujemy obrazek
				$pic_02 = "photo_".$photo_form['photo_id']."_02.jpg";
				$sql = "update photo set pic_02 = '$pic_02' where id = '$photo_form[photo_id]'";
				$rv = $this->DBM->modifyTable($sql);

				
				// pierwsza miniatura
				$this->resizePhotoPhoto ($_FILES['pic_02']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_01_02.jpg", 70, 70);
				$this->resizePhotoPhoto ($_FILES['pic_02']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_02_02.jpg", 400, 400);
				$this->resizePhotoPhoto ($_FILES['pic_02']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_03_02.jpg", 800, 800);
				unlink($_FILES['pic_02']['tmp_name']);					
				
			}			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 3
			if ($photo_form['remove_photo_03']) {
				// usuwamy obrazek
				$pic_03 = "";
				$sql = "update photo set pic_03 = '$pic_03' where id = '$photo_form[photo_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_01_03.jpg");
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_02_03.jpg");
				unlink($__CFG['photo_pictures_path'].$photo_form['photo_id']."_03_03.jpg");
			}
			elseif ($_FILES['pic_03']['name']) {
				// aktualizujemy obrazek
				$pic_03 = "photo_".$photo_form['photo_id']."_03.jpg";
				$sql = "update photo set pic_03 = '$pic_03' where id = '$photo_form[photo_id]'";
				$rv = $this->DBM->modifyTable($sql);
				

				
				// pierwsza miniatura
				$this->resizePhotoPhoto ($_FILES['pic_03']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_01_03.jpg", 70, 70);
				$this->resizePhotoPhoto ($_FILES['pic_03']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_02_03.jpg", 400, 400);
				$this->resizePhotoPhoto ($_FILES['pic_03']['tmp_name'], $__CFG['photo_pictures_path'].$photo_form[photo_id]."_03_03.jpg", 800, 800);
				unlink($_FILES['pic_03']['tmp_name']);					
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// a potem wersja językowa
			$sql = "update photo_multilang set type = '$photo_form[type]', url_name = '$photo_form[url_name]', podpis = '$photo_form[podpis]',  title = '$photo_form[title]', abstract = '$photo_form[abstract]', content = '$photo_form[content]', status = '$photo_form[status]', date_modified = '$date_now' where photo_id = '$photo_form[photo_id]' and language_id = '$photo_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($photo_id) { 
			return $photo_id;
		}
		else {
			return $photo_form['photo_id'];
		}
	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removePhoto($photo_id) {
			
		if ($photo_id) {
			// metryka
			$sql = "delete from photo where id = $photo_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from photo_multilang where photo_id = $photo_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * uaktualnia automatycznie kolejnosc artykułow w kategorii
	 */
	
	function updateOrder($photo_id, $position) {
			
		if ($photo_id && $position) {
			
			$sql = "update photo set `order` = '$position' where id = '$photo_id' ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;

	}
	
	/**
	 * gets n random photos from given category
	 */
	
	function getRandomPhotos ($category_id, $limit, $lang_id) {
		
		if ($category_id && $limit && $lang_id) {
			$sql = "select photo.category_id, photo.order, photo_multilang.* from photo, photo_multilang where photo_multilang.photo_id = photo.id and photo.category_id = '$category_id' and photo_multilang.language_id = '$lang_id' order by rand() limit $limit";
			$photo_list = $this->DBM->getTable($sql);
			
			return $photo_list;
		}
		
	}
	
	/**
	 * gets all photos
	 */
	
	function getPhotosAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from photo_multilang where language_id = '$lang_id' order by date_modified desc ";
			$photo_list = $this->DBM->getTable($sql);
			
			return $photo_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 5;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizePhotoPhoto ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['photo_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['photo_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 100);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['photo_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 100);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>