<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Portfolio {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Portfolio() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getPortfolios($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = 100;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.category_id = '$category_id' and portfolio_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.category_id = '$category_id' and portfolio_multilang.language_id = '$lang_id' ";		
		$sql .= " order by portfolio.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$portfolio_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($portfolio_list_temp)) {
			$portfolio_list = array();
			foreach ($portfolio_list_temp as $portfolio_details) {
				$portfolio_list[$portfolio_details[portfolio_id]] = $portfolio_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $portfolio_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($portfolio_id, $status) {
		
		if ($portfolio_id) {
			
			$sql = "update portfolio_multilang set status = '$status' where portfolio_multilang.portfolio_id = '$portfolio_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getPortfoliosSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND portfolio.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND portfolio.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(portfolio_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(portfolio_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(portfolio_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(portfolio_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(portfolio_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(portfolio_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND portfolio_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND portfolio_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND portfolio_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND portfolio_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND portfolio_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND portfolio_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by portfolio.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$portfolio_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($portfolio_list_temp)) {
			$portfolio_list = array();
			foreach ($portfolio_list_temp as $portfolio_details) {
				$portfolio_list[$portfolio_details[portfolio_id]] = $portfolio_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $portfolio_list;
	}
	
	/**
	 * pobiera przefiltrowaną listę artykułów /dla widokow/
	 */
	
	function getPortfoliosSearchToView($limit, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {

		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND portfolio.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND portfolio.category_id = ".$search_form['category_id']." ";
			}
			else{
				$sql .= " AND (portfolio.category_id = 1 or portfolio.category_id = 3 or portfolio.category_id = 10 or portfolio.category_id = 11) ";
				$sql_count .= " AND (portfolio.category_id = 1 or portfolio.category_id = 3 or portfolio.category_id = 10 or portfolio.category_id = 11) ";				
				
			}
			if ($search_form['phrase']) {
				$sql .= " AND (lower(portfolio_multilang.title) like lower('%".$search_form['phrase']."%') or lower(portfolio_multilang.content) like lower('%".$search_form['phrase']."%')) ";
				$sql_count .= " AND (lower(portfolio_multilang.title) like lower('%".$search_form['phrase']."%') or lower(portfolio_multilang.content) like lower('%".$search_form['phrase']."%')) ";
			}

		}
		
		$sql .= " order by portfolio.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$portfolio_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($portfolio_list_temp)) {
			$portfolio_list = array();
			foreach ($portfolio_list_temp as $portfolio_details) {
				$portfolio_list[$portfolio_details[portfolio_id]] = $portfolio_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $portfolio_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getPortfoliosForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$portfolio_list = $this->getPortfoliosSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_portfolio where product_id = '$product_id'";
			$pictures_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($pictures_temp)) {
				foreach ($pictures_temp as $details) {
					if ($portfolio_list[$details['portfolio_id']]) {
						$portfolio_list[$details['portfolio_id']]['selected'] = 1;
					}
				}
			}
			
			return $portfolio_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getPortfoliosOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang, product_portfolio where portfolio_multilang.portfolio_id = portfolio.id and product_portfolio.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and product_portfolio.product_id = '$product_id' ";
			$portfolio_list = $this->DBM->getTable($sql);
			
			return $portfolio_list;
		}
	}
	
	
	/**
	 * get pictures by category id
	 */
	
	function getPortfoliosByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$portfolio_list = $this->getPortfoliosSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $portfolio_list;
		}
	}
	
	/**
	 * get pictures by category id
	 */
	
	function getPortfoliosToMenu ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$portfolio_list = $this->getPortfoliosSearch("order", "asc", $search_form, 1, $lang_id, $page_number);
			
			return $portfolio_list;
		}
	}
	
	/**
	 * wyciaga dane o znajomych
	 */
	
	function getPortfoliosByNews ($limit, $page_number, $lang_id){
		
		if($limit){

			global $__CFG;
		// TEST!!!
		$__CFG['record_count_limit'] = $limit;
		
		$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '5' order by date_created desc";		
			$sql_count  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '5' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getPortfoliosByCategoryToView ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '$category_id' and portfolio_multilang.status = '2' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '$category_id' and portfolio_multilang.status = '2' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getPortfoliosByCategoryToViewRandom ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '$category_id' and portfolio_multilang.status = '2' order by rand()";		
			$sql_count  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '$category_id' and portfolio_multilang.status = '2' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii biorąc pod uwagę typ wiadomości
	 */
	
	function getPortfoliosByCategoryAndTypeToView ($type, $limit, $page_number, $lang_id, $category_id){
		
			if($type && $limit && $page_number && $lang_id && $category_id){
			
			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '$category_id' and portfolio_multilang.type = '$type' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio.category_id = '$category_id' and portfolio_multilang.type = '$type' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}

	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego artykułu/
	 */
	
	function getPortfolioByUrlNameAndId($url_name, $portfolio_id) {
			
		if ($url_name && $portfolio_id) {	
			//print_r($portfolio_id);
			$sql = "select portfolio_multilang.* from portfolio_multilang where portfolio_multilang.url_name = '$url_name' and portfolio_multilang.portfolio_id != '$portfolio_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo artykułu/
	 */
	
	function getPortfolioByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select portfolio_multilang.* from portfolio_multilang where portfolio_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getPortfolio($portfolio_id, $lang_id) {
			
		if ($portfolio_id && $lang_id) {	
			$sql = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio.pic_02, portfolio.pic_03, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.id = '$portfolio_id' and portfolio_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getPortfolioByUrlNameToView($url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio.pic_02, portfolio.pic_03, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.url_name = '$url_name' and portfolio_multilang.language_id = '$lang_id'";
			$portfolio_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($portfolio_details)) {
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.`order` > '$portfolio_details[order]' and portfolio.category_id = '$portfolio_details[category_id]' and portfolio_multilang.language_id = '$lang_id' and portfolio_multilang.status != 1 order by portfolio.`order` asc limit 1";
				$next_portfolio = $this->DBM->getRow($sql);
				
				if (sizeof($next_portfolio)) {
					$portfolio_details['next'] = $next_portfolio['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.`order` < '$portfolio_details[order]' and portfolio.category_id = '$portfolio_details[category_id]' and portfolio_multilang.language_id = '$lang_id' and portfolio_multilang.status != 1 order by portfolio.`order` desc limit 1";
				$previous_portfolio = $this->DBM->getRow($sql);
				
				if (sizeof($previous_portfolio)) {
					$portfolio_details['previous'] = $previous_portfolio['url_name'];
				}
			}		
		
		
		}
		return $portfolio_details;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getPortfolioByUrlNameAndTypeToView($type, $url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio.pic_02, portfolio.pic_03, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.url_name = '$url_name' and portfolio_multilang.language_id = '$lang_id'";
			$portfolio_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($portfolio_details)) {
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.`order` > '$portfolio_details[order]' and portfolio.category_id = '$portfolio_details[category_id]' and portfolio_multilang.language_id = '$lang_id' and portfolio_multilang.type = '$type' and portfolio_multilang.status != 1 order by portfolio.`order` asc limit 1";
				$next_portfolio = $this->DBM->getRow($sql);
				
				if (sizeof($next_portfolio)) {
					$portfolio_details['next'] = $next_portfolio['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.`order` < '$portfolio_details[order]' and portfolio.category_id = '$portfolio_details[category_id]' and portfolio_multilang.language_id = '$lang_id' and portfolio_multilang.type = '$type' and portfolio_multilang.status != 1 order by portfolio.`order` desc limit 1";
				$previous_portfolio = $this->DBM->getRow($sql);
				
				if (sizeof($previous_portfolio)) {
					$portfolio_details['previous'] = $previous_portfolio['url_name'];
				}
			}		
		
		
		}
		return $portfolio_details;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstPortfolioInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select portfolio.id from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.category_id = '$category_id' and portfolio_multilang.language_id = '$lang_id' and portfolio_multilang.status != 0 order by portfolio.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getPortfolioDetails($portfolio_id, $lang_id) {
		
		if ($portfolio_id && $lang_id) {
			$sql = "select portfolio.category_id, portfolio.order, portfolio.pic_01, portfolio_multilang.* from portfolio, portfolio_multilang where portfolio_multilang.portfolio_id = portfolio.id and portfolio.id = '$portfolio_id' and portfolio_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function savePortfolio($portfolio_form) {
		
		global $__CFG;
		
		// data bieżąca 
		if($portfolio_form['date_created']){
			
			$date_now = $portfolio_form['date_created'];
			
		}
		else{
			
			$date_now = date("Y-m-d H:i:s", time());
			
		}
		
		
		
		if (!$portfolio_form['portfolio_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from portfolio where category_id = '$portfolio_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into portfolio (category_id, `order`) values ('$portfolio_form[category_id]', '$next_order') ";
			$rv = $this->DBM->modifyTable($sql);
			$portfolio_id = $this->DBM->lastInsertID;
			
					// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "portfolio_".$portfolio_id."_01.jpg";
				$sql = "update portfolio set pic_01 = '$pic_01' where id = '$portfolio_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizePortfolioPortfolio ($_FILES['pic_01']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_01_01.jpg", 70, 70);
				$this->resizePortfolioPortfolio ($_FILES['pic_01']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_02_01.jpg", 500, 500);
				$this->resizePortfolioPortfolio ($_FILES['pic_01']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_03_01.jpg", 800, 800);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			// dodaj obrazek nr 2
			if ($_FILES['pic_02']['name']) {
				$pic_02 = "portfolio_".$portfolio_id."_02.jpg";
				$sql = "update portfolio set pic_02 = '$pic_02' where id = '$portfolio_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizePortfolioPortfolio ($_FILES['pic_02']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_01_02.jpg", 70, 70);
				$this->resizePortfolioPortfolio ($_FILES['pic_02']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_02_02.jpg", 500, 500);
				$this->resizePortfolioPortfolio ($_FILES['pic_02']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_03_02.jpg", 800, 800);
				unlink($_FILES['pic_02']['tmp_name']);				
				

			}
			
					
			// dodaj obrazek nr 3
			if ($_FILES['pic_03']['name']) {
				$pic_03 = "portfolio_".$portfolio_id."_03.jpg";
				$sql = "update portfolio set pic_03 = '$pic_03' where id = '$portfolio_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizePortfolioPortfolio ($_FILES['pic_03']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_01_03.jpg", 70, 70);
				$this->resizePortfolioPortfolio ($_FILES['pic_03']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_02_03.jpg", 500, 500);
				$this->resizePortfolioPortfolio ($_FILES['pic_03']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_id."_03_03.jpg", 800, 800);
				unlink($_FILES['pic_03']['tmp_name']);				
				

			}			
		
			
			if ($portfolio_id) {
				
				// zapisujemy wersję językową
				$sql  = "insert into portfolio_multilang (portfolio_id, type, url_name, podpis, language_id, title, abstract, content, status, date_created, date_modified) ";
				$sql .= " values ('$portfolio_id', '$portfolio_form[type]', '$portfolio_form[url_name]', '$portfolio_form[podpis]', '$portfolio_form[language_id]', '$portfolio_form[title]', '$portfolio_form[abstract]', '$portfolio_form[content]', '$portfolio_form[status]', '$date_now', '$date_now') ";
				$rv = $this->DBM->modifyTable($sql);
				
				// i wszystkie pozostałe wersje językowe (puste)
				$sql = "select * from language";
				$languages_list = $this->DBM->getTable($sql);
				
				if (sizeof($languages_list)) {
					foreach ($languages_list as $language) {
						// bez tego, który już wczesniej zapisaliśmy!
						if ($language['id'] != $portfolio_form['language_id']) {
							$sql  = "insert into portfolio_multilang (portfolio_id, language_id, status, date_created, date_modified, title) ";
							$sql .= " values ('$portfolio_id', '$language[id]', '0', '$date_now', '$date_now', '$portfolio_form[title]') ";
							$rv = $this->DBM->modifyTable($sql);
						}
					}
				}
			}
		}
		else {
			
			// aktualizacja artykułu
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update portfolio set category_id = '$portfolio_form[category_id]' where id = '$portfolio_form[portfolio_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			
					// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($portfolio_form['remove_portfolio_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update portfolio set pic_01 = '$pic_01' where id = '$portfolio_form[portfolio_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_01_01.jpg");
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_02_01.jpg");
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_03_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "portfolio_".$portfolio_form['portfolio_id']."_01.jpg";
				$sql = "update portfolio set pic_01 = '$pic_01' where id = '$portfolio_form[portfolio_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				


				
				// pierwsza miniatura
				$this->resizePortfolioPortfolio ($_FILES['pic_01']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_01_01.jpg", 70, 70);
				$this->resizePortfolioPortfolio ($_FILES['pic_01']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_02_01.jpg", 500, 500);
				$this->resizePortfolioPortfolio ($_FILES['pic_01']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_03_01.jpg", 800, 800);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}
			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 2
			if ($portfolio_form['remove_portfolio_02']) {
				// usuwamy obrazek
				$pic_02 = "";
				$sql = "update portfolio set pic_02 = '$pic_02' where id = '$portfolio_form[portfolio_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_01_02.jpg");
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_02_02.jpg");
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_03_02.jpg");
			}
			elseif ($_FILES['pic_02']['name']) {
				// aktualizujemy obrazek
				$pic_02 = "portfolio_".$portfolio_form['portfolio_id']."_02.jpg";
				$sql = "update portfolio set pic_02 = '$pic_02' where id = '$portfolio_form[portfolio_id]'";
				$rv = $this->DBM->modifyTable($sql);

				
				// pierwsza miniatura
				$this->resizePortfolioPortfolio ($_FILES['pic_02']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_01_02.jpg", 70, 70);
				$this->resizePortfolioPortfolio ($_FILES['pic_02']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_02_02.jpg", 500, 500);
				$this->resizePortfolioPortfolio ($_FILES['pic_02']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_03_02.jpg", 800, 800);
				unlink($_FILES['pic_02']['tmp_name']);					
				
			}			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 3
			if ($portfolio_form['remove_portfolio_03']) {
				// usuwamy obrazek
				$pic_03 = "";
				$sql = "update portfolio set pic_03 = '$pic_03' where id = '$portfolio_form[portfolio_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_01_03.jpg");
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_02_03.jpg");
				unlink($__CFG['portfolio_pictures_path'].$portfolio_form['portfolio_id']."_03_03.jpg");
			}
			elseif ($_FILES['pic_03']['name']) {
				// aktualizujemy obrazek
				$pic_03 = "portfolio_".$portfolio_form['portfolio_id']."_03.jpg";
				$sql = "update portfolio set pic_03 = '$pic_03' where id = '$portfolio_form[portfolio_id]'";
				$rv = $this->DBM->modifyTable($sql);
				

				
				// pierwsza miniatura
				$this->resizePortfolioPortfolio ($_FILES['pic_03']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_01_03.jpg", 70, 70);
				$this->resizePortfolioPortfolio ($_FILES['pic_03']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_02_03.jpg", 500, 500);
				$this->resizePortfolioPortfolio ($_FILES['pic_03']['tmp_name'], $__CFG['portfolio_pictures_path'].$portfolio_form[portfolio_id]."_03_03.jpg", 800, 800);
				unlink($_FILES['pic_03']['tmp_name']);					
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// a potem wersja językowa
			$sql = "update portfolio_multilang set type = '$portfolio_form[type]', url_name = '$portfolio_form[url_name]', podpis = '$portfolio_form[podpis]',  title = '$portfolio_form[title]', abstract = '$portfolio_form[abstract]', content = '$portfolio_form[content]', status = '$portfolio_form[status]', date_modified = '$date_now' where portfolio_id = '$portfolio_form[portfolio_id]' and language_id = '$portfolio_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($portfolio_id) { 
			return $portfolio_id;
		}
		else {
			return $portfolio_form['portfolio_id'];
		}
	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removePortfolio($portfolio_id) {
			
		if ($portfolio_id) {
			// metryka
			$sql = "delete from portfolio where id = $portfolio_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from portfolio_multilang where portfolio_id = $portfolio_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * uaktualnia automatycznie kolejnosc artykułow w kategorii
	 */
	
	function updateOrder($portfolio_id, $position) {
			
		if ($portfolio_id && $position) {
			
			$sql = "update portfolio set `order` = '$position' where id = '$portfolio_id' ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;

	}
	
	/**
	 * gets n random pictures from given category
	 */
	
	function getRandomPortfolios ($limit, $lang_id) {
		
		if ($limit && $lang_id) {
			$sql = "select house_multilang.url_name as category_name, portfolio.category_id, portfolio.order, portfolio_multilang.* from portfolio, portfolio_multilang, house_multilang where portfolio.category_id = house_multilang.house_id and portfolio_multilang.portfolio_id = portfolio.id and portfolio_multilang.language_id = '$lang_id' and portfolio_multilang.status = '2' order by rand() limit $limit";
			$portfolio_list = $this->DBM->getTable($sql);
			
			return $portfolio_list;
		}
		
	}
	
	/**
	 * gets all pictures
	 */
	
	function getPortfoliosAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from portfolio_multilang where language_id = '$lang_id' order by date_modified desc ";
			$portfolio_list = $this->DBM->getTable($sql);
			
			return $portfolio_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 5;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizePortfolioPortfolio ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['portfolio_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['portfolio_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 100);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['portfolio_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 100);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>