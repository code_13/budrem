<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Staff {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Staff() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * wyciąga listę dostepnych wydawców
	 */
	
	function getStaffsForHome($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id = 1) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.category_id = '$category_id' and staff_multilang.status = '2' and staff_multilang.home = '1' and staff_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.category_id = '$category_id' and staff_multilang.status = '2' and staff_multilang.home = '1' and staff_multilang.language_id = '$lang_id' ";		
		$sql .= " order by rand() asc limit 3 ";
		
		// echo $sql."<hr>";
		
		$staff_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($staff_list_temp)) {
			$staff_list = array();
			foreach ($staff_list_temp as $staff_details) {
				$staff_list[$staff_details[staff_id]] = $staff_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $staff_list;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getStaffs($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.category_id = '$category_id' and staff_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.category_id = '$category_id' and staff_multilang.language_id = '$lang_id' ";		
		$sql .= " order by staff.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$staff_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($staff_list_temp)) {
			$staff_list = array();
			foreach ($staff_list_temp as $staff_details) {
				$staff_list[$staff_details[staff_id]] = $staff_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $staff_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($staff_id, $status) {
		
		if ($staff_id) {
			
			$sql = "update staff_multilang set status = '$status' where staff_multilang.staff_id = '$staff_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}

	/**
	 * Przestawia status  na strone glowna
	 */
	
	function setHome ($staff_id, $status) {
		
		if ($staff_id) {
			
			$sql = "update staff_multilang set home = '$status' where staff_multilang.staff_id = '$staff_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getStaffsSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND staff.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND staff.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(staff_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(staff_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(staff_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(staff_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(staff_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(staff_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND staff_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND staff_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND staff_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND staff_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND staff_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND staff_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by staff.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$staff_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($staff_list_temp)) {
			$staff_list = array();
			foreach ($staff_list_temp as $staff_details) {
				$staff_list[$staff_details[staff_id]] = $staff_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $staff_list;
	}
	
	/**
	 * pobiera przefiltrowaną listę artykułów /dla widokow/
	 */
	
	function getStaffsSearchToView($limit, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {

		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND staff.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND staff.category_id = ".$search_form['category_id']." ";
			}
			else{
				$sql .= " AND (staff.category_id = 1 or staff.category_id = 3 or staff.category_id = 10 or staff.category_id = 11) ";
				$sql_count .= " AND (staff.category_id = 1 or staff.category_id = 3 or staff.category_id = 10 or staff.category_id = 11) ";				
				
			}
			if ($search_form['phrase']) {
				$sql .= " AND (lower(staff_multilang.title) like lower('%".$search_form['phrase']."%') or lower(staff_multilang.content) like lower('%".$search_form['phrase']."%')) ";
				$sql_count .= " AND (lower(staff_multilang.title) like lower('%".$search_form['phrase']."%') or lower(staff_multilang.content) like lower('%".$search_form['phrase']."%')) ";
			}

		}
		
		$sql .= " order by staff.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$staff_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($staff_list_temp)) {
			$staff_list = array();
			foreach ($staff_list_temp as $staff_details) {
				$staff_list[$staff_details[staff_id]] = $staff_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number, $limit);
		}
		
		return $staff_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getStaffsForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$staff_list = $this->getStaffsSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_staff where product_id = '$product_id'";
			$staffs_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($staffs_temp)) {
				foreach ($staffs_temp as $details) {
					if ($staff_list[$details['staff_id']]) {
						$staff_list[$details['staff_id']]['selected'] = 1;
					}
				}
			}
			
			return $staff_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getStaffsOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang, product_staff where staff_multilang.staff_id = staff.id and product_staff.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and product_staff.product_id = '$product_id' ";
			$staff_list = $this->DBM->getTable($sql);
			
			return $staff_list;
		}
	}
	
	
	/**
	 * get staffs by category id
	 */
	
	function getStaffsByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$staff_list = $this->getStaffsSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $staff_list;
		}
	}
	
	/**
	 * get staffs by category id
	 */
	
	function getStaffsToMenu ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			$search_form['status'] = 2;
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$staff_list = $this->getStaffsSearch("order", "asc", $search_form, 1, $lang_id, $page_number);
			
			return $staff_list;
		}
	}
	
	/**
	 * wyciaga dane o znajomych
	 */
	
	function getStaffsByNews ($limit, $page_number, $lang_id){
		
		if($limit){

			global $__CFG;
		// TEST!!!
		$__CFG['record_count_limit'] = $limit;
		
		$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and staff.category_id = '5' order by date_created desc";		
			$sql_count  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and staff.category_id = '5' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getStaffsByCategoryToView ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.status = '2' and staff_multilang.language_id = '$lang_id' and staff.category_id = '$category_id' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.status = '2' and staff_multilang.language_id = '$lang_id' and staff.category_id = '$category_id' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii
	 */
	
	function getStaffsByHomeToView ($limit, $page_number, $lang_id, $category_id){
		
		if($limit && $page_number && $lang_id && $category_id){

			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and staff.category_id = '$category_id' and staff_multilang.home = '1' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and staff.category_id = '$category_id' and staff_multilang.home = '1' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga newsy wg kategorii biorąc pod uwagę typ wiadomości
	 */
	
	function getStaffsByCategoryAndTypeToView ($type, $limit, $page_number, $lang_id, $category_id){
		
			if($type && $limit && $page_number && $lang_id && $category_id){
			
			global $__CFG;		
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and staff.category_id = '$category_id' and staff_multilang.type = '$type' order by `order` asc";		
			$sql_count  = "select count(*) as ilosc from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.language_id = '$lang_id' and staff.category_id = '$category_id' and staff_multilang.type = '$type' ";
			
			$sql .= " limit $offset, $limit ";
			
			
			//echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
			
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}

	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego artykułu/
	 */
	
	function getStaffByUrlNameAndId($url_name, $staff_id) {
			
		if ($url_name && $staff_id) {	
			//print_r($staff_id);
			$sql = "select staff_multilang.* from staff_multilang where staff_multilang.url_name = '$url_name' and staff_multilang.staff_id != '$staff_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany artykuł istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo artykułu/
	 */
	
	function getStaffByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select staff_multilang.* from staff_multilang where staff_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getStaff($staff_id, $lang_id) {
			
		if ($staff_id && $lang_id) {	
			$sql = "select staff.category_id, staff.order, staff.pic_01, staff.pic_02, staff.pic_03, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.id = '$staff_id' and staff_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getStaffByUrlNameToView($url_name, $lang_id) {
		
		global $smarty;
			
		if ($url_name && $lang_id) {	
			$sql = "select staff.category_id, staff.order, staff.pic_01, staff.pic_02, staff.pic_03, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.url_name = '$url_name' and staff_multilang.language_id = '$lang_id'";
			$staff_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($staff_details)) {
				
				//Jesli istnieje dany artykuł - podajemy go tagow
				$tags['title'] = str_replace('"', '', $staff_details['title']);
				$tags['abstract'] = str_replace('"', '', $staff_details['abstract']);
				
				$smarty->assign("tags", $tags);
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.`order` > '$staff_details[order]' and staff.category_id = '$staff_details[category_id]' and staff_multilang.language_id = '$lang_id' and staff_multilang.status != 1 order by staff.`order` asc limit 1";
				$next_staff = $this->DBM->getRow($sql);
				
				if (sizeof($next_staff)) {
					$staff_details['next'] = $next_staff['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.`order` < '$staff_details[order]' and staff.category_id = '$staff_details[category_id]' and staff_multilang.language_id = '$lang_id' and staff_multilang.status != 1 order by staff.`order` desc limit 1";
				$previous_staff = $this->DBM->getRow($sql);
				
				if (sizeof($previous_staff)) {
					$staff_details['previous'] = $previous_staff['url_name'];
				}
			}		
		
		
		}
		return $staff_details;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getStaffByUrlNameAndTypeToView($type, $url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select staff.category_id, staff.order, staff.pic_01, staff.pic_02, staff.pic_03, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff_multilang.url_name = '$url_name' and staff_multilang.language_id = '$lang_id'";
			$staff_details = $this->DBM->getRow($sql);
			
		
			if (sizeof($staff_details)) {
				
				// wybieramy nastepny (nowszy)
				
				$sql = "select staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.`order` > '$staff_details[order]' and staff.category_id = '$staff_details[category_id]' and staff_multilang.language_id = '$lang_id' and staff_multilang.type = '$type' and staff_multilang.status != 1 order by staff.`order` asc limit 1";
				$next_staff = $this->DBM->getRow($sql);
				
				if (sizeof($next_staff)) {
					$staff_details['next'] = $next_staff['url_name'];
				}
				
				// wybieramy poprzedni (starszy) 
				
				$sql = "select staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.`order` < '$staff_details[order]' and staff.category_id = '$staff_details[category_id]' and staff_multilang.language_id = '$lang_id' and staff_multilang.type = '$type' and staff_multilang.status != 1 order by staff.`order` desc limit 1";
				$previous_staff = $this->DBM->getRow($sql);
				
				if (sizeof($previous_staff)) {
					$staff_details['previous'] = $previous_staff['url_name'];
				}
			}		
		
		
		}
		return $staff_details;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstStaffInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select staff.id from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.category_id = '$category_id' and staff_multilang.language_id = '$lang_id' and staff_multilang.status != 0 order by staff.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getStaffDetails($staff_id, $lang_id) {
		
		if ($staff_id && $lang_id) {
			$sql = "select staff.category_id, staff.order, staff.pic_01, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.id = '$staff_id' and staff_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function saveStaff($staff_form) {
		
		global $__CFG;
		
		// data bieżąca 
		if($staff_form['date_created']){
			
			$date_now = $staff_form['date_created'];
			
		}
		else{
			
			$date_now = date("Y-m-d H:i:s", time());
			
		}
		
		
		
		if (!$staff_form['staff_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from staff where category_id = '$staff_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into staff (category_id, `order`) values ('$staff_form[category_id]', '0') ";
			$rv = $this->DBM->modifyTable($sql);
			$staff_id = $this->DBM->lastInsertID;
			
					// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "staff_".$staff_id."_01.jpg";
				$sql = "update staff set pic_01 = '$pic_01' where id = '$staff_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeStaffPicture ($_FILES['pic_01']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_01_01.jpg", 138, 213);
				$this->resizeStaffPicture ($_FILES['pic_01']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_02_01.jpg", 220, 222);
				$this->resizeStaffPicture ($_FILES['pic_01']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_03_01.jpg", 500, 500);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			// dodaj obrazek nr 2
			if ($_FILES['pic_02']['name']) {
				$pic_02 = "staff_".$staff_id."_02.jpg";
				$sql = "update staff set pic_02 = '$pic_02' where id = '$staff_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeStaffPicture ($_FILES['pic_02']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_01_02.jpg", 138, 213);
				$this->resizeStaffPicture ($_FILES['pic_02']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_02_02.jpg", 220, 222);
				$this->resizeStaffPicture ($_FILES['pic_02']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_03_02.jpg", 500, 500);
				unlink($_FILES['pic_02']['tmp_name']);				
				

			}
			
					
			// dodaj obrazek nr 3
			if ($_FILES['pic_03']['name']) {
				$pic_03 = "staff_".$staff_id."_03.jpg";
				$sql = "update staff set pic_03 = '$pic_03' where id = '$staff_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeStaffPicture ($_FILES['pic_03']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_01_03.jpg", 138, 213);
				$this->resizeStaffPicture ($_FILES['pic_03']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_02_03.jpg", 220, 222);
				$this->resizeStaffPicture ($_FILES['pic_03']['tmp_name'], $__CFG['staff_pictures_path'].$staff_id."_03_03.jpg", 500, 500);
				unlink($_FILES['pic_03']['tmp_name']);				
				

			}			
		
			
			if ($staff_id) {
				
				// zapisujemy wersję językową
				$sql  = "insert into staff_multilang (staff_id, type, url_name, podpis, language_id, title, abstract, content, status, date_created, date_modified) ";
				$sql .= " values ('$staff_id', '$staff_form[type]', '$staff_form[url_name]', '$staff_form[podpis]', '$staff_form[language_id]', '$staff_form[title]', '$staff_form[abstract]', '$staff_form[content]', '$staff_form[status]', '$date_now', '$date_now') ";
				$rv = $this->DBM->modifyTable($sql);
				
				// i wszystkie pozostałe wersje językowe (puste)
				$sql = "select * from language";
				$languages_list = $this->DBM->getTable($sql);
				
				if (sizeof($languages_list)) {
					foreach ($languages_list as $language) {
						// bez tego, który już wczesniej zapisaliśmy!
						if ($language['id'] != $staff_form['language_id']) {
							$sql  = "insert into staff_multilang (staff_id, language_id, status, date_created, date_modified, title) ";
							$sql .= " values ('$staff_id', '$language[id]', '0', '$date_now', '$date_now', '$staff_form[title]') ";
							$rv = $this->DBM->modifyTable($sql);
						}
					}
				}
			}
		}
		else {
			
			// aktualizacja artykułu
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update staff set category_id = '$staff_form[category_id]' where id = '$staff_form[staff_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			
					// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($staff_form['remove_picture_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update staff set pic_01 = '$pic_01' where id = '$staff_form[staff_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_01_01.jpg");
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_02_01.jpg");
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_03_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "staff_".$staff_form['staff_id']."_01.jpg";
				$sql = "update staff set pic_01 = '$pic_01' where id = '$staff_form[staff_id]'";
				$rv = $this->DBM->modifyTable($sql);
				


				
				// pierwsza miniatura
				$this->resizeStaffPicture ($_FILES['pic_01']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_01_01.jpg", 138, 213);
				$this->resizeStaffPicture ($_FILES['pic_01']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_02_01.jpg", 220, 222);
				$this->resizeStaffPicture ($_FILES['pic_01']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_03_01.jpg", 500, 500);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}
			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 2
			if ($staff_form['remove_picture_02']) {
				// usuwamy obrazek
				$pic_02 = "";
				$sql = "update staff set pic_02 = '$pic_02' where id = '$staff_form[staff_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_01_02.jpg");
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_02_02.jpg");
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_03_02.jpg");
			}
			elseif ($_FILES['pic_02']['name']) {
				// aktualizujemy obrazek
				$pic_02 = "staff_".$staff_form['staff_id']."_02.jpg";
				$sql = "update staff set pic_02 = '$pic_02' where id = '$staff_form[staff_id]'";
				$rv = $this->DBM->modifyTable($sql);
				

				
				// pierwsza miniatura
				$this->resizeStaffPicture ($_FILES['pic_02']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_01_02.jpg", 138, 213);
				$this->resizeStaffPicture ($_FILES['pic_02']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_02_02.jpg", 220, 222);
				$this->resizeStaffPicture ($_FILES['pic_02']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_03_02.jpg", 500, 500);
				unlink($_FILES['pic_02']['tmp_name']);					
				
			}			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 3
			if ($staff_form['remove_picture_03']) {
				// usuwamy obrazek
				$pic_03 = "";
				$sql = "update staff set pic_03 = '$pic_03' where id = '$staff_form[staff_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_01_03.jpg");
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_02_03.jpg");
				unlink($__CFG['staff_pictures_path'].$staff_form['staff_id']."_03_03.jpg");
			}
			elseif ($_FILES['pic_03']['name']) {
				// aktualizujemy obrazek
				$pic_03 = "staff_".$staff_form['staff_id']."_03.jpg";
				$sql = "update staff set pic_03 = '$pic_03' where id = '$staff_form[staff_id]'";
				$rv = $this->DBM->modifyTable($sql);

				
				// pierwsza miniatura
				$this->resizeStaffPicture ($_FILES['pic_03']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_01_03.jpg", 138, 213);
				$this->resizeStaffPicture ($_FILES['pic_03']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_02_03.jpg", 220, 222);
				$this->resizeStaffPicture ($_FILES['pic_03']['tmp_name'], $__CFG['staff_pictures_path'].$staff_form[staff_id]."_03_03.jpg", 500, 500);
				unlink($_FILES['pic_03']['tmp_name']);					
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// a potem wersja językowa
			$sql = "update staff_multilang set type = '$staff_form[type]', url_name = '$staff_form[url_name]', podpis = '$staff_form[podpis]',  title = '$staff_form[title]', abstract = '$staff_form[abstract]', content = '$staff_form[content]', status = '$staff_form[status]', date_modified = '$date_now' where staff_id = '$staff_form[staff_id]' and language_id = '$staff_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($staff_id) { 
			return $staff_id;
		}
		else {
			return $staff_form['staff_id'];
		}
	}
	
	
	/**
	 * zapisuje pojedynczy artykuł (notes z formularza)
	 */
	
	function saveStaffNotes($staff_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
			
		
		$staff_form['category_id'] = 10;
		$staff_form['language_id'] = 1;
		$staff_form['status'] = 0;
		
		if (!$staff_form['staff_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from staff where category_id = '$staff_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into staff (category_id, `order`) values ('$staff_form[category_id]', '0') ";
			$rv = $this->DBM->modifyTable($sql);
			$staff_id = $this->DBM->lastInsertID;
	
		
			if ($staff_id) {
				
				// zapisujemy wersję językową
				$sql  = "insert into staff_multilang (staff_id, type, url_name, podpis, language_id, title, abstract, content, status, date_created, date_modified) ";
				$sql .= " values ('$staff_id', '$staff_form[type]', '$staff_form[url_name]', '$staff_form[name]', '$staff_form[language_id]', '$staff_form[title]', '$staff_form[abstract]', '$staff_form[content]', '$staff_form[status]', '$date_now', '$date_now') ";
				$rv = $this->DBM->modifyTable($sql);
				
				// i wszystkie pozostałe wersje językowe (puste)
				$sql = "select * from language";
				$languages_list = $this->DBM->getTable($sql);
				
				if (sizeof($languages_list)) {
					foreach ($languages_list as $language) {
						// bez tego, który już wczesniej zapisaliśmy!
						if ($language['id'] != $staff_form['language_id']) {
							$sql  = "insert into staff_multilang (staff_id, language_id, status, date_created, date_modified, title) ";
							$sql .= " values ('$staff_id', '$language[id]', '0', '$date_now', '$date_now', '$staff_form[title]') ";
							$rv = $this->DBM->modifyTable($sql);
						}
					}
				}
			}
		}

		// zwracamy ID ostatnio zapisanego artykułu

		return $staff_id;

	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removeStaff($staff_id) {
			
		if ($staff_id) {
			// metryka
			$sql = "delete from staff where id = $staff_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from staff_multilang where staff_id = $staff_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * uaktualnia automatycznie kolejnosc artykułow w kategorii
	 */
	
	function updateOrder($staff_id, $position) {
			
		if ($staff_id && $position) {
			
			$sql = "update staff set `order` = '$position' where id = '$staff_id' ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;

	}
	
	/**
	 * gets n random staffs from given category
	 */
	
	function getRandomStaffs ($category_id, $limit, $lang_id) {
		
		if ($category_id && $limit && $lang_id) {
			$sql = "select staff.category_id, staff.order, staff_multilang.* from staff, staff_multilang where staff_multilang.staff_id = staff.id and staff.category_id = '$category_id' and staff_multilang.language_id = '$lang_id' order by rand() limit $limit";
			$staff_list = $this->DBM->getTable($sql);
			
			return $staff_list;
		}
		
	}
	
	/**
	 * gets all staffs
	 */
	
	function getStaffsAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from staff_multilang where language_id = '$lang_id' order by date_modified desc ";
			$staff_list = $this->DBM->getTable($sql);
			
			return $staff_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 2;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizeStaffPicture ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['gallery_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 80);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 80);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>