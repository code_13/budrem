<?
/**
 * @copyright 
 * @author 
 * @see 
 *
 * obsłuha artykułów tekstowych w wersji wielojęzycznej
 *
 */

require_once 'Utils.class.php';

class Video {

	/**
	 * @var object DBManager
	 */
	var $DBM;

	function Video() {
		global $DBM;
	 	$this->DBM = $DBM;
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getVideos($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id'";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' ";		
		$sql .= " order by video.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$video_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($video_list_temp)) {
			$video_list = array();
			foreach ($video_list_temp as $video_details) {
				$video_list[$video_details[video_id]] = $video_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $video_list;
	}
	
	/**
	 * wyciaga listę artykułów dla rss
	 */
	
	function getVideoByRss ($order = "order", $direction = "asc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video_multilang.status = '2' ";
			
			$sql .= " order by video_multilang.date_created desc limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * Nadaje polskie nazwy miesiecy, dni etc
	 */
	
	function dateV($format,$timestamp=null){
		$to_convert = array(
			'l'=>array('dat'=>'N','str'=>array('Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota','Niedziela')),
			'F'=>array('dat'=>'n','str'=>array('styczeń','luty','marzec','kwiecień','maj','czerwiec','lipiec','sierpień','wrzesień','październik','listopad','grudzień')),
			'f'=>array('dat'=>'n','str'=>array('sty','lut','mar','kwi','maj','cze','lip','sie','wrz','paź','lis','gru'))
		);
		if ($pieces = preg_split('#[:/.\-, ]#', $format)){	
			if ($timestamp === null) { $timestamp = time(); }
			foreach ($pieces as $datepart){
				if (array_key_exists($datepart,$to_convert)){
					$replace[] = $to_convert[$datepart]['str'][(date($to_convert[$datepart]['dat'],$timestamp)-1)];
				}else{
					$replace[] = date($datepart,$timestamp);
				}
			}
			$result = strtr($format,array_combine($pieces,$replace));
			return $result;
		}
	}
	
	/**
	 * pobiera listę artykułów (posortowaną)
	 */
	
	function getVideosByWtz($order = "order", $direction = "desc", $page_number = 1, $category_id = 1, $lang_id, $wtz) {
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		if (!$category_id) $category_id = 1;
		
		$sql  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.author_id = '$wtz' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.author_id = '$wtz'  ";		
		$sql .= " order by video.`$order` $direction limit $offset, $limit ";
		
		// echo $sql."<hr>";
		
		$video_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($video_list_temp)) {
			$video_list = array();
			foreach ($video_list_temp as $video_details) {
				$video_list[$video_details[video_id]] = $video_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $video_list;
	}
	
	/**
	 * Przestawia status 
	 */
	
	function setStatus ($video_id, $status) {
		
		if ($video_id) {
			
			$sql = "update video_multilang set status = '$status' where video_multilang.video_id = '$video_id' ";
			$this->DBM->modifyTable($sql);
			
		}
	}	
		
	/**
	 * pobiera przefiltrowaną listę artykułów
	 */
	
	function getVideosSearch($order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		
			// ustawienie numeru strony do stronicowania (jezeli nie została podana)
			if (!sizeof($_REQUEST['page_number'])) {
				$_REQUEST['page_number'] = 1;
			}		
		
		$page_number = $_REQUEST['page_number'];
		
		
		global $__CFG;
		
		$limit = $__CFG['record_count_limit'];
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' ";		
		$sql_count  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' ";
				
		if(sizeof($search_form)) {
			
			if ($search_form['category_id']) {
				$sql .= " AND video.category_id = ".$search_form['category_id']." ";
				$sql_count .= " AND video.category_id = ".$search_form['category_id']." ";
			}
			if ($search_form['title']) {
				$sql .= " AND lower(video_multilang.title) like lower('%".$search_form['title']."%') ";
				$sql_count .= " AND lower(video_multilang.title) like lower('%".$search_form['title']."%') ";
			}
			if ($search_form['abstract']) {
				$sql .= " AND lower(video_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
				$sql_count .= " AND lower(video_multilang.abstract) like lower('%".$search_form['abstract']."%') ";
			}
			if ($search_form['content']) {
				$sql .= " AND lower(video_multilang.content) like lower('%".$search_form['content']."%') ";
				$sql_count .= " AND lower(video_multilang.content) like lower('%".$search_form['content']."%') ";
			}
			if ($search_form['status']) {
				$sql .= " AND video_multilang.status = ".$search_form['status']." ";
				$sql_count .= " AND video_multilang.status = ".$search_form['status']." ";
			}
			if ($search_form['date_created_from'] && $search_form['date_created_to']) {
				$sql .= " AND video_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
				$sql_count .= " AND video_multilang.date_created between '".$search_form['date_created_from']."' and '".$search_form['date_created_to']."' ";
			}
			if ($search_form['date_modified_from'] && $search_form['date_modified_to']) {
				$sql .= " AND video_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
				$sql_count .= " AND video_multilang.date_modified between '".$search_form['date_modified_from']."' and '".$search_form['date_modified_to']."' ";
			}
		}
		
		$sql .= " order by video.`$order` $direction ";
		$sql .= " limit $offset, $limit ";
		
		// echo $sql;
		
		// ilośc wszystkich rekordów
		$temp = $this->DBM->getRow($sql_count);
		$record_count = $temp['ilosc'];
		
		// echo $record_count."<br>";
		
		$video_list_temp = $this->DBM->getTable($sql);
		
		// reindex array
		if (sizeof($video_list_temp)) {
			$video_list = array();
			foreach ($video_list_temp as $video_details) {
				$video_list[$video_details[video_id]] = $video_details;
			}
		}
		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParametersNew($record_count, $page_number);
		}
		
		return $video_list;
	}
		
	/**
	 * pobiera przefiltrowaną listę artykułów powiązanych z podanym produktem
	 * wraz z listą wszystkich dostępnych artykułów (dla celów administracyjnych)
	 */
	
	function getVideosForProduct($product_id, $order = "order", $direction = "desc", $search_form = null, $page_number = 1, $lang_id) {
		
		global $__CFG;
		
		if ($product_id) {
			
			// najpierw wybieramy normalną listę artykułów - przefiltrowaną wg. wskazanych filtrów
			$video_list = $this->getVideosSearch($order, $direction, $search_form, $page_number, $lang_id);
			
			// następnie wybieramy listę artykułów powiązanych z podanym produktem
			$sql = "select * from product_video where product_id = '$product_id'";
			$videos_temp = $this->DBM->getTable($sql);
			
			// przelatujemy tą tablicę zazanczając na liscie wssystkich artykułów te, które są powiązane
			if (sizeof($videos_temp)) {
				foreach ($videos_temp as $details) {
					if ($video_list[$details['video_id']]) {
						$video_list[$details['video_id']]['selected'] = 1;
					}
				}
			}
			
			return $video_list;
		}
	}
	
	/**
	 * pobiera artykuły TYLKO powiązane z podanym produktem
	 */
	
	function getVideosOnlyForProduct ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$sql = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang, product_video where video_multilang.video_id = video.id and product_video.video_id = video.id and video_multilang.language_id = '$lang_id' and product_video.product_id = '$product_id' ";
			$video_list = $this->DBM->getTable($sql);
			
			return $video_list;
		}
	}
	
	
	/**
	 * get videos by category id
	 */
	
	function getVideosByCategory ($category_id, $lang_id) {
		
		if ($category_id && $lang_id) {
			$search_form['category_id'] = $category_id;
			
			// paging cheating ;-)
			$__CFG['record_count_limit'] = 1;
			
			$video_list = $this->getVideosSearch("order", "desc", $search_form, 1, $lang_id, $page_number);
			
			return $video_list;
		}
	}
	
	/**
	 * wyciaga listę videoa dla widoku serwisu wraz z pagingiem
	 */
	
	function getVideosByView ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video.category_id = '1' and video_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video.category_id = '1' and video_multilang.status = '2' ";
			
			$sql .= " order by video.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$video_list = $this->DBM->getTable($sql);
			
		
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number, $limit);
			}
		}
		
		return $video_list;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getVideoByUrlNameToView($url_name, $lang_id) {
			
		if ($url_name && $lang_id) {	
			$sql = "select video.category_id, video.order, video.pic_01, video.pic_02, video.pic_03, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video_multilang.url_name = '$url_name' and video_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
			
			$mies_pl = $this->dateV('f',strtotime($rv['date_created']));
			$rv[miesiac] = $mies_pl;			
			
		}
		return $rv;
	}
	
	/**
	 * Liczba komentarzy dla danego wpisu
	 */
	
	function calculateCommentsVolumeForVideo($video_list) {
		
		if (sizeof($video_list)) {
			
			// dla każdego użytkownika sprawdzamy ilośc jego znajomych
			foreach ($video_list as $key => $video_details) {
				
				//print_r($video_details);
				
				$sql = "select count(*) as ilosc from comment_video where video_id = '$video_details[video_id]' ";
				$details = $this->DBM->getRow($sql);

				$video_list[$key]['comments_volume'] = $details['ilosc'];
			}
		}
		
		return $video_list;	
	}
	
	/**
	 * wyciaga listę videoa dla widoku serwisu wraz z pagingiem
	 */
	
	function getVideosByHome ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video.category_id = '1' and video_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video.category_id = '1' and video_multilang.status = '2' ";
			
			$sql .= " order by video.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * wyciaga listę videoa dla rss
	 */
	
	function getVideosByRss ($order = "order", $direction = "desc", $limit, $page_number = 1, $lang_id){
		
		if($limit){
			
			
			$direction = "desc";
			$order = "order";

			global $__CFG;
			// TEST!!!
			$__CFG['record_count_limit'] = $limit;
		
			$limit = $__CFG['record_count_limit'];			
			$offset = ($page_number - 1) * $limit;
			
			
			$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video.category_id = '1' and video_multilang.status = '2' ";		
			$sql_count  = "select count(*) as ilosc from video, video_multilang where video_multilang.video_id = video.id and video_multilang.language_id = '$lang_id' and video.category_id = '1' and video_multilang.status = '2' ";
			
			$sql .= " order by video.`$order` $direction limit $offset, $limit ";
			
			
			// echo $sql;
			
			// ilośc wszystkich rekordów
			$temp = $this->DBM->getRow($sql_count);
			$record_count = $temp['ilosc'];
			
			//echo $record_count."<br>";
			
			$news_list = $this->DBM->getTable($sql);
			
			if(isset($page_number)) {
				// przeliczamy parametry
				$this->convertPagingParametersNew($record_count, $page_number);
			}
		}
		
		return $news_list;
	}
	
	/**
	 * pobiera pojedynczy artykuł do edycji
	 */
	
	function getVideo($video_id, $lang_id) {
			
		if ($video_id && $lang_id) {	
			$sql = "select video.category_id, video.order, video.pic_01, video.pic_02, video.pic_03, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.id = '$video_id' and video_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * wyciąga pierwszy artykuł z podanej kategorii (tylko id)
	 */
	
	function getFirstVideoInCategory ($category_id, $lang_id) {
		
		if ($category_id) {
			
			$sql = "select video.id from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.status != 0 order by video.`order` desc limit 1";
			$details = $this->DBM->getRow($sql);
			
			return $details['id'];
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł do widoku szczegółów
	 */
	
	function getVideoDetails($video_id, $lang_id) {
		
		if ($video_id && $lang_id) {
			$sql = "select video.category_id, video.order, video.pic_01, video.pic_02, video.pic_03, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.id = '$video_id' and video_multilang.language_id = '$lang_id'";
			$rv = $this->DBM->getRow($sql);
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany video istnieje o tej samej nazwie url /uzywane do walidacji - dla istniejaqcego wpisu/
	 */
	
	function getVideoByUrlNameAndId($url_name, $video_id) {
			
		if ($url_name && $video_id) {	
			//print_r($product_id);
			$sql = "select video_multilang.* from video_multilang where video_multilang.url_name = '$url_name' and video_multilang.video_id != '$video_id'";
			$rv = $this->DBM->getRow($sql);
			//echo $sql;
			
		}
		return $rv;
	}
	
	/**
	 * sprawdza czy dany wpis videoa istnieje o tej samej nazwie url /uzywane do walidacji - dla nowgo wpisu/
	 */
	
	function getVideoByUrlName($url_name) {
			
		if ($url_name) {	
			$sql = "select video_multilang.* from video_multilang where video_multilang.url_name = '$url_name' ";
			$rv = $this->DBM->getRow($sql);
			
		}
		return $rv;
	}
	
	/**
	 * zapisuje pojedynczy artykuł
	 */
	
	function saveVideo($video_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
		
		$symbol_filmu = $this->YouTube($video_form['link']);
		
		if (!$video_form['video_id']) {
			
			// kolejność - to jest do przeniesienia do nowej klasy Order (?) albo osobna metoda w tej klasie (?)
			$sql = "select max(`order`) as last_order from video where category_id = '$video_form[category_id]'";
			$order = $this->DBM->getRow($sql);
			$next_order = $order['last_order'] + 1;
			
			// nowa metryka
			$sql = "insert into video (category_id, `order`) values ('$video_form[category_id]', '$next_order') ";
			$rv = $this->DBM->modifyTable($sql);
			$video_id = $this->DBM->lastInsertID;
			
			// dodaj obrazek nr 1
			if ($_FILES['pic_01']['name']) {
				$pic_01 = "video_".$video_id."_01.jpg";
				$sql = "update video set pic_01 = '$pic_01' where id = '$video_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeVideoPicture ($_FILES['pic_01']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_01_01.jpg", 50, 50);
				$this->resizeVideoPicture ($_FILES['pic_01']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_02_01.jpg", 640, 480);
				$this->resizeVideoPicture ($_FILES['pic_01']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_03_01.jpg", 1280, 500);
				unlink($_FILES['pic_01']['tmp_name']);				
				

			}
			
			// dodaj obrazek nr 2
			if ($_FILES['pic_02']['name']) {
				$pic_02 = "video_".$video_id."_02.jpg";
				$sql = "update video set pic_02 = '$pic_02' where id = '$video_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeVideoPicture ($_FILES['pic_02']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_01_02.jpg", 50, 50);
				$this->resizeVideoPicture ($_FILES['pic_02']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_03_02.jpg", 640, 480);
				$this->resizeVideoPicture ($_FILES['pic_02']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_04_02.jpg", 1280, 500);
				unlink($_FILES['pic_02']['tmp_name']);				
				

			}
			
					
			// dodaj obrazek nr 3
			if ($_FILES['pic_03']['name']) {
				$pic_03 = "video_".$video_id."_03.jpg";
				$sql = "update video set pic_03 = '$pic_03' where id = '$video_id'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeVideoPicture ($_FILES['pic_03']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_01_03.jpg", 50, 50);
				$this->resizeVideoPicture ($_FILES['pic_03']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_03_03.jpg", 640, 480);
				$this->resizeVideoPicture ($_FILES['pic_03']['tmp_name'], $__CFG['video_pictures_path'].$video_id."_04_03.jpg", 1280, 500);
				unlink($_FILES['pic_03']['tmp_name']);				
				

			}
			
			if ($video_id) {
				
				$sql  = "insert into video_multilang (video_id, url_name, language_id, title, abstract, content, status, date_created, date_modified, author_id, date_video, link, symbol) ";
				$sql .= " values ('$video_id', '$video_form[url_name]', '$video_form[language_id]', '$video_form[title]', '$video_form[abstract]', '$video_form[content]', '$video_form[status]', '$date_now', '$date_now', '$video_form[author_id]', '$video_form[date_video]', '$video_form[link]', '$symbol_filmu') ";
				$rv = $this->DBM->modifyTable($sql);
				
			}
		}
		else {
			
			// aktualizacja artykułu
			
			// najpierw metryka artykułu (zmiana conajwyżej kategorii)
			$sql = "update video set category_id = '$video_form[category_id]' where id = '$video_form[video_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 1
			if ($video_form['remove_picture_01']) {
				// usuwamy obrazek
				$pic_01 = "";
				$sql = "update video set pic_01 = '$pic_01' where id = '$video_form[video_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_01_01.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_02_01.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_03_01.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_04_01.jpg");
			}
			elseif ($_FILES['pic_01']['name']) {
				// aktualizujemy obrazek
				$pic_01 = "video_".$video_form['video_id']."_01.jpg";
				$sql = "update video set pic_01 = '$pic_01' where id = '$video_form[video_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				
				// pierwsza miniatura
				$this->resizeVideoPicture ($_FILES['pic_01']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_01_01.jpg", 50, 50);
				$this->resizeVideoPicture ($_FILES['pic_01']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_02_01.jpg", 640, 480);
				$this->resizeVideoPicture ($_FILES['pic_01']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_03_01.jpg", 1280, 500);
				unlink($_FILES['pic_01']['tmp_name']);					
				
			}
			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 2
			if ($video_form['remove_picture_02']) {
				// usuwamy obrazek
				$pic_02 = "";
				$sql = "update video set pic_02 = '$pic_02' where id = '$video_form[video_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_01_02.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_03_02.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_04_02.jpg");
			}
			elseif ($_FILES['pic_02']['name']) {
				// aktualizujemy obrazek
				$pic_02 = "video_".$video_form['video_id']."_02.jpg";
				$sql = "update video set pic_02 = '$pic_02' where id = '$video_form[video_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeVideoPicture ($_FILES['pic_02']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_01_02.jpg", 50, 50);
				$this->resizeVideoPicture ($_FILES['pic_02']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_03_02.jpg", 640, 480);
				$this->resizeVideoPicture ($_FILES['pic_02']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_04_02.jpg", 1280, 500);
				unlink($_FILES['pic_02']['tmp_name']);					
				
			}			
			
			// update obrazka (usunięcie lub aktualizacja - o ile został podany) nr 3
			if ($video_form['remove_picture_03']) {
				// usuwamy obrazek
				$pic_03 = "";
				$sql = "update video set pic_03 = '$pic_03' where id = '$video_form[video_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_01_03.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_03_03.jpg");
				unlink($__CFG['video_pictures_path'].$video_form['video_id']."_04_03.jpg");
			}
			elseif ($_FILES['pic_03']['name']) {
				// aktualizujemy obrazek
				$pic_03 = "video_".$video_form['video_id']."_03.jpg";
				$sql = "update video set pic_03 = '$pic_03' where id = '$video_form[video_id]'";
				$rv = $this->DBM->modifyTable($sql);
				
				// pierwsza miniatura
				$this->resizeVideoPicture ($_FILES['pic_03']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_01_03.jpg", 50, 50);
				$this->resizeVideoPicture ($_FILES['pic_03']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_03_03.jpg", 640, 480);
				$this->resizeVideoPicture ($_FILES['pic_03']['tmp_name'], $__CFG['video_pictures_path'].$video_form[video_id]."_04_03.jpg", 1280, 500);
				unlink($_FILES['pic_03']['tmp_name']);					
				
			}				
			
			// a potem wersja językowa
			$sql = "update video_multilang set title = '$video_form[title]', url_name = '$video_form[url_name]', abstract = '$video_form[abstract]', content = '$video_form[content]', status = '$video_form[status]', date_modified = '$date_now', author_id = '$video_form[author_id]', date_video = '$video_form[date_video]', link = '$video_form[link]', symbol = '$symbol_filmu' where video_id = '$video_form[video_id]' and language_id = '$video_form[language_id]'";
			$rv = $this->DBM->modifyTable($sql);
			
		}
		
		// zwracamy ID ostatnio zapisanego artykułu
		if ($video_id) { 
			return $video_id;
		}
		else {
			return $video_form['video_id'];
		}
	}
	
	/**
	 * zapisuje powiązanie artykułów z produktem
	 */
	
	function saveVideosForProduct ($product_id, $videos) {
		
		if ($product_id && sizeof($videos)) {
			
			foreach ($videos as $video_id => $value) {
				
				// najpierw sprawdzamy, czy już przypadkiem takiego powiązania nie ma
				$sql = "select * from product_video where product_id = '$product_id' and video_id = '$video_id'";
				$details = $this->DBM->getRow($sql);
				
				// dalej działamy tylko wtedy kiedy nie ma jeszcze takiego powiązania!
				if (!sizeof($details)) {
					$sql = "insert into product_video (product_id, video_id) values ('$product_id', '$video_id')";
					$this->DBM->modifyTable($sql);
				}
			}
		}
	}
	
	/**
	 * Link z serwisu YouTube
	 */
	
	function YouTube($youtubeurl){
		

		$rpme = array("http://","www.",".com","youtube");
		$VID = str_ireplace($rpme, "", $youtubeurl);
		$VID = substr($VID, strpos($VID,"v=") + 2, strlen($VID) - strpos($VID,"v=") + 2);
	
		If(strstr($VID,"&")){
			$VID = substr($VID, 0, strpos($VID,"&") - 0);
		}
		
		$html = str_ireplace("\n","",fopen("http://www.youtube.com/watch?v=" . $VID, "r"));
		
		return $VID;		
		
		
	}
	
	/**
	 * zapisuje komentarz do video
	 */
	
	function saveCommentVideo($comment_form) {
		
		global $__CFG;
		
		// data bieżąca 
		$date_now = date("Y-m-d H:i:s", time());
		
		if ($comment_form['content']) {
			
			$content = strip_tags($comment_form['content']);
			$name= strip_tags($comment_form['name']);
			
			// nowa metryka
			$sql = "insert into comment_video ( user_id, video_id, date_created, content, remote_ip ) values ('$name', '$comment_form[video_id]', '$date_now', '$content', '$_SERVER[REMOTE_ADDR]' ) ";
			$rv = $this->DBM->modifyTable($sql);
		}
		
	}
	
	/**
	 * usuwa pojedynczy artykuł
	 */
	
	function removeVideo($video_id) {
			
		if ($video_id) {
			// metryka
			$sql = "delete from video where id = $video_id ";
			$rv = $this->DBM->modifyTable($sql);
			// i wszystkie wersje językowe
			$sql = "delete from video_multilang where video_id = $video_id ";
			$rv = $this->DBM->modifyTable($sql);
		}
		return $rv;
	}
	
	/**
	 * usuwa powiązania podanych artykułów z podanym produktem
	 */
	
	function removeVideosFromProduct ($product_id, $videos) {
		
		if ($product_id && sizeof($videos)) {
			
			foreach ($videos as $video_id => $value) {
				
				$sql = "delete from product_video where product_id = '$product_id' and video_id = '$video_id'";
				$this->DBM->modifyTable($sql);
			}
		}
	}
	
	/**
	 * gets n random videos from given category
	 */
	
	function getRandomVideos ($category_id, $limit, $lang_id) {
		
		if ($category_id && $limit && $lang_id) {
			$sql = "select video.category_id, video.order, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' order by rand() limit $limit";
			$video_list = $this->DBM->getTable($sql);
			
			return $video_list;
		}
		
	}
	
	/**
	 * wyciąga listę artykułów z zazanczonymi artykułami dla danego produktu
	 */
	
	function getVideosForProductAdmin ($product_id, $lang_id) {
		
		if ($product_id && $lang_id) {
			
			$videos = $this->getVideos("order", "desc", 1, 2, $lang_id);
			
			return $videos;
		}
		
	}
	
	/**
	 * gets all videos
	 */
	
	function getVideosAll ($lang_id) {
		
		if ($lang_id) {
			$sql = "select * from video_multilang where language_id = '$lang_id' order by date_modified desc ";
			$video_list = $this->DBM->getTable($sql);
			
			return $video_list;
		}
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters($record_count, $page_number) {
		
		global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $__CFG['record_count_limit']);
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		else {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		$this->paging = $paging;
		return $paging;
		
	}
	
		/**
		 * Przelicza wszystkie parametry do stronicowania z zakresami
		 */
	
	function convertPagingParametersNew($record_count, $page_number, $limit) {
		
			global $__CFG;
		
		$paging = array();
		$last_page = ceil($record_count / $limit);
		
		// na wszelki wypadek
		if ($last_page == 0) {
			$last_page = "";
		}
		
		// echo "last page : ".$last_page."<br>";
		
		// poprzednia strona
		if ($page_number == 1) {
			$paging['previous'] = "";
			$paging['first'] = "";
		}
		else {
			$paging['previous'] = $page_number - 1;
			$paging['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging['next'] = "";
			$paging['last'] = "";	
		}
		elseif ($last_page)  {
			$paging['next'] = $page_number + 1;
			$paging['last'] = $last_page;
		}
		
		$paging['current'] = $page_number;
		
		// i jeszcze nawigacja po kilku stronach
		// zakładamy, że rozstrzał w jedną i w drugą stronę będzie równy 4 strony
		
		$range = 3;
		
		if ($page_number < $range + 1) {
			$from = 1;
		}
		else {
			$from = $page_number - $range;
		}
		
		if ($last_page < $page_number + $range) {
			$to = $last_page;
		}
		else {
			$to = $page_number + $range;
		}
		$paging['count'] = $record_count;
		$paging['page_from'] = $from;
		$paging['page_to'] = $to;
		
		//print_r($paging);
		
		$this->paging = $paging;
		return $paging;
		
	}
	
	/**
	 * przelicza wszystkie parametry do stronicowania
	 */
	
	function convertPagingParameters2($record_count, $page_number, $limit) {
		
		global $__CFG;
		
		$paging_video = array();
		$last_page = ceil($record_count / $limit);
		
		// echo "last page : ".$last_page."<br>";
		$paging_video['count'] = $last_page;
		// poprzednia strona
		if ($page_number == 1) {
			$paging_video['previous'] = "";
			$paging_video['first'] = "";
		}
		else {
			$paging_video['previous'] = $page_number - 1;
			$paging_video['first'] = "1";
		}
		
		// następna strona
		if ($page_number == $last_page) {
			$paging_video['next'] = "";
			$paging_video['last'] = "";	
		}
		else {
			$paging_video['next'] = $page_number + 1;
			$paging_video['last'] = $last_page;
		}
		
		$paging_video['current'] = $page_number;
		
		
		$this->paging_video = $paging_video;
		return $paging_video;
		
	}
	
	/**
	 * wyciąga najnowsze newsy
	 */
	
	function getRecentNews ($category_id, $limit, $lang_id) {
		
		if ($lang_id) {
		
			$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.status = 2 ";		
			$sql .= " order by video.`order` desc limit $limit ";
			
			// echo $sql;
			
			$video_list = $this->DBM->getTable($sql);
			
			return $video_list;
		}
	}
	
	/**
	 * pobiera listę komentarzy do video	
	 */
	
	function getCommentsByVideos($video_id, $limit_count, $page_number = 1, $order = "order", $direction = "desc") {
		
		global $__CFG;

		$limit = $limit_count;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from comment_video, video where video.id = comment_video.video_id and video.id = '$video_id' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select comment_video.* from comment_video, video where video.id = comment_video.video_id and video.id = '$video_id' ";		
		$sql .= " order by comment_video.date_created desc limit $offset, $limit ";
		
		//echo $sql."<hr>";
		
		$comment_list = $this->DBM->getTable($sql);
		

		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters2($record_count, $page_number, $limit);
		}
		
		return $comment_list;
	}
	
	/**
	 * pobiera listę komentarzy do video	/administracja/
	 */
	
	function getCommentsByVideosForAdmin($video_id, $limit_count, $page_number = 1, $order = "order", $direction = "desc") {
		
		global $__CFG;

		$limit = $limit_count;
		$offset = ($page_number - 1) * $limit;
		
		$sql  = "select count(*) as ilosc from comment_video, video where video.id = comment_video.video_id and video.id = '$video_id' ";
				
		$temp = $this->DBM->getRow($sql);
		$record_count = $temp['ilosc'];
		
		$sql  = "select comment_video.* from comment_video, video where  video.id = comment_video.video_id and video.id = '$video_id' ";		
		$sql .= " order by comment_video.date_created desc limit $offset, $limit ";
		
		//echo $sql."<hr>";
		
		$comment_list = $this->DBM->getTable($sql);
		

		
		if(isset($page_number)) {
			// przeliczamy parametry
			$this->convertPagingParameters($record_count, $page_number);
		}
		
		return $comment_list;
	}
	
	/**
	 * usuwa powiązania podanych artykułów z podanym produktem
	 */
	
	function removeCommentForVideo ($comment_id) {
		
		if ($comment_id) {
			

				
			$sql = "delete from comment_video where id = '$comment_id' ";
			$this->DBM->modifyTable($sql);

		}
	}
	
	/**
	 * wyciąga editoriale
	 */
	
	function getEditorials ($category_id, $lang_id) {
		
		if ($lang_id) {
		
			$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' ";		
			$sql .= " order by video.`order` desc";
			
			// echo $sql;
			
			$video_list = $this->DBM->getTable($sql);
			
			return $video_list;
		}
	}
	
	/**
	 * wyciąga pojedynczy editorial wraz z nawigacją
	 */
	
	function getEditorial ($video_id, $lang_id, $user_id = null) {
		
		if ($lang_id) {
			
			// kategoria editoriali
			if ($user_id) {
				$category_id = 4;
			}
			else {
				$category_id = 9;
			}
			
			// jeżeli został podany konkretny editorial
			if ($video_id) {
				
				$sql = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.id = '$video_id' and video_multilang.language_id = '$lang_id' and video_multilang.status != 0";
				$video_details = $this->DBM->getRow($sql);
				
				if (sizeof($video_details)) {
					
					// wybieramy nastepny (nowszy)
					
					$sql = "select video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.`order` > '$video_details[order]' and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.status != 0 order by video.`order` asc limit 1";
					$next_video = $this->DBM->getRow($sql);
					
					if (sizeof($next_video)) {
						$video_details['next_video'] = $next_video['video_id'];
					}
					
					// wybieramy poprzedni (starszy) 
					
					$sql = "select video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.`order` < '$video_details[order]' and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.status != 0 order by video.`order` desc limit 1";
					$previous_video = $this->DBM->getRow($sql);
					
					if (sizeof($previous_video)) {
						$video_details['previous_video'] = $previous_video['video_id'];
					}
					
					return $video_details;
				}	
			}
			else {
				// nie podano konkretnego - bierzemy dwa najnowsze - od razu do nawigacji
				
				$sql  = "select video.category_id, video.order, video.pic_01, video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.category_id = '$category_id' and video_multilang.language_id = '$lang_id' and video_multilang.status != 0 ";
				$sql .= " order by video.`order` desc limit 2";
				$details = $this->DBM->getTable($sql);
				
				// echo $sql;
				
				// szczegóły bieżącego
				$video_details = $details[0];
				
				// nowszego (następnego) nie ma !
				// ...
				
				// a to jest starszy
				if (sizeof($details[1])) {
					// poprzedni editorial ( = starszy!)
					$video_details['previous_video'] = $details[1]['video_id'];
				}
				
				return $video_details;
			}
		}
	}
	
	/**
	 * pobiera pojedynczy artykuł jako treść helpa
	 */
	
	function getHelp($video_id, $lang_id) {
			
		if ($video_id && $lang_id) {	
			$sql = "select video_multilang.* from video, video_multilang where video_multilang.video_id = video.id and video.id = '$video_id' and video_multilang.language_id = '$lang_id'";
			$video_details = $this->DBM->getRow($sql);
			
			return $video_details['content'];	
		}
	}
	
	/**
	 * wysyła artykuł mailem (HTML)
	 */
	
	function sendVideoByEmail ($video_details, $email) {
		
		global $_mail_params;
		global $dict_templates;
		global $__CFG;
		global $smarty;
		
		if (sizeof($video_details) && $email) {
			
			$content = $smarty->fetch("send_video.tpl");
				
			$mail_data['html_body'] = $content;
			
			$mail_data['headers']['MIME-Version']= '1.0';
			$mail_data['headers']['Subject'] = $dict_templates['SendVideoSubject'];
			$mail_data['headers']['From'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['To'] = $email;
			$mail_data['headers']['Reply-To'] = Utils::prepareSubjectBase64($__CFG['newsletter_mail_name'])." <".$__CFG['newsletter_mail_address'].">";
			$mail_data['headers']['Date'] = date("r",time());
			
			require_once 'HCMailer2.class.php';
			
			$mailerek = new HCMailer2($mail_data);
			$mailerek->sendMailMime();
			
			return true;
		}
	}
	
	/**
	 * generuje z podanego artykułu plik tekstowy
	 */
	
	function createFileFromVideo ($video_id, $path, $lang_id) {
		
		global $__CFG;
		
		if ($video_id && $path && $lang_id) {
			
			// szczegóły artykułu
			$sql = "select title, content from video_multilang where video_id = '$video_id' and language_id = '$lang_id'";
			$details = $this->DBM->getRow($sql);
			
			if (sizeof($details)) {
				
				$details['content'] = str_replace("</p>", "\r\n", $details['content']);
				$details['content'] = strip_tags($details['content']);
				
				$filename = strtolower(Utils::toascii_replace(str_replace(" ", "_", trim($details['title'])))).".txt";
				
				// echo $path.$filename."<br/>";
				
				$fp = fopen($path.$filename, 'w');
				fwrite($fp, $details['title']."\r\n\r\n");
				fwrite($fp, $details['content']);
				fclose($fp);
				
				// echo " done<br/>";
			} 
		}
	}
	
	function echocode ($code){
		
		if($code){
			
			print_r($code);
			
		}
		
	}
	
	/**
	 * przeskalowuje importowane zdj�cie w miejscu - z resamplingiem
	 */
	
	function resizeVideoPicture ($source_path, $target_path, $xmin, $ymin) {
		
		global $__CFG;
		
		if ($source_path && $target_path && $xmin && $ymin) {
			
			// tylko jeden format zdj�� jest dozwolony
			
			// $details = getimagesize($__CFG['gallery_pictures_path'].$filename);
			$details = getimagesize($source_path);
			
			if ($details['mime'] == "image/jpeg") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromjpeg($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagejpeg($image_p, $target_path, 80);
				
			}
			
			if ($details['mime'] == "image/gif") {
			
				// Set a maximum height and width
				$width = $xmin;
				$height = $ymin;
				
				// Get new dimensions 
				// list($width_orig, $height_orig) = getimagesize($__CFG['gallery_pictures_path'].$filename);
				list($width_orig, $height_orig) = getimagesize($source_path);
				
				$ratio_orig = $width_orig/$height_orig;
				
				if ($width/$height > $ratio_orig) {
				   $width = $height*$ratio_orig;
				} else {
				   $height = $width/$ratio_orig;
				}
				
				// Resample
				$image_p = imagecreatetruecolor($width, $height);
				
				$image = imagecreatefromgif($source_path);
				
				imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
				
				imagegif($image_p, $target_path, 80);
				
			}
		}
		else {
			return false;
		}
	}	
	
	
	
	
}
?>