<!DOCTYPE html>
<html lang="en"><head>
    
    {include file="meta.tpl"} 
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="{$DP}css/bootstrap.min.css">
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="{$DP}css/navbar.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/color/red.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/styles.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/tools.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/footer-dark.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/animate.min.css">
    
    <!-- MegaMenu styles-->
    <link href="{$DP}css/megamenu.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="{$DP}font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{$DP}js/html5shiv.js"></script>
      <script src="{$DP}js/respond.min.js"></script>
    <![endif]-->

        <!--[if lte IE 7]><script src="{$DP}js/socialmediaicons-lte-ie7.js"></script><![endif]-->

    <!-- LOAD JQUERY LIBRARY -->
	<script type="text/javascript" src="{$DP}js/jquery-1.11.3.min.js"></script>
	
    <!-- MEGAFOLIO PRO GALLERY CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="{$DP}megafolio/css/settings.css" media="screen" />


    <!-- MEGAFOLIO PRO GALLERY JS FILES  -->
	<script type="text/javascript" src="{$DP}megafolio/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="{$DP}megafolio/js/jquery.themepunch.megafoliopro.js"></script>


	<!-- THE FANYCYBOX ASSETS -->
	<link rel="stylesheet" href="{$DP}megafolio/fancybox/jquery.fancybox.css?v=2.1.3" type="text/css" media="screen" />
	<script type="text/javascript" src="{$DP}megafolio/fancybox/jquery.fancybox.pack.js?v=2.1.3"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media ONLY FOR ADVANCED USAGE-->
	<script type="text/javascript" src="{$DP}megafolio/fancybox/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
    
    <!-- Favicons -->
	{include file="favicon.tpl"}
    
    <!-- For Sticky Header -->
    <script type="text/javascript" src="{$DP}js/custom.js"></script>
    
  <!-- for Animation Elements -->
    <script src="{$DP}js/wow.js"></script>
    
  </head>
  <body>

   {include file="header.tpl"}

   <!-- Main start -->
   <div class="main" role="main">
      	<!-- subheader start -->
      		<div class="row business-header">
      			<div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="custom-page-header">
                          <h1>{$blog_details.title}</h1>
                        </div>
                    </div>
                    {*<div class="col-lg-6 col-md-6 col-sm-6">*}
                        {*<ol class="breadcrumb pull-right">*}
                          {*<li><a href="{$DP}">Start</a></li>*}
                          {*<li><a href="{$DP}blog/index">Budrem Blog</a></li>*}
                        {*</ol>*}
    				{*</div>*}
    			</div>
      		</div>
     	<!-- subheader end -->
      	<!-- Content start -->
      		<div class="row content">
      			<div class="container">
                <!-- Content Start -->
                    
                    <!-- Blog FullWidth Start -->
                        
                          
                          <div class="col-md-9 col-sm-8 blog-item">
                          	{*
                          <h2 class="page-header">Blog Medium Image </h2>
                          *}

                          	  <!-- Blog Item Start -->
                                <div class="row"><!-- /.row start -->
                                  
                                  <div class="col-md-5"> <img src="{$DP}images/blog/{$blog_details.blog_id}_02_01.jpg" class="img-responsive" alt="{$blog_details.title}"> </div>
                                  <div class="col-md-7">
                                    <h3>{$blog_details.title}</h3>
                                    <p class="post-meta">
                                    	<span><i class="glyphicon glyphicon-time"></i> {$blog_details.date_created|date_format:"%Y-%m-%d"}</span>
                                    </p>
                                    <p><strong>{$blog_details.abstract}</strong></p>
                                  </div>
                                 
                                 </div><!-- /.row end -->
                                 <p>{$blog_details.content}</p>
                              <!-- Blog Item End -->
                              
                              {if $picture_list}
                              
                              	<h3 class="title">Galeria</h3>
                              
	                            <!-- The GRID System -->
	                            <div class="megafolio-container noborder norounded dark-bg-entries">
	                            	
	            						{foreach from=$picture_list item=picture name=picture}
	                                    <!-- A GALLERY ENTRY -->
	                                    <div class="mega-entry cat-one cat-all" data-src="{$DP}images/picture/{$picture.picture_id}_02_01.jpg" data-width="551" data-height="600">
	            
	                                        <!-- THE HOVER FUNCTION -->
	                                        <div class="mega-hover  alone notitle">
	                                            <a class="fancybox" data-fancybox-group="group" title="{$picture.title}" href="{$DP}images/picture/{$picture.picture_id}_03_01.jpg"><div class="mega-hoverview"></div></a>
	                                        </div>
	                                    </div>
	            						{/foreach}
	            
	                            </div>                              
                              {/if}
                    		 
                             <!-- Separator Start -->
                             <div class="row separator top40 bottom40">
                             	<div class="separator-style"></div>
                             </div>
                             <!-- Separator End -->



                    
                      </div>
                          
                    <!-- Blog FullWidth End -->
                    
				<!-- Content End -->   
                <!-- Right Sidebar Start -->
                    <div class="col-md-3 col-sm-4 sidebar">
                        <!-- Text Widget Start -->
                        <div class="panel-light top10">
                          <div class="panel-heading"> <span class="panel-title">Galerie domów</span> </div>
                          <div class="panel-body">
                          	{foreach from=$random_portfolio item=portfolio name=portfolio}
                            <p><a href="{$DP}domy/view/{$portfolio.category_name}/"><img class="img-rounded img-responsive" src="{$DP}images/portfolio/{$portfolio.portfolio_id}_02_01.jpg" alt="{$portfolio.title}"></a>
                              <a class="btn btn-primary" href="{$DP}domy/view/{$portfolio.category_name}/">Więcej <span class="glyphicon glyphicon-chevron-right"></span></a>
                            </p>
                            {/foreach}
                          </div>
                        </div>
                        <!-- Text Widget End -->    

                    </div>
                <!-- Right Sidebar End -->
                </div>
      		</div>
     	<!-- Content end -->
 	</div>
    <!-- Main end -->

  	{include file="footer.tpl"}
  	
  	
	<script type="text/javascript">
		
		{literal}

		jQuery(document).ready(function() {


			var api=jQuery('.megafolio-container').megafoliopro(
				{
					filterChangeAnimation:"rotatescale",			// fade, rotate, scale, rotatescale, pagetop, pagebottom,pagemiddle
					filterChangeSpeed:600,					// Speed of Transition
					filterChangeRotate:10,					// If you ue scalerotate or rotate you can set the rotation (99 = random !!)
					filterChangeScale:0.6,					// Scale Animation Endparameter
					delay:20,
					defaultWidth:980,
					paddingHorizontal:0,
					paddingVertical:0,
					layoutarray:[12]		// Defines the Layout Types which can be used in the Gallery. 2-9 or "random". You can define more than one, like {5,2,6,4} where the first items will be orderd in layout 5, the next comming items in layout 2, the next comming items in layout 6 etc... You can use also simple {9} then all item ordered in Layout 9 type.

				});


			// FANCY BOX ( LIVE BOX) WITH MEDIA SUPPORT
			jQuery(".fancybox").fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});

			// THE FILTER FUNCTION
			jQuery('.filter').click(function() {
				jQuery('.filter').each(function() { jQuery(this).removeClass("selected")});
				api.megafilter(jQuery(this).data('category'));
				jQuery(this).addClass("selected");
			});




		});
		{/literal}
	</script> 	



  	    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
    <script src="{$DP}js/bootstrap.min.js"></script>
    
    <!-- for Mega Menu -->
    <script src="{$DP}js/for_megamenu_run_prettify.js"></script>
    
    <!-- for Retina Graphics -->
    <script type="text/javascript" src="{$DP}js/retina.js"></script> 
  </body>
</html>