<!DOCTYPE html>
<html lang="en"><head>
   	
   	{include file="meta.tpl"}
        
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="{$DP}css/bootstrap.min.css">
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="{$DP}css/navbar.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/color/red.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/styles.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/tools.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/footer-dark.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/animate.min.css">
    
    <!-- MegaMenu styles-->
    <link href="{$DP}css/megamenu.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="{$DP}font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{$DP}js/html5shiv.js"></script>
      <script src="{$DP}js/respond.min.js"></script>
    <![endif]-->

        <!--[if lte IE 7]><script src="{$DP}js/socialmediaicons-lte-ie7.js"></script><![endif]-->

    <!-- LOAD JQUERY LIBRARY -->
	<script type="text/javascript" src="{$DP}js/jquery-1.11.3.min.js"></script>
    
    <!-- Favicons -->
	{include file="favicon.tpl"}
    
    <!-- For Sticky Header -->
    <script type="text/javascript" src="{$DP}js/custom.js"></script>
    
  <!-- for Animation Elements -->
    <script src="{$DP}js/wow.js"></script>
    
  </head>
  <body>
  	
   {include file="header.tpl"}
   
   <!-- Main start -->
   <div class="main" role="main">
      	
      	
      	
        <!-- Google Map for Header Start -->
        <div class="google-map-header-bottom">
        
              <div class="row-fluid">


              <iframe height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2561.788346944076!2d18.47720750197802!3d50.05279606251073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47114f94fa52272b%3A0x39b21aeef874a561!2sMjra+Iwanowicza+Rogozina+20%2C+44-310+Radlin%2C+Polska!5e0!3m2!1spl!2sus!4v1458825844733" allowfullscreen></iframe>
                 
              </div>		
        
        </div>
        <!-- Google Map for Header End -->      	
      	
      	{*
      	<!-- subheader start -->
      		<div class="row business-header">
      			<div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="custom-page-header">
                          <h1>Dział kontaktowy </h1>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <ol class="breadcrumb pull-right">
                          <li><a href="{$DP}">Start</a></li>
                          <li class="active">Kontakt</li>
                        </ol>
    				</div>
    			</div>
      		</div>
     	<!-- subheader end -->
          *}

        
        
      	<!-- Content start -->
      		<div class="row content">
      			
      			
      			<div class="container">
                    <div class="row">
                    	{if $good_message}
                		<div class="alert alert-dismissable alert-success fade in">
                          <button type="button" class="close" data-dismiss="alert">×</button>
                          <strong>Gratulacje!</strong> {$good_message}</a>.
                        </div>
                        {/if}
                        <div class="col-lg-8 col-sm-8">
                              <h2 class="page-header"><strong>FORMULARZ</strong> KONTAKTOWY</h2>
                              <div id="waiting_register" class="col-md-12 centered"><img class="centered top50" src="{$DP}images/loading.gif" alt="Loader"/></div>
                              
                              <!-- Contact Form Start -->
                                  
                                  <form role="form" method="post" id="question_form" novalidate="novalidate" onsubmit="sendContact(); return false;" class="form bg-clouds padding-top20 padding-bottom20">
                                      
                                      
                                      <input type="hidden" name="action" value="sendContact">
                                      <div class="row">
                                        <div class="form-group">
                                          <div class="col-md-6 control-group" id="name_group_register">
                                            <label>Imię i nazwisko *</label>
                                            <input type="text" value="" class="form-control" id="InputName" name="contact_form[name]" maxlength="100" >
                                            <span style="display:none" class="help-block"></span>
                                          </div>
                                          <div class="col-md-6 control-group" id="email_group_register">
                                            <label>Adres e-mail *</label>
											<input type="text" value="" class="form-control" id="InputEmail1" name="contact_form[email]" maxlength="100">
											<span style="display:none" class="help-block"></span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12 control-group" id="content_group_register">
                                              <label>Wiadomość *</label>
                                              <textarea style="height: 110px;" id="InputContent" name="contact_form[content]" class="form-control" rows="10" maxlength="5000"></textarea>
                                              <span style="display:none" class="help-block"></span>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="row top30 bottom20">
                                        <div class="col-md-12">
                                          <input type="submit" data-loading-text="Loading..." class="btn btn-primary btn" value="Wyślij">
                                        </div>
                                      </div>                    
                                  </form>
                                <!-- Contact Form end -->
                              
                        </div>
                        
                        <div class="col-lg-4 col-sm-4 bottom40">
                          
                          <h2 class="page-header">{$intro_main.title}</h2>
                          <p>{$intro_main.content}</p>
                          

                          
                        </div>
                      </div>
                      
                </div>
      		</div>
     	<!-- Content end -->
        
        
        
       

        
        
 	</div>
    <!-- Main end -->
    
  	{include file="footer.tpl"}


  	    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
    <script src="{$DP}js/bootstrap.min.js"></script>
    
    <!-- for Mega Menu -->
    <script src="{$DP}js/for_megamenu_run_prettify.js"></script>
    
    <!-- for Retina Graphics -->
    <script type="text/javascript" src="{$DP}js/retina.js"></script> 
  </body>
</html>