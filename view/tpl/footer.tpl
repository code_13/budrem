  <!-- MainBottom start -->
 		
        <!-- Start Footer 1-4 -->
        <section class="content-block-nopad bg-offwhite padding-top60 padding-bottom40">
          <div id="footer1">
              <div>
                <div class="container">
                  <div class="row">
                  	
                    <div class="col-md-2 navbar-brand"> <a class="" href="{$DP}"><img alt="Budrem Rybnik" src="{$DP}images/logo.png"></a> </div>
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-3">
                      <h5 class="title">Menu</h5>
                      <ul class="bottom-links">
                                <li {if $script eq home}class="active"{/if}><a href="/">Start</a></li>
                                
								

                              <li {if $script eq blog}class="active"{/if}><a href="{$DP}blog">Blog</a></li>
                              <li {if $script eq contact}class="active"{/if}><a href="{$DP}kontakt">Kontakt</a></li>
                      </ul>
                    </div>                    
                    
                    

                    <div class="col-md-3">
                      <h5 class="title">Oferta</h5>
                      <ul class="bottom-links">
                            <li {if $script eq gallery}class="active"{/if} class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Oferta <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                {foreach from=$category_list item=category}
                                <li><a href="{$DP}oferta/{$category.url_name}/">{$category.title}</a></li>
                                {/foreach}
                              </ul>
                            </li>
                            {*
                            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Domy na sprzedaż <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                <li><a href="{$DP}domy/index">Aktualna oferta domów</a></li>
                                <li><a href="#">Portfolio</a></li>

                              </ul>
                            </li>
                            *}
                          <li {if $script eq house}class="active"{/if}><a href="{$DP}domy/index">Domy na sprzedaż</a></li>
                          <li {if $script eq product}class="active"{/if}><a href="{$DP}apartamenty-widokowe">Apartamenty</a></li>
                          <li {if $script eq product}class="active"{/if} class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Apartamenty <b class="caret"></b></a>
                              <ul class="dropdown-menu">
                                  {foreach from=$menu_list item=menu}
                                      <li><a href="{$DP}apartamenty/{$menu.url_name}/">{$menu.title}</a></li>
                                  {/foreach}
                              </ul>
                          </li>
                      </ul>
                    </div>



                  </div>
                </div>
              </div>
              <!-- /.item --> 
              
            </div>

          <!-- /.container --> 
        </section>
        <!--// END Footer 1-4 -->
	
 	<!-- MainBottom end -->
 	<!-- footer start -->
 		<div class="row footer">
 			<div class="container">
            	<div class="col-md-6 col-sm-6 col-xs-4"><p class="copyright">Copyright © 2016 BUD-REM</p></div>
                <div class="col-md-6 col-sm-6 col-xs-8">
                	<p class="copyright pull-right">created by <a href="http://www.code13.pl" target="new">Code13</a></p>
                </div>
            </div>
 		</div>
 	<!-- footer end -->
    
 	<!-- scroll-to-top start -->
    <span id="scroll-to-top">
      <a class="scroll-up">
        <span class="glyphicon glyphicon-chevron-up"></span>
      </a>
	</span>
    <!-- scroll-to-top end -->

  {literal}

      <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-69569231-11', 'auto');
          ga('send', 'pageview');

      </script>

  {/literal}
    