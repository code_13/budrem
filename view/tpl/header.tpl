  <!-- Header start -->
  	<header>
  	<!-- Toolbar start -->
  		<div class="row toolbar hidden-xs">
  			<div class="container">
            	<div class="col-sm-6 col-xs-6">
            <span class="glyphicon glyphicon-earphone"></span> &nbsp;&nbsp;662-030-221<span class="separator"></span><a href="#"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;biuro@bud-rem.rybnik.pl</a>
				</div>
				{*
                <div class="col-sm-6 hidden-xs">
                	<div class="social-media-icons pull-right">
                        <a href="http://facebook.com" title="Facebook" class="tooltip-bottom" data-toggle="tooltip" target="new"><span aria-hidden="true" class="mk-social-facebook"></span></a>
                    </div>
                </div>
                *}
			</div>
  		</div>
  	<!-- Toolbar end -->
  	<!-- header-main start -->
                <div class="row header-main">
                    <div class="container">
                        <!-- Navbar Start -->
                        <nav class="navbar navbar-default megamenu">
                          <div class="navbar-header">
                            <button type="button" data-toggle="collapse" data-target="#navbar-collapse-2" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="{$DP}" class="navbar-brand"><img src="{$DP}images/logo.png" class="img-responsive" alt="BudRem Rybnik"></a>
                          </div>
                          <div id="navbar-collapse-2" class="navbar-collapse collapse pull-right">
                            <ul class="nav navbar-nav">


                                <li {if $script eq home}class="active"{/if}><a href="/">Start</a></li>


                                <li {if $script eq gallery}class="active"{/if} class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Oferta <b class="caret"></b></a>
                                  <ul class="dropdown-menu">
                                  	{foreach from=$category_list item=category}
                                    <li><a href="{$DP}oferta/{$category.url_name}/">{$category.title}</a></li>
                                    {/foreach}
                                  </ul>
                                </li>
								{*
                                <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Domy na sprzedaż <b class="caret"></b></a>
                                  <ul class="dropdown-menu">
                                  	<li><a href="{$DP}domy/index">Aktualna oferta domów</a></li>
                                    <li><a href="#">Portfolio</a></li>

                                  </ul>
                                </li>
                                *}
                              <li {if $script eq house}class="active"{/if}><a href="{$DP}domy/index">Domy na sprzedaż</a></li>
                              {*<li {if $script eq product}class="active"{/if}><a href="{$DP}apartamenty-widokowe">Apartamenty</a></li>*}
                            <li {if $script eq product}class="active"{/if} class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Apartamenty <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    {foreach from=$menu_list item=menu}
                                        <li><a href="{$DP}apartamenty/{$menu.url_name}/">{$menu.title}</a></li>
                                    {/foreach}
                                </ul>
                            </li>
                              <li {if $script eq blog}class="active"{/if}><a href="{$DP}blog">Blog</a></li>
                              <li {if $script eq contact}class="active"{/if}><a href="{$DP}kontakt">Kontakt</a></li>

                            </ul>
                          </div>
                        </nav>
                        <!-- Navbar end -->
                    </div>
                </div>
     <!-- header-main end -->
   </header>
   <!-- Header end -->