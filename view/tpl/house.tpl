<!DOCTYPE html>
<html lang="en"><head>
    
    {include file="meta.tpl"} 
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="{$DP}css/bootstrap.min.css">
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="{$DP}css/navbar.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/color/red.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/styles.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/tools.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/footer-dark.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/animate.min.css">
    
    <!-- MegaMenu styles-->
    <link href="{$DP}css/megamenu.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="{$DP}font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{$DP}js/html5shiv.js"></script>
      <script src="{$DP}js/respond.min.js"></script>
    <![endif]-->

        <!--[if lte IE 7]><script src="{$DP}js/socialmediaicons-lte-ie7.js"></script><![endif]-->

    <!-- LOAD JQUERY LIBRARY -->
	<script type="text/javascript" src="{$DP}js/jquery-1.11.3.min.js"></script>
    
    <!-- Favicons -->
	{include file="favicon.tpl"}
    
    <!-- For Sticky Header -->
    <script type="text/javascript" src="{$DP}js/custom.js"></script>
    
  <!-- for Animation Elements -->
    <script src="{$DP}js/wow.js"></script>
    
  </head>
  <body>

   {include file="header.tpl"}

   <!-- Main start -->
   <div class="main" role="main">
      	<!-- subheader start -->
      		<div class="row business-header">
      			<div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="custom-page-header">
                          <h1>Domy na sprzedaż </h1>
                        </div>
                    </div>
                    {*<div class="col-lg-6 col-md-6 col-sm-6">*}
                        {*<ol class="breadcrumb pull-right">*}
                          {*<li><a href="{$DP}">Start</a></li>*}
                          {*<li class="active">Domy</li>*}
                        {*</ol>*}
    				{*</div>*}
    			</div>
      		</div>

      		<div class="row content">
      			<div class="container">
                <!-- Content Start -->
                    
                    <!-- Blog FullWidth Start -->
                        
                          
                          <div class="col-md-9 col-sm-8 blog-item">
                          	{*
                          <h2 class="page-header">Blog Medium Image </h2>
                          *}
                          	  {foreach from=$house_list item=house name=house}
                          	  <!-- Blog Item Start -->
                                <div class="row"><!-- /.row start -->
                                  
                                  <div class="col-md-5"> <a href="{$DP}domy/view/{$house.url_name}/"><img src="{$DP}images/house/{$house.house_id}_02_01.jpg" class="img-responsive" alt="{$house.title}"></a> </div>
                                  <div class="col-md-7">
                                    <h3><a href="{$DP}domy/view/{$house.url_name}/">{$house.title}</a></h3>
                                    <p>{$house.abstract}</p>
                                    <a class="btn btn-primary" href="{$DP}domy/view/{$house.url_name}/">Czytaj więcej <span class="glyphicon glyphicon-pencil"></span></a>
                                  </div>
                                 
                                 </div><!-- /.row end -->
                              <!-- Blog Item End -->
                    		 
                             <!-- Separator Start -->
                             <div class="row separator top40 bottom40">
                             	<div class="separator-style"></div>
                             </div>
                             <!-- Separator End -->
							{/foreach}

                    
                      </div>
                          
                    <!-- Blog FullWidth End -->
                    
				<!-- Content End -->   
                <!-- Right Sidebar Start -->
                    <div class="col-md-3 col-sm-4 sidebar">


                        <!-- Text Widget Start -->
                        <div class="panel-light top10">
                          <div class="panel-heading"> <span class="panel-title">Ostatnio na blogu</span> </div>
                          <div class="panel-body">
                          	{foreach from=$last_blog name=blog item=blog}
                            <p><img class="img-rounded img-responsive" src="{$DP}images/blog/{$blog.blog_id}_02_01.jpg" alt="{$blog.title}">{$blog.title} <br>
                              <a class="btn btn-primary" href="{$DP}blog/view/{$blog.url_name}/">Czytaj więcej <span class="glyphicon glyphicon-chevron-right"></span></a>
                              </p>
                            {/foreach}
                          </div>
                        </div>
                        <!-- Text Widget End -->


                    </div>
                <!-- Right Sidebar End -->
                </div>
      		</div>

 	</div>
    <!-- Main end -->

  	{include file="footer.tpl"}



  	    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster--> 
    <script src="{$DP}js/bootstrap.min.js"></script>
    
    <!-- for Mega Menu -->
    <script src="{$DP}js/for_megamenu_run_prettify.js"></script>
    
    <!-- for Retina Graphics -->
    <script type="text/javascript" src="{$DP}js/retina.js"></script> 
  </body>
</html>