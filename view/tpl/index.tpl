<!DOCTYPE html>
<html lang="en"><head>
    
    {include file="meta.tpl"}

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="css/bootstrap.min.css">
    
    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/navbar.css">
    <link rel="stylesheet" type="text/css" href="css/color/red.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="css/tools.css">
    <link rel="stylesheet" type="text/css" href="css/footer-light.css">
    <link rel="stylesheet" type="text/css" href="css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link rel="stylesheet" type="text/css" href="css/animate.min.css">
    
    <!-- MegaMenu styles-->
    <link href="css/megamenu.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

        <!--[if lte IE 7]><script src="js/socialmediaicons-lte-ie7.js"></script><![endif]-->

    <!-- LOAD JQUERY LIBRARY -->
	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
    
    <!-- MEGAFOLIO PRO GALLERY CSS JS FILES  -->
    <link rel="stylesheet" type="text/css" href="megafolio/css/settings.css" media="screen" />
	<script type="text/javascript" src="megafolio/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="megafolio/js/jquery.themepunch.megafoliopro.js"></script>
	<link rel="stylesheet" href="megafolio/fancybox/jquery.fancybox.css?v=2.1.3" type="text/css" media="screen" />
	<script type="text/javascript" src="megafolio/fancybox/jquery.fancybox.pack.js?v=2.1.3"></script>
	<script type="text/javascript" src="megafolio/fancybox/helpers/jquery.fancybox-media.js?v=1.0.5"></script>
    
    <!-- LOADING FONTS AND ICONS 
    <link rel="stylesheet" type="text/css" href="revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">-->
    <link rel="stylesheet" type="text/css" href="revolution/fonts/font-awesome/css/font-awesome.min.css">
    
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
        
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
    
    <!-- Favicons -->
	{include file="favicon.tpl"}
    
  </head>
  <body>
   {include file="header.tpl"}
   
   {include file="panel_slider.tpl"}


   <div class="row content">
       <div class="container">

           <div class="col-md-8 col-sm-8 blog-item">

               {if $house_promo}

                   <div class="row">
                       {*<div class="panel-light top10 text-center">*}
                            {*<div class="panel-heading center"> <span class="panel-title">Polecamy domy</span> </div>*}
                       {*</div>*}

                       {foreach from=$house_promo item=house name=house}
                           <div class="col-sm-12">
                               <h5 class="text-center top30 mod1">{$house.title}</h5>
                               <a href="{$DP}domy/view/{$house.url_name}/">
                                   <img class="img-responsive img-rounded" src="{$DP}php/timthumb.php?src={$DP}images/house/{$house.house_id}_03_01.jpg&q=100&w=700&h=500&a=t" alt="{$house.title}">
                               </a>

                           </div>
                       {/foreach}

                   </div>

               {/if}

               {if $product_promo}

                   <div class="row">
                       {*<div class="panel-light top30 bottom10 text-center">*}
                           {*<div class="panel-heading center"> <span class="panel-title">Polecamy apartamenty</span> </div>*}
                       {*</div>*}
                       {foreach from=$product_promo item=product name=product}
                           <div class="col-sm-12">
                               <h5 class="text-center top30 mod1">{$product.title}</h5>
                               <a href="{$DP}apartamenty/{$product.url_name}/">
                                   <img class="img-responsive img-rounded" src="{$DP}php/timthumb.php?src={$DP}images/product/{$product.product_id}_03_01.jpg&q=100&w=700&h=500&a=t" alt="{$product.title}">
                               </a>
                           </div>
                       {/foreach}
                   </div>

               {/if}

           </div>

           <!-- Right Sidebar Start -->
           <div class="col-md-4 col-sm-4 sidebar">
               <!-- Text Widget Start -->
               <div class="panel-light top10">
                   <div class="panel-heading"> <span class="panel-title">{$intro_main.title}</span> </div>
                   <div class="panel-body">
                       <p class="mod1 concrete">{$intro_main.content}</p>
                   </div>
               </div>
               <!-- Text Widget End -->
           </div>
           <!-- Right Sidebar End -->
       </div>
   </div>
                



  

  

  
  
  
  {include file="footer.tpl"}

<!--
#####################################
 - Megafolio FullWidth Style Start -
#####################################
-->
			<script type="text/javascript">
{literal}

				jQuery(document).ready(function() {


					var api=jQuery('.megafolio-container').megafoliopro(
						{
							filterChangeAnimation:"rotatescale",			// fade, rotate, scale, rotatescale, pagetop, pagebottom,pagemiddle
							filterChangeSpeed:600,					// Speed of Transition
							filterChangeRotate:10,					// If you ue scalerotate or rotate you can set the rotation (99 = random !!)
							filterChangeScale:0.6,					// Scale Animation Endparameter
							delay:20,
							defaultWidth:980,
							paddingHorizontal:0,
							paddingVertical:0,
							layoutarray:[12]		// Defines the Layout Types which can be used in the Gallery. 2-9 or "random". You can define more than one, like {5,2,6,4} where the first items will be orderd in layout 5, the next comming items in layout 2, the next comming items in layout 6 etc... You can use also simple {9} then all item ordered in Layout 9 type.

						});


					// FANCY BOX ( LIVE BOX) WITH MEDIA SUPPORT
					jQuery(".fancybox").fancybox({
						openEffect  : 'none',
						closeEffect : 'none',
						helpers : {
							media : {}
						}
					});

					// THE FILTER FUNCTION
					jQuery('.filter').click(function() {
						jQuery('.filter').each(function() { jQuery(this).removeClass("selected")});
						api.megafilter(jQuery(this).data('category'));
						jQuery(this).addClass("selected");
					});




				});
{/literal}
			</script>
            
<!--
#####################################
 - Megafolio FullWidth Style End -
#####################################
-->

<!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
        (Load Extensions only on Local File Systems !
        The following part can be removed on Server for On Demand Loading) -->	
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
    
    <!-- Custom JS -->
    <script type="text/javascript" src="js/custom.js"></script>
    
    <!-- JS code for Parallax Scrolling  -->
    <script src="js/parallax-scrolling-script.js"></script>

    <!-- Bootstrap core JavaScript --> 
    <script src="js/bootstrap.min.js"></script>
    
    <!-- for Mega Menu -->
    <script src="js/for_megamenu_run_prettify.js"></script>
    
    <!-- for Retina Graphics -->
    <script type="text/javascript" src="js/retina.js"></script>
    
    <!-- for Animation Elements -->
    <script src="js/wow.js"></script>
    	
  </body>
</html>