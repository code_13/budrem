    <title>{if $head.title}{$head.title}{else}BUD-REM Budowa domów.{/if}</title>
    <meta charset="utf-8">
	<meta name="keywords" content="{if $head.keywords}{$head.keywords}{else}budowa domów, apartamnety mieszkalne, Radlin, Rybnik, śląsk, Żory, Wodzisław, roboty ogólnobudowlane, wykończenia, docieplenia, budynki{/if}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{if $head.desc}{$head.desc}{else}Bud Rem to firma zajmująca się wszechstronnie wszelkimi usługami ogólnobudowlanymi, ale przedewszystkim budową domów of fundamentów, aż po sam dach. Zapraszamy do zapoznania się z naszą ofertą.{/if}">