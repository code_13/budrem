{if $paging.page_to && $paging.page_from ne $paging.page_to}    
<!-- Pagination Start -->
<div class="row text-center">

	<div class="col-lg-12">
		<ul class="pagination">
			
			
			{if $paging.previous}
			<li><a href="{$DP}blogg/index/{$paging.previous}">&laquo;</a></li>
			{/if}
			
		    {section name=page start=$paging.page_from loop=$paging.page_to+1}
		    {if $smarty.section.page.index ne $paging.current} 	
		    
		    <li><a href="{$DP}blogg/index/{$smarty.section.page.index}">{$smarty.section.page.index}</a></li>
		    
    		{else}		    
		    		
			<li class="active"><a href="{$DP}blogg/index/{$smarty.section.page.index}">{$smarty.section.page.index}</a></li>
			
		    {/if}
		    {/section}			
			



			{if $paging.next}
			<li><a href="{$DP}blogg/index/{$paging.next}">&raquo;</a></li>
			{/if}
		</ul>        
	</div>

</div>
<!-- Pagination End -->
{/if}







