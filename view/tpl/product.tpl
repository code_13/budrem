<!DOCTYPE html>
<html lang="en"><head>

    {include file="meta.tpl"}

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link rel="stylesheet" media="screen" href="{$DP}css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="{$DP}css/navbar.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/color/red.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/styles.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/header-fullwidth.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/tools.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/footer-dark.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/socialmediaicons.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/responsive.css">
    <link rel="stylesheet" type="text/css" href="{$DP}css/animate.min.css">

    <!-- MegaMenu styles-->
    <link href="{$DP}css/megamenu.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{$DP}font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{$DP}js/html5shiv.js"></script>
      <script src="{$DP}js/respond.min.js"></script>
    <![endif]-->

        <!--[if lte IE 7]><script src="{$DP}js/socialmediaicons-lte-ie7.js"></script><![endif]-->

    <!-- LOAD JQUERY LIBRARY -->
	<script type="text/javascript" src="{$DP}js/jquery-1.11.3.min.js"></script>

    <!-- MEGAFOLIO PRO GALLERY CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="{$DP}megafolio/css/settings.css" media="screen" />


    <!-- MEGAFOLIO PRO GALLERY JS FILES  -->
	<script type="text/javascript" src="{$DP}megafolio/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="{$DP}megafolio/js/jquery.themepunch.megafoliopro.js"></script>


	<!-- THE FANYCYBOX ASSETS -->
	<link rel="stylesheet" href="{$DP}megafolio/fancybox/jquery.fancybox.css?v=2.1.3" type="text/css" media="screen" />
	<script type="text/javascript" src="{$DP}megafolio/fancybox/jquery.fancybox.pack.js?v=2.1.3"></script>

	<!-- Optionally add helpers - button, thumbnail and/or media ONLY FOR ADVANCED USAGE-->
	<script type="text/javascript" src="{$DP}megafolio/fancybox/helpers/jquery.fancybox-media.js?v=1.0.5"></script>

    <!-- Favicons -->
	{include file="favicon.tpl"}

    <!-- For Sticky Header -->
    <script type="text/javascript" src="{$DP}js/custom.js"></script>

  <!-- for Animation Elements -->
    <script src="{$DP}js/wow.js"></script>

  </head>
  <body>

   {include file="header.tpl"}

   <!-- Main start -->
   <div class="main" role="main">
      	<!-- subheader start -->
        <div class="row business-apart" style="background: url('{$DP}php/timthumb.php?src={$DP}images/product/{$product_details.product_id}_03_01.jpg&q=100&w=1920&h=500&a=t') center center no-repeat;">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="custom-page-header">
                      <h1>{$product_details.title}</h1>
                    </div>
                </div>
                {*<div class="col-lg-6 col-md-6 col-sm-6">*}
                    {*<ol class="breadcrumb pull-right">*}
                      {*<li><a href="{$DP}">Start</a></li>*}
                      {*<li><a href="{$DP}apartamenty-widokowe">{$product_details.title}</a></li>*}
                    {*</ol>*}
                {*</div>*}
            </div>
        </div>

       <div class="row content">
           <div class="container">
               <!-- Content Start -->

               <!-- Blog FullWidth Start -->


               <div class="col-md-8 col-sm-8">

                   <h2 class="page-header">{$product_details.title}</h2>

                   <div class="no_way">{$product_details.abstract}</div>
                   <div class="no_way">{$product_details.content}</div>


                   <!-- Blog Item End -->


                   <!-- Separator Start -->
                   <div class="row separator top40 bottom40">
                       <div class="separator-style"></div>
                   </div>
                   <!-- Separator End -->


                   {if $photo_list}

                       <!-- The GRID System -->
                       <div class="megafolio-container noborder norounded dark-bg-entries">

                           {foreach from=$photo_list item=photo name=photo}
                               <!-- A product ENTRY -->
                               <div class="mega-entry cat-one cat-all"
                                    data-src="{$DP}images/photo/{$photo.photo_id}_03_01.jpg" data-width="551"
                                    data-height="600">

                                   <!-- THE HOVER FUNCTION -->
                                   <div class="mega-hover  alone notitle">
                                       <a class="fancybox" data-fancybox-group="group" title="{$product.title}"
                                          href="{$DP}images/photo/{$photo.photo_id}_03_01.jpg">
                                           <div class="mega-hoverview"></div>
                                       </a>
                                   </div>
                                   <!-- THE CAPTION -->
                                   <div class="productcaption-bottom">
                                       <div class="productsubline">{$product.title}</div>
                                   </div>
                               </div>
                           {/foreach}


                       </div>
                   {/if}


               </div>

               <!-- Blog FullWidth End -->

               <!-- Content End -->
               <!-- Right Sidebar Start -->
               <div class="col-md-4 col-sm-4 sidebar">

                   <!-- Text Widget Start -->
                   <div class="panel-light top10">
                       <div class="panel-heading"><span class="panel-title">Zobacz na mapie</span></div>
                       <div class="panel-body">
                           <div style="height: 500px;" id="google-map" class="gmap ghghhg"></div>
                       </div>
                   </div>
                   <!-- Text Widget End -->
               </div>
               <!-- Right Sidebar End -->
           </div>
       </div>
     	<!-- Content end -->
 	</div>

  	{include file="footer.tpl"}


	<script type="text/javascript">

		{literal}

		jQuery(document).ready(function() {


			var api=jQuery('.megafolio-container').megafoliopro(
				{
					filterChangeAnimation:"rotatescale",			// fade, rotate, scale, rotatescale, pagetop, pagebottom,pagemiddle
					filterChangeSpeed:600,					// Speed of Transition
					filterChangeRotate:10,					// If you ue scalerotate or rotate you can set the rotation (99 = random !!)
					filterChangeScale:0.6,					// Scale Animation Endparameter
					delay:20,
					defaultWidth:980,
					paddingHorizontal:0,
					paddingVertical:0,
					layoutarray:[12]		// Defines the Layout Types which can be used in the Gallery. 2-9 or "random". You can define more than one, like {5,2,6,4} where the first items will be orderd in layout 5, the next comming items in layout 2, the next comming items in layout 6 etc... You can use also simple {9} then all item ordered in Layout 9 type.

				});


			// FANCY BOX ( LIVE BOX) WITH MEDIA SUPPORT
			jQuery(".fancybox").fancybox({
				openEffect  : 'none',
				closeEffect : 'none',
				helpers : {
					media : {}
				}
			});

			// THE FILTER FUNCTION
			jQuery('.filter').click(function() {
				jQuery('.filter').each(function() { jQuery(this).removeClass("selected")});
				api.megafilter(jQuery(this).data('category'));
				jQuery(this).addClass("selected");
			});




		});
		{/literal}
	</script>



  	    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster-->
    <script src="{$DP}js/bootstrap.min.js"></script>

    <!-- for Mega Menu -->
    <script src="{$DP}js/for_megamenu_run_prettify.js"></script>

    <!-- for Retina Graphics -->
    <script type="text/javascript" src="{$DP}js/retina.js"></script>


   <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD-8yQhbsLR3fg89GopBhxZjvicKG15T3s"></script>
   <script type="text/javascript" src="{$DP}js/jquery.gmap.js"></script>

   <script type="text/javascript">
       {literal}
       var myLatlng = new google.maps.LatLng({/literal}{$product_details.param1}{literal},{/literal}{$product_details.param2}{literal});
       var mapOptions = {
           zoom: 14,
           center: myLatlng
       }
       var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);

       var marker = new google.maps.Marker({
           position: myLatlng,
           title:"{/literal}{$product_details.title}{literal}"
       });
       marker.addListener('click', toggleBounce);

       function toggleBounce() {
           if (marker.getAnimation() !== null) {
               marker.setAnimation(null);
           } else {
               marker.setAnimation(google.maps.Animation.BOUNCE);
           }
       }
       // To add the marker to the map, call setMap();
       marker.setMap(map);
       {/literal}
   </script>

  </body>
</html>
